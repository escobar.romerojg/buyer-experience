module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended',
    'prettier',
    'plugin:@gitlab/default',
  ],
  parserOptions: {
    parser: require.resolve('@typescript-eslint/parser'),
    extraFileExtensions: ['.vue'],
  },
  plugins: ['@gitlab', 'custom-rules'],
  rules: {
    'custom-rules/vhtml-slptypography': 'error',
    'no-unused-vars': 'off',
    'vue/no-v-html': 'off',
    'filenames/match-regex': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/no-unresolved': 'off',
    'import/extensions': [0, '{ <js>: "always"}'],
    'vue/html-self-closing': 'off',
    'no-plusplus': 'off',
    'babel/camelcase': 'off',
    'default-case': 'off',
    'promise/always-return': 'off',
  },
};
