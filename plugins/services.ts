import { Context } from '@nuxt/types';
import {
  GetStartedService,
  WhyGitLabService,
  AnalyticsAndInsightsService,
  SolutionsService,
  FreeTrialService,
  SolutionsStartupsService,
  SecurityService,
} from '../services';

// eslint-disable-next-line import/no-default-export
export default ($ctx: Context, inject: any) => {
  const whyGitlabService = new WhyGitLabService($ctx);
  const getStartedService = new GetStartedService($ctx);
  const analyticsAndInsightsService = new AnalyticsAndInsightsService($ctx);
  const freeTrialService = new FreeTrialService($ctx);
  const securityService = new SecurityService($ctx);
  const solutionsService = new SolutionsService($ctx);
  const solutionsStartupsService = new SolutionsStartupsService($ctx);

  inject('whyGitlabService', whyGitlabService);
  inject('getStartedService', getStartedService);
  inject('analyticsAndInsightsService', analyticsAndInsightsService);
  inject('freeTrialService', freeTrialService);
  inject('securityService', securityService);
  inject('solutionsService', solutionsService);
  inject('solutionsStartupsService', solutionsStartupsService);
};
