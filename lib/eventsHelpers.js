function faqItems(group) {
  return group.map((item) => {
    return {
      question: item?.header || item?.fields.header || '',
      answer: item?.text || item?.fields.text || '',
    };
  });
}

function faqHelper(data) {
  const questions =
    data.singleAccordionGroupItems && faqItems(data.singleAccordionGroupItems);

  return {
    header: data.title || null,
    data_inbound_analytics: data.data_inbound_analytics || null,
    background: data.background || null,
    show_all_button: data.showAllButton || null,
    texts: {
      show: data.expandAllCarouselItemsText || null,
      hide: data.hideAllCarouselItemsText || null,
    },
    groups: [
      {
        header: data.subtitle || '',
        questions,
      },
    ],
  };
}

function metaDataHelper(data) {
  const seo =
    data.seo?.fields || data.seoMetadata?.metadata || data.fields || {};
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    no_index: seo.noIndex,
  };
}

function imageBuilder(data) {
  return {
    alt: data?.altText || data?.title || '',
    src: data?.image?.fields?.file?.url || data?.file?.url || '',
    description: data?.image?.fields?.description || '',
  };
}

function dateHelper(
  date,
  options = { year: 'numeric', month: 'long', day: 'numeric' },
) {
  const tempDate = new Date(date);
  return tempDate.toLocaleDateString('en-US', options);
}

function createEventCards(card) {
  const date = dateHelper(card.date, { month: 'long', day: 'numeric' });
  const image =
    card.seo.fields.ogImage?.fields &&
    imageBuilder(card.seo.fields.ogImage?.fields);

  return {
    location: card.location,
    locationState: card.locationState,
    date,
    image,
    slug: card.slug,
    eventCardEyebrowImage:
      card.eventCardEyebrowImage &&
      imageBuilder(card.eventCardEyebrowImage.fields),
  };
}
function objectCleanUp(element) {
  if (element.fields.assets) {
    element.fields.assets = element.fields.assets.map((asset) => asset.fields);
  }
  if (element.fields.cta) {
    element.fields.cta = element.fields.cta.fields;
  }
  if (element.fields.backgroundImage) {
    element.fields.backgroundImage = imageBuilder(
      element.fields.backgroundImage.fields,
    );
  }
  if (element.fields.primaryCta) {
    element.fields.primaryCta = element.fields.primaryCta.fields;
  }
  if (element.fields.card) {
    element.fields.card = element.fields.card.map(
      (card) => card.fields && createEventCards(card.fields),
    );
  }
  if (element.fields.singleAccordionGroupItems) {
    element.fields.singleAccordionGroupItems =
      element.fields.singleAccordionGroupItems.map(
        (accordion) => accordion.fields,
      );
  }
  return element.fields;
}

function mapPage(array) {
  let hero = {};
  const regions = [];
  let form = {};
  let faq = {};
  let banner = {};
  let nextSteps;
  let metadata;

  array.forEach((element) => {
    switch (element.sys?.contentType?.sys?.id) {
      case 'eventHero':
        hero = objectCleanUp(element);
        break;
      case 'cardGroup':
        regions.push({ ...objectCleanUp(element) });
        break;
      case 'form':
        form = element.fields;
        break;
      case 'faq':
        faq = objectCleanUp(element);
        break;
      case 'headerAndText':
        banner = objectCleanUp(element);
        break;
      case 'nextSteps':
        nextSteps = element.fields.variant;
        break;
      case 'seo':
        metadata = element.fields;
        break;
      default:
        break;
    }
  });
  return { metadata, hero, regions, form, faq, banner, nextSteps };
}
function pageCleanup(data) {
  const mappedData = {};

  Object.keys(data).forEach((key) => {
    if (Array.isArray(data[key])) {
      mappedData[key] = mapPage(data[key]);
    } else {
      mappedData[key] = data[key];
    }
  });

  return mappedData;
}

function speakerBuilder(speaker) {
  return {
    name: speaker.author || '',
    title: speaker.authorTitle || '',
    company: speaker.authorCompany || '',
    image:
      speaker.authorHeadshot && imageBuilder(speaker.authorHeadshot?.fields),
    biography: speaker.quoteText || '',
    class: speaker.authorHeadshotCustomClass || '',
  };
}
function mapArray(array) {
  return array
    .filter((element) => element.fields)
    .map((element) => {
      return objectCleanUp(element);
    });
}
function dataCleanUp(data) {
  const mappedData = {};

  Object.keys(data).forEach((key) => {
    if (Array.isArray(data[key])) {
      mappedData[key] = mapArray(data[key]);
    } else {
      mappedData[key] = data[key];
    }
  });

  return mappedData;
}

function heroHelper(data) {
  const herofields = data.hero?.fields || null;
  const hero = {
    headline: data.name || '',
    accent: herofields?.title || '',
    subHeading: herofields?.subheader || '',
    list: (data.hero && herofields?.description?.split('\n')) || [],
    cta: herofields?.primaryCta?.fields || {},
  };

  return hero;
}
function worldTourHeroHelper(data, breadcrumbs) {
  const hero = {
    breadcrumbs,
    header: data.title || '',
    text: data.description || '',
    image: data.backgroundImage && imageBuilder(data.backgroundImage?.fields),
    cta: data.cta || data.primaryCta?.fields || {},
  };
  return hero;
}

function worldTourLandingHeroBuilder(data) {
  const hero = {
    header: data.title || '',
    subtitle: data.description || '',
    image: data.backgroundImage,
    button: data.cta || data.primaryCta || {},
  };
  return hero;
}

function sectionBuilder(section, key) {
  const { header, text, subheader, cta, assets } = section;
  let parsedAssets = assets;
  if (assets) {
    parsedAssets = assets.map((asset) => {
      if (asset.image) {
        return imageBuilder(asset);
      }
      if (asset.authorHeadshot) {
        return speakerBuilder(asset);
      }
      return parsedAssets;
    });
  }
  return { header, text, subheader, cta, [key || 'assets']: parsedAssets };
}
function sectionsHelper(data, key) {
  let sections = [];
  if (data) {
    sections = data.map((section) => {
      return sectionBuilder(section, key);
    });
  }
  return sections;
}

function agendaHelper(data) {
  let agenda = {};
  if (data) {
    agenda = { ...data?.shift(), schedule: [...data] };
  }
  return agenda;
}

function worldTourAgendaHelper(data) {
  const agendaIntro = sectionBuilder(data?.shift());
  const agenda = data.map((session) => {
    const speakers =
      session.assets && session.assets.map((asset) => speakerBuilder(asset));
    return {
      time: session.header,
      name: session.subheader,
      description: session.text,
      speakers,
    };
  });

  return { ...agendaIntro, agenda };
}

function careersHelper(data) {
  let careersContent = null;
  if (data) {
    careersContent = { ...data[0] };
  }
  return careersContent;
}

function formHelper(data) {
  return dataCleanUp(data);
}

function regionsBuilder(data) {
  let regions = [];
  if (data) {
    regions = data.map((region) => {
      return { name: region.header, cities: region.card };
    });
  }
  return regions;
}
module.exports = {
  imageBuilder,
  speakerBuilder,
  mapArray,
  dataCleanUp,
  heroHelper,
  worldTourHeroHelper,
  sectionBuilder,
  sectionsHelper,
  agendaHelper,
  worldTourAgendaHelper,
  careersHelper,
  formHelper,
  objectCleanUp,
  dateHelper,
  pageCleanup,
  worldTourLandingHeroBuilder,
  regionsBuilder,
  faqItems,
  faqHelper,
  metaDataHelper,
};
