import {
  MetaPropertyCharset,
  MetaPropertyEquiv,
  MetaPropertyMicrodata,
  MetaPropertyName,
  MetaPropertyProperty,
  MetaInfo,
} from 'vue-meta/types/vue-meta';
import { NuxtI18nMeta } from '@nuxtjs/i18n/types/vue';
import { MetadataDTO } from '../models/page.dto';
import { createSchemaPage, createSchemaFaq } from './craftSchema';
import {
  DEFAULT_META_DESCRIPTION,
  DEFAULT_OPENGRAPH_IMAGE,
  DEFAULT_SCHEMA_ORG,
  META_NAME,
  SITE_URL,
  TWITTER_CARD_CONTENT,
  TWITTER_CREATOR_CONTENT,
  TWITTER_SITE_CONTENT,
} from '~/common/constants';
import { localizedPageNames } from '../common/localized-pages';

function buildSchemaOrg(schemaOrg: string, description: string) {
  const schemaScript = {
    hid: 'schemaOrg',
    innerHTML: '',
    type: 'application/ld+json',
  };

  if (schemaOrg) {
    schemaScript.innerHTML = schemaOrg;
  } else {
    const schema = {
      ...DEFAULT_SCHEMA_ORG,
      description: description || DEFAULT_SCHEMA_ORG.description,
    };

    schemaScript.innerHTML = JSON.stringify(schema);
  }

  return schemaScript;
}

function buildTopicsSchema({
  title,
  description,
  path,
  topicName,
  topicsHeader,
  dateModified,
  datePublished,
}: {
  title: string;
  description: string;
  path: string;
  datePublished?: string;
  dateModified?: string;
  topicName?: string;
  topicsHeader?: any;
}) {
  const schemaValues = {
    title,
    description,
    datePublished,
    dateModified: dateModified || datePublished,
    url: `${SITE_URL}${path}`,
    articleSection: topicName,
    timeRequired: topicsHeader?.data.read_time || '',
    image: '/nuxt-images/topics/gitops-topic.png',
  };

  return {
    hid: 'schemaTopics',
    json: createSchemaPage(schemaValues),
    type: 'application/ld+json',
  };
}

/**
 * This function verifies if the page has or does not have enabled the "nuxtI18nHead" as there is not a built-in way to do it.
 * When the option is not enabled and the nuxtI18nHead is used, the `href` attribute is the same for all positions, allowing to validate this if the page has enabled localization
 * @param nuxtI18nHead
 * @param path
 * Extra else if needed generate hreflang attributes with trailing slashes other than the homepage. See https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/2556.
 */
function getLocalizedMeta(
  path: string,
  nuxtI18nHead?: NuxtI18nMeta,
): Record<string, any>[] {
  const allMetaHrefs: string[] =
    nuxtI18nHead?.link.map(
      (metaObject) =>
        metaObject.href + `${metaObject.href.at(-1) !== '/' ? '/' : ''}`,
    ) ?? [];
  const uniqueMetaHrefs: string[] = [...new Set(allMetaHrefs)];
  const pageNameSections = path.split('/');
  const pageName = pageNameSections[pageNameSections.length - 2];
  const isTemplatePage =
    path.includes('/solutions/') || path.includes('/get-started/');
  const isLocalized = localizedPageNames.includes(pageName);

  if (!isLocalized && isTemplatePage) {
    const newMeta = {
      hid: 'i18n-alt-en-us',
      rel: 'alternate',
      hreflang: 'en-us',
      href: `${SITE_URL}${path}`,
    };
    return [newMeta];
  } else {
    const regexPathHasLang = /^\/[a-z]{2}-[a-z]{2}\//im;
    const newMeta: any[] = uniqueMetaHrefs.map((href) => {
      const langCodeMatch: string[] = href.match(regexPathHasLang) ?? [];
      const langCode =
        langCodeMatch.length > 0 ? langCodeMatch[0].slice(1, -1) : 'en-us';
      const hid = `i18n-alt-${langCode}`;
      const rel = 'alternative';
      const hreflang = langCode;
      const newHref = `${SITE_URL}${href}`;
      return { hid, rel, hreflang, href: newHref };
    });
    return newMeta;
  }
}

/* eslint-disable babel/camelcase */
export function getPageMetadata(
  {
    description = DEFAULT_META_DESCRIPTION,
    title,
    image_title: imageTitle,
    twitter_image: twitterImage,
    image_alt: imageAlt,
    schema_faq: schemaFaq,
    schema_org: schemaOrg,
    topic_name: topicName,
    topics_header: topicsHeader,
    date_published: datePublished,
    date_modified: dateModified,
    no_index: noindex,
  }: MetadataDTO,
  path: string,
  nuxtI18nHead?: NuxtI18nMeta,
): MetaInfo {
  const meta: (
    | MetaPropertyCharset
    | MetaPropertyEquiv
    | MetaPropertyName
    | MetaPropertyMicrodata
    | MetaPropertyProperty
    | any
  )[] = [];
  const script: any = [];

  meta.push({
    hid: META_NAME.description,
    name: META_NAME.description,
    content: description,
  });
  meta.push({
    hid: META_NAME.twitterDescription,
    name: META_NAME.twitterDescription,
    content: description,
  });

  meta.push({
    hid: META_NAME.ogDescription,
    property: META_NAME.ogDescription,
    content: description,
  });

  meta.push({
    hid: META_NAME.ogTitle,
    property: META_NAME.ogTitle,
    content: title,
  });

  meta.push({
    hid: META_NAME.twitterCreator,
    name: META_NAME.twitterCreator,
    content: TWITTER_CREATOR_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterSite,
    name: META_NAME.twitterSite,
    content: TWITTER_SITE_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterCard,
    name: META_NAME.twitterCard,
    content: TWITTER_CARD_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterAltImage,
    name: META_NAME.twitterAltImage,
    content: imageAlt,
  });

  meta.push({
    hid: META_NAME.ogImageAlt,
    property: META_NAME.ogImageAlt,
    content: imageAlt,
  });

  if (path) {
    meta.push({
      hid: META_NAME.ogUrl,
      name: META_NAME.ogUrl,
      content: `${SITE_URL}${path}`,
    });
  }

  if (title) {
    meta.push({
      hid: META_NAME.ogType,
      property: META_NAME.ogType,
      content: META_NAME.article,
    });
    meta.push({
      hid: META_NAME.twitterTitle,
      name: META_NAME.twitterTitle,
      content: title,
    });
  } else {
    meta.push({
      hid: META_NAME.ogType,
      content: META_NAME.website,
      property: META_NAME.ogType,
    });
  }

  if (noindex === true) {
    meta.push({
      hid: 'robots',
      name: 'robots',
      content: 'noindex, nofollow',
    });
  }

  if (imageTitle) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${imageTitle}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: `${SITE_URL}${imageTitle}`,
    });
  } else if (twitterImage) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${twitterImage}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: `${SITE_URL}${twitterImage}`,
    });
  } else {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
  }

  if (schemaFaq) {
    script.push({
      hid: 'schemaFaq',
      json: createSchemaFaq(schemaFaq),
      type: 'application/ld+json',
    });
  }

  if (path.includes('topics')) {
    script.push(
      buildTopicsSchema({
        title,
        description,
        path,
        topicName,
        topicsHeader,
        dateModified,
        datePublished,
      }),
    );
  }

  script.push(buildSchemaOrg(schemaOrg, description));

  const link: any = getLocalizedMeta(path, nuxtI18nHead);

  return {
    title: `${title} | GitLab`,
    meta,
    link,
    script,
    __dangerouslyDisableSanitizers: ['script', 'innerHTML'],
  };
}
