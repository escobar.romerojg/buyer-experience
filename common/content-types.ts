export const CONTENT_TYPES = Object.freeze({
  NEXT_STEPS: 'nextSteps',
  EVENT: 'event',
  HERO: 'eventHero',
  CARD_GROUP: 'cardGroup',
  CARD: 'card',
  CASE_STUDY: 'caseStudy',
  BLOCK_GROUP: 'blockGroup',
  EXTERNAL_VIDEO: 'externalVideo',
  ASSET: 'asset',
  VIDEO_CAROUSEL: 'videoCarousel',
  CODE_SUGGESTIONS_IDE: 'codeSuggestionsIde',
  CUSTOM_COMPONENT: 'customComponent',
  CUSTOMER_LOGOS: 'customerLogos',
  FAQ: 'faq',
  HEADER_AND_TEXT: 'headerAndText',
  PAGE: 'page',
  QUOTE: 'quote',
  SIDE_MENU: 'sideMenu',
  SIDE_NAVIGATION: 'sideMenu',
  TAB_CONTROLS_CONTAINER: 'tabControlsContainer',
  TOPICS: 'topics',
  TWO_COLUMN_BLOCK: 'twoColumnBlock',
});

export const NAV_CONTENT_TYPES = Object.freeze({
  FOOTER: 'footer',
  NAVIGATION: 'navigation',
});
