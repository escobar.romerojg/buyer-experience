---
  title: Solutions
  description: GitLab is The DevSecOps Platform that empowers organizations to deliver software faster, more efficiently, while strengthening security and compliance.
  hero:
    header: GitLab Solutions
    description: GitLab is The DevSecOps Platform that empowers organizations to deliver software faster, more efficiently, while strengthening security and compliance.
    primary_button:
      text: Try Ultimate for free
      href: https://gitlab.com/-/trials/new?_gl=1%2A1lt5wks%2A_ga%2AMTc4NzA4MjI4Ni4xNjc1OTYwMzk0%2A_ga_ENFH3X7M5Y%2AMTY4MTE0OTk0MC4xMjkuMS4xNjgxMTUwMTMwLjAuMC4w&glm_content=default-saas-trial&glm_source=about.gitlab.com%2Fpricing%2F
      data_ga_name: sales
      data_ga_location: hero
    secondary_button:
      href: /pricing/
      text: Learn about pricing
      data_ga_name: pricing
      data_ga_location: hero
    video:
      url: https://player.vimeo.com/video/702922416?h=06212a6d7c
      image: /nuxt-images/solutions/landing/solutions_video_thumbnail.png
  navigation:
    hide_link: true
    header:
      variant: heading4
    buttons:
      - text: By Solution
        icon_left: idea-collaboration
        icon_right: arrow-down
        href: '#by-solution'
        data_ga_location: anchor
      - text: By Company size
        icon_left: digital-transformation
        icon_right: arrow-down
        href: '#by-company-size'
        data_ga_location: anchor
      - text: By Industry
        icon_left: devsecops
        icon_right: arrow-down
        href: '#by-industry'
        data_ga_location: anchor
  by_solution:
    hero:
      header: Leading DevSecOps solutions to solve your company’s toughest challenges
      badges:
      - src: /nuxt-images/badges/enterpriseleader_fall2022.svg
        alt: G2 Enterprise Leader - Fall 2022
      - src: /nuxt-images/badges/midmarketleader_fall2022.svg
        alt: G2 Mid-Market Leader - Fall 2022
      - src: /nuxt-images/badges/smallbusinessleader_fall2022.svg
        alt: G2 Small Business Leader - Fall 2022
      - src: /nuxt-images/badges/bestresults_fall2022.svg
        alt: G2 Best Results - Fall 2022
      - src: /nuxt-images/badges/bestrelationshipenterprise_fall2022.svg
        alt: G2 Best Relationship Enterprise - Fall 2022
      - src: /nuxt-images/badges/bestrelationshipmidmarket_fall2022.svg
        alt: G2 Best Relationship Mid-Market - Fall 2022
      - src: /nuxt-images/badges/easiesttodobusinesswith_fall2022.svg
        alt: G2 Easiest To Do Business With Mid-Market - Fall 2022
      - src: /nuxt-images/badges/bestusability_fall2022.svg
        alt: G2 Best Usability - Fall 2022
    grid:

      solutions:
        - name: DevSecOps Platform
          icon: devsecops
          description: Balance speed and security by automating software delivery and securing your end-to-end software supply chain
          href: /platform/
        - name: Security and Governance
          icon: shield-check-light
          description: Integrate security into your DevSecOps lifecycle
          href: /solutions/security-compliance/
        - name: GitLab Duo
          description: Boost efficiency with the help of AI in every phase of the software development life cycle.
          icon: ai-code-suggestions
          href: /gitlab-duo/
        - name: Continuous integration & delivery
          description: Build, maintain, deploy, and monitor complex pipelines with advanced CI/CD
          icon: continuous-integration
          href: /solutions/continuous-integration/
        - name: Value stream management
          description: Measure and manage the business value of your DevSecOps lifecycle
          icon: visibility
          href: /solutions/value-stream-management/
        - name: Automated software delivery
          description: Build software faster and improve developer productivity by eliminating repetitive tasks
          icon: continuous-delivery
          href: /solutions/delivery-automation/
        - name: Agile development
          description: Plan and manage your projects with integrated Agile support
          icon: agile
          href: /solutions/agile-delivery/
        - name: Source code management
          description: Maximize productivity, with faster delivery and increased visibility
          icon: cog-code
          href: /stages-devops-lifecycle/source-code-management/
        - name: GitOps
          description: Increase the reliability and security of your cloud native, multi-cloud, and legacy environments
          icon: digital-transformation
          href: /solutions/gitops/
        - name: Continuous software compliance
          description: Build in compliance for less risk and greater efficiency
          icon: report
          href: /solutions/continuous-software-security-assurance/
        - name: Software supply chain security
          description: Ensure your software supply chain is secure and compliant
          icon: shield-check-light
          href: /solutions/supply-chain/
        - name: InnerSource
          description: Harness open source best practices and processes to work and collaborate more effectively
          icon: open-source
          href: /solutions/innersource/
        - name: GitLab Dedicated
          description: Combine the low overhead and effiency of a SaaS platform with the flexibility and privacy of self hosting
          icon: shield-check-light
          href: /dedicated/
        - name: Analytics and Insights
          description: Analyze, discover and optimize the hidden value within your DevSecOps lifecycle
          icon: open-source
          href: /solutions/analytics-and-insights/
  by_company_size:
    hero:
      header: GitLab helps organizations of all sizes ship secure software faster
      quote: GitLab allows us to collaborate very well with team members and between different teams. As a project manager, being able to track a project or the workload of a team member helps prevent a project from delays. And with GitLab, it all resides within one house.
      image:
        src: /nuxt-images/blogimages/iron-mountain-solutions-hero.jpeg
        alt: Iron Mountain image
      video:
          url: https://player.vimeo.com/video/762685637?h=f96e969756
          image: /nuxt-images/solutions/landing/featured_solution.png
      author:
        name: Jason Monoharan
        title: Jason Monoharan Technologies
        company:
          name: Iron Mountain
          url: /customers/iron-mountain/
    cards:
      - icon: remote-world-alt
        name: Enterprise
        description: Collaborate across your organization, ship secure code faster, and drive business results
        href: /enterprise/
      - icon: bulb-bolt
        name: Startups
        description: Grow your customer base and differentiate your product in the market
        href: /solutions/startups/
      - icon: collaboration
        name: Small business
        description: Bring teams together to help them iterate faster and innovate together
        href: /small-business/
    cta:
      text: Find out which pricing plan is best for your team
      primaryButton:
        text: GitLab Ultimate
        href: /pricing/ultimate/
      secondaryButton:
        text: GitLab Premium
        href: /pricing/premium/
  by_industry:
    hero:
      header: Learn how GitLab can help solve industry-specific challenges
      cards:
        - icon: piggy-bank-alt
          name: Finance
          description: Reduce security and compliance risk and accelerate time to market
          href: /solutions/finance/
        - icon: institution
          name: Public Sector
          description: Grow your customer base and differentiate your product in the market
          href: /solutions/public-sector/
        - icon: learn
          name: Education
          description: Help everyone at your institution work together more efficiently
          href: /solutions/education/
    showcase:
      categories:
        - name: 'Technology'
          cta:
            text: See more customer stories
            href: /customers/
          quote:
            logo: /nuxt-images/logos/hackerone-logo.png
            header: HackerOne achieves 5x faster deployments with GitLab’s integrated security
            text: GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.
            author:
              name: Mitch Trale
              title: Head of Infrastructure
              company: HackerOne
            metrics:
              - value: 4 Hours
                text: saved weekly per developer
              - value: 7.5x
                text: Faster pipeline time
            button:
              href: /customers/hackerone/
        - name: 'Financial Services'
          cta:
            text: See more customer stories
            href: /customers/
          quote:
            logo: /nuxt-images/enterprise/logo-goldman-sachs.svg
            header: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
            text: GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs.
            author:
              name: Andrew Knight
              title: Managing Director
              company: Goldman Sachs
            metrics:
              - value: 1,500+
                text: Adopters in the first two weeks
              - value: 99.9%
                text: Decrease in build time
            button:
              href: /customers/goldman-sachs/
        - name: 'Public Sector'
          cta:
            text: See more customer stories
            href: /customers/
          quote:
            logo: /nuxt-images/logos/usarmy.svg
            header: How the U.S. Army Cyber School created “Courseware as Code” with GitLab
            text: Instead of having to teach people 10 different things, they just learn one thing, which makes it much easier in the long run.
            author:
              name:  Chris Apsey
              title: Captain
              company: U.S. Army
            metrics:
              - value: 35x
                text: Faster review cycle time
              - value: 8,415
                text: Commits by students and contributors
            button:
              href: /customers/us_army_cyber_school/
        - name: 'Education'
          cta:
            text: See more customer stories
            href: /customers/
          quote:
            logo: /nuxt-images/logos/deakin-logo-1.png
            header: Deakin University cuts toolchain sprawl with GitLab
            text: One of the drivers for us to adopt GitLab was the number of different out-of-the-box security features that allowed us to replace other solutions and open source tools and therefore the skill sets that came along with them.
            author:
              name:  Aaron Whitehand
              title: Director of Digital Enablement
              company: Deakin University
            metrics:
              - value: 60%
                text: Reduction in manual tasks
              - value: 100%
                text: Of code in major projects scanned for quality
            button:
              href: /customers/deakin-university/
    logos:
      paginated: true
      rows_per_page: 3
      purple_background: true
      data_ga_location: by industry
      # partners property loaded by contentful
  badges:
    variant: light
    header: Loved by users. Recognized by analysts.
    tabs :
      - tab_name: Industry Analyst Research
        tab_id: research
        tab_icon: doc-pencil-alt
        copy: |
          What top Industry Analysts are saying about GitLab
        cta:
          url: /analysts
          cta_text: Learn more
          data_ga_name: analysts page
          data_ga_location: industry analyst research tab - badge section
        analysts:
          - logo: /nuxt-images/logos/forrester-logo.svg
            text: 'The 2019 Forrester Wave™: Cloud-Native Continuous Integration Tools'
            link:
              url: /analysts/forrester-cloudci19/
              data_ga_name: Forrester Cloud-Native Continuous Integration Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant for Enterprise Agile Planning Tools'
            link:
              url: /analysts/gartner-eapt21/
              data_ga_name: Gartner Magic Quadrant for Enterprise Agile Planning Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant™ for Application Security Testing'
            link:
              url: /analysts/gartner-ast22/
              data_ga_name: Gartner Magic Quadrant for Application Security Testing
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2021 Gartner® Market Guide for Value Stream Delivery Platforms'
            link:
              url: /analysts/gartner-vsdp21/
              data_ga_name: Gartner Market Guide for Value Stream Delivery Platforms
      - tab_name: G2 Leader in DevSecOps
        tab_id: leaders
        tab_icon: ribbon-check-alt-2
        copy: |
          GitLab ranks as a G2 Leader across DevSecOps categories
        badge_images:
          - src: /nuxt-images/badges/enterpriseleader_fall2022.svg
            alt: G2 Enterprise Leader - Fall 2022
          - src: /nuxt-images/badges/midmarketleader_fall2022.svg
            alt: G2 Mid-Market Leader - Fall 2022
          - src: /nuxt-images/badges/smallbusinessleader_fall2022.svg
            alt: G2 Small Business Leader - Fall 2022
          - src: /nuxt-images/badges/bestresults_fall2022.svg
            alt: G2 Best Results - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipenterprise_fall2022.svg
            alt: G2 Best Relationship Enterprise - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipmidmarket_fall2022.svg
            alt: G2 Best Relationship Mid-Market - Fall 2022
          - src: /nuxt-images/badges/easiesttodobusinesswith_fall2022.svg
            alt: G2 Easiest To Do Business With Mid-Market - Fall 2022
          - src: /nuxt-images/badges/bestusability_fall2022.svg
            alt: G2 Best Usability - Fall 2022
