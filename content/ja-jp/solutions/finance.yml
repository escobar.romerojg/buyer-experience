---
  title: 金融サービスでのGitLab
  description: ソーシャルコーディング、継続的インテグレーション、リリースの自動化により、開発が加速し、ソフトウェアの品質が向上し、ミッションの目標の達成に繋がることが証明されています。詳細をご確認ください！
  twitter_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  og_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  image_title: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: 金融サービスでのGitLab
        subtitle: 革新的な金融機関向けのDevSecOpsプラットフォーム
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimateを無料で試す
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: 価格設定のお問い合わせはこちら
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
          image_url_mobile: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
          alt: "画像：金融サービスでのGitLab"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: Goldman Sachs
            image: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
            url: /customers/goldman-sachs/
            aria_label: Goldman Sachsの顧客事例へのリンク
          - name: Bendigo and Adelaide Bank
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Bendigo and Adelaide Bankの顧客事例へのリンク
          - name: Credit Agricole
            image: /nuxt-images/customers/credit-agricole-logo-2.png
            url: /customers/credit-agricole/
            aria_label: Credit Agricoleの顧客事例へのリンク
          - name: Worldline
            image: /nuxt-images/logos/worldline-logo-mono.png
            url: /customers/worldline/
            aria_label: Worldlineの顧客事例へのリンク
          - name: Moneyfarm
            image: /nuxt-images/logos/moneyfarm_logo.png
            url: /customers/moneyfarm/
            aria_label: Moneyfarmの顧客事例へのリンク
          - name: UBS
            image: /nuxt-images/home/logo_ubs_mono.svg
            url: /blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: UBSの顧客事例へのリンク
    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: お客様の声
          href: '#testimonials'
        - title: 機能
          href: '# capabilities'
        - title: メリット
          href: '#benefits'
        - title: 事例
          href: '#case-studies'
      slot_enabled: true
      slot_offset: 2
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: セキュリティ、高速化、効率化を実現
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    header: セキュリティとコンプライアンスにおけるリスクを低減
                    text: 開発プロセスの早い段階で脆弱性とセキュリティの問題を検出し、コンプライアンス要件を遵守するためにポリシーを実施します。
                    link_text: DevSecOpsの詳細はこちら
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: メリット
                  - icon:
                      name: increase
                      alt: 増加アイコン
                      variant: marketing
                    header: 市場投入までの時間を短縮
                    text: 包括的なDevSecOpsプラットフォームを使用してソフトウェアデリバリープロセスを自動化することで、お客様により良い体験を提供でき、より速く新機能をお届けできます。
                    link_text: 詳細はこちら
                    link_url: /platform/
                    ga_name: free up learn more
                    ga_location: メリット
                  - icon:
                      name: piggy-bank-alt
                      alt: Piggy Bankアイコン
                      variant: marketing
                    header: 運用コストを削減
                    text: 単一のソリューションでツールチェーンの複雑さを軽減し、より優れたコラボレーション、生産性、イノベーションを可能にします。
                    link_text: 節約できる金額を計算
                    link_url: /calculator/roi/
        - name: 'div'
          id: 'お客様の声'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  金融サービス業界に信頼され 
                  <br />
                  開発者が愛用
                quotes:
                  - title_img:
                      url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
                      alt: Goldman Sachs
                    quote: 「GitLabを使用することで、エンジニアリング部門の開発速度を劇的に向上させることができました。今では、チームによっては、1日に1,000以上のCI機能ブランチのビルドを実行し、マージしています！」
                    author: Andrew Knight氏
                    position: Goldman Sachs社管理ディレクター
                    ga_carousel: financial services goldman sachs
                  - title_img:
                      url: /nuxt-images/home/logo_ubs_mono.svg
                      alt: UBSのロゴ
                    quote: |
                      「GitLabがなければ、このようなシームレスなワークフローは実現できませんでした。結果的に、多くの競合他社を凌駕し、コーディング、テスト、デプロイの間の障壁を無くすことができました。」
                    author: Rick Carey氏
                    position: UBS社グループチーフテクノロジーオフィサー
                    ga_carousel: financial services UBS
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: New10ロゴ
                    quote: 「GitLabはKubernetesやServerlessに対応し、DASTやSASTのような素晴らしいセキュリティ機能をサポートしているため、当社の非常にモダンなアーキテクチャで大変役立っています。GitLabのおかげで、最先端を行くアーキテクチャを実現できています。」
                    author: Kirill Kolyaskin氏
                    position: New10社最高技術責任者
                    ga_carousel: financial services new10
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: 金融サービス向けの包括的なDevSecOpsプラットフォーム
                sub_description: '以下の機能を使用して、顧客の財務上のニーズをより効率的かつ安全に支援できます。'
                white_bg: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                alt: 紹介する画像
                solutions:
                  - title: SBOMと依存関係
                    description: 各依存関係をインストールしたパッケージやソフトウェアライセンスなど、既知の脆弱性やその他の重要な詳細を含む、プロジェクトの依存関係またはソフトウェア部品表をわかりやすく表示します。
                    icon:
                      name: less-risk
                      alt: 低リスクアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: 包括的なセキュリティスキャン
                    description: SAST、シークレット検出、コンテナスキャン、クラスターイメージスキャン、DAST、IaCスキャン、APIファジングなどのビルトインセキュリティツールを使用して、脆弱性を検出し、アプリケーションを保護します。
                    icon:
                      name: monitor-pipeline
                      alt: パイプラインが表示されたモニターのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: Comprehensive security scanning
                    data_ga_location: solutions block
                  - title: ファジングテスト
                    description: GitLabのパイプラインにカバレッジガイド付きファジングテストを追加することで、他のQAプロセスで見逃す可能性のあるバグや潜在的なセキュリティイシューを発見します。
                    icon:
                      name: monitor-test
                      alt: テスト結果が表示されたモニターのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: コンプライアンスのための共通管理
                    description: 保護ブランチおよび保護環境からマージリクエストの承認、監査ログ、アーティファクト保持、および包括的なセキュリティスキャンまで、さまざまなGitLab機能を使用して、共通コントロールを自動化および実施します。
                    icon:
                      name: shield-check
                      alt: チェックマーク付きの盾アイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: "https://about.gitlab.com/solutions/financial-services-regulatory-compliance/"
                    data_ga_name: controls for compliance
                    data_ga_location: solutions block
                  - title: コンプライアンスに準拠したワークフローの自動化
                    description: コンプライアンスチームが構成した設定が保持され、コンプライアンスフレームワークとパイプラインと正しく連携していることを確認します。
                    icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: 金融サービスに適した ユニークなソリューション
                cards:
                  - title: ビルトインのセキュリティスキャン
                    description: 開発者がセキュリティの問題をすぐに見つけて修正できるようサポートします。GitLabの<a href="https://docs.gitlab.com/ee/user/application_security/">ビルトインのスキャナー</a>（SAST、DAST、コンテナスキャン、IaCスキャン、シークレット検出など）を使用できるほか、カスタムスキャナーを統合できます。
                    icon:
                      name: monitor-test
                      alt: テスト結果が表示されたモニターのアイコン
                      variant: marketing
                  - title: クラウドの活用をサポート
                    description: GitLabはクラウドネイティブアプリケーション用に設計されており、<a href="https://about.gitlab.com/solutions/kubernetes/">Kubernetes</a>、<a href="https://about.gitlab.com/partners/technology-partners/aws/">AWS</a>、<a href="https://about.gitlab.com/partners/technology-partners/google-cloud-platform/" >Google Cloud</a>、および<a href="https://about.gitlab.com/partners/technology-partners/#cloud-partners">その他のクラウドプロバイダー</a>と緊密に統合できます。
                    icon:
                      name: gitlab-cloud
                      alt: gitlab-cloudアイコン
                      variant: marketing
                  - title: オンプレミス、セルフホスティング、SaaS
                    description: GitLabはすべての環境で動作します。お好みに合わせてお選びください。
                    icon:
                      name: monitor-gitlab
                      alt: monitor-gitlabアイコン
                      variant: marketing

        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: 金融業界における成功事例
                rows:
                  - title: Goldman Sachs
                    subtitle: Goldman Sachs社で数十ものチームが24時間以内に本番環境にプッシュできている方法は？
                    image:
                      url: /nuxt-images/blogimages/Goldman_Sachs_case_study.jpg
                      alt: 通りから見た建物
                    button:
                      href: https://about.gitlab.com/customers/goldman-sachs/
                      text: 詳細はこちら
                      data_ga_name: goldman sachs learn more
                      data_ga_location: case-studies
                  - title: Moneyfarm
                    subtitle: ヨーロッパの資産管理会社であるMoneyfarmは、開発者の利用体験を向上させ、デプロイの回数を2倍に増やしました
                    image:
                      url: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
                      alt: 建物の窓に映った反射
                    button:
                      href: https://about.gitlab.com/customers/moneyfarm/
                      text: 詳細はこちら
                      data_ga_name: uw learn more
                      data_ga_location: case-studies
                  - title: Bendigo and Adelaide Bank
                    subtitle: オーストラリアで最も信頼されている銀行の1つがクラウドテクノロジーを採用し、運用効率を向上させた方法
                    image:
                      url: /nuxt-images/blogimages/bab_cover_image.jpg
                      alt: 白黒の建物
                    button:
                      href: https://about.gitlab.com/customers/bab/
                      text: 詳細はこちら
                      data_ga_name: bendigo and adelaide learn more
                      data_ga_location: case-studies
    - name: 'by-solution-link'
      data:
        title: 'GitLabイベント'
        description: "イベントに参加して、セキュリティとコンプライアンスを強化しながら、チームがソフトウェアデリバリーをスピーディかつ効率的に行えるようになる方法を学びましょう。2022年は多数のイベントを主催、運営、または参加予定です。皆さまとお会いできるのを楽しみにしています！"
        link: /events/
        image: /nuxt-images/events/google-cloud-next/pubsec-event-link.jpeg
        alt: プレゼンテーション時の群衆
        icon: calendar-alt-2