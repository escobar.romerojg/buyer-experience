---
  title: GitLabが実践するインナーソース
  description: インナーソースとは、オープンソースでの開発手法を使って、組織内でオープンソースの文化を確立することです。詳細をご確認ください!
  template: 'industry'
  no_gradient: true
  nextstep_variant: 5
  components:
    - name: 'solutions-hero'
      data:
        title: GitLabが実践するインナーソース
        subtitle: 統合された開発者ポータルを使用して、オープンソースにおけるベストプラクティスとプロセスを活用し、より効果的に作業およびコラボレーションを行いましょう。
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/stock3.png
          image_url_mobile: /nuxt-images/solutions/stock3.png
          alt: "画像: 公共部門向けGitLab"
          bordered: true
    - name: 'side-navigation-variant'
      links:
        - title: メリット
          href: '#benefits'
        - title: 機能
          href: '#capabilities'
        - title: 価格設定
          href: '#pricing'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: インナーソースを選ぶ理由
                subtitle: |
                  パフォーマンスの高い開発チームとエンジニアリングチームは、オープンソースのプロセスや文化を導入して、開発者の体験を向上し、コラボレーションを改善しています。チームは、GitLabを内部の開発ポータルとして使用しています。
                link:
                    text: インナーソースの詳細について
                    href: https://about.gitlab.com/topics/version-control/what-is-innersource/
                    ga_name: innersource topic
                    ga_location: benefits
                is_accordion: true
                items:
                  - icon:
                      name: speed-alt-2
                      alt: speed-alt-2
                      variant: marketing
                    header: より優れたソフトウェアをより迅速に構築
                    text: ユニットテスト、コードカバレッジ、継続的インテグレーション、ビルトインのエンドツーエンドのセキュリティにより、コードをより速く強化し、改善することができます。
                  - icon:
                      name: docs-alt
                      alt: ドキュメントアイコン
                      variant: marketing
                    header: 包括的なドキュメントを作成
                    text: コードは、MRではコメントと変更の提案を使用して、またプロセスのWikiやイシュー、Readmeでより適切にドキュメント化されます。
                  - icon:
                      name: stopwatch
                      alt: ストップウォッチのアイコン
                      variant: marketing
                    header: 効率性の向上を達成
                    text: コード、アーキテクチャ、およびアセットはチームと組織全体で検出し、利用できます。
                  - icon:
                      name: user-collaboration
                      alt: user-collaborationアイコン
                      variant: marketing
                    header: コラボレーションを改善
                    text: 単一のプラットフォームを使用することで、組織全体のチーム内およびチーム間のコードレビュー、コミュニケーション、および貢献の摩擦が軽減されます。
                  - icon:
                      name: user-laptop
                      alt: ユーザーとノートパソコンのアイコン
                      variant: marketing
                    header: チームメンバーのエクスペリエンスを強化
                    text: サイロ化を解消でき、生産性と満足度が上がるので、チームメンバーの維持と採用を改善できます。
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'solutions-by-solution-list'
              data:
                title: GitLabが実践するインナーソース
                icon: slp-check-circle-alt
                header: "GitLabは、開発者の体験を合理化し、楽しいものにします。それにより、ソフトウェア開発チームは品質を改善し、効率性を向上させ、共同作業を推進し、イノベーションを加速することができます。 次のような機能を利用できる統合された内部向け開発者ポータルと、GitLabの単一のDevSecOpsプラットフォームアプローチによって、開発者の体験を最適化します。"
                items: 
                  - カスタマイズ可能なReadmeと手順書による、プロセスと製品のドキュメント化
                  - | 
                    [マージリクエスト](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge request" data-ga-location="capabilities"}による、一元化され、共同作業を行いやすいコードレビュー
                  - | 
                    [Wiki](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wikis" data-ga-location="capabilities"}によるナレッジ管理
                  - | 
                    [イシュー](https://docs.gitlab.com/ee/user/project/issues){data-ga-name="issues" data-ga-location="capabilities"}と[スレッドディスカッション](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="threaded discussions" data-ga-location="capabilities"}でアイデアの提案、コミュニケーション、および更新
                  - 包括的なAPIドキュメント
                  - テストカバレッジの結果の可視化
                  - UIに直接収められた、VSコードをベースとした強力なWeb IDE
                  - ネイティブCLI
                footer: すべてが、計画段階から本番稼働まで作業を行うプラットフォームに格納されています。
                cta:
                  text: インナーソースの効果を発揮する方法を学ぶ
                  video: https://www.youtube.com/embed/ZS1mCpBHXaI
                  ga_name: innersource unlock
                  ga_location: capabilities
                  
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: 最適なプランを見つけましょう
                cta:
                  url: /pricing/
                  text: 価格設定の詳細について
                  data_ga_name: pricing learn more
                  data_ga_location: pricing
                  aria_label: 価格設定
                tiers:
                  - id: free
                    title: Free
                    items:
                      - SASTとシークレット検出
                      - JSONファイルの調査
                  - id: premium
                    title: Premium
                    items:
                      - Freeの機能に加え、以下の機能が含まれます
                      - 承認とガードレール
                    link:
                      href: /pricing/premium/
                      text: Premiumの詳細はこちら
                      data_ga_name: premium learn more
                      data_ga_location: pricing
                      aria_label: Premiumプラン
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Premiumの機能に加え、以下の機能が含まれます
                      - DAST、APIファジング、ライセンスコンプライアンスなど
                      - SBOMと依存関係リスト
                      - 脆弱性管理
                      - コンプライアンス管理
                    link:
                      href: /pricing/ultimate
                      text: Ultimateの詳細はこちら
                      data_ga_name: ultimate learn more
                      data_ga_location: pricing
                      aria_label: Ultimateプラン
                    cta:
                      href: /free-trial/
                      text: Ultimateを無料で試す
                      data_ga_name: try ultimate
                      data_ga_location: pricing
                      aria_label: Ultimateプラン