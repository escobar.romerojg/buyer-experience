---
  title: Plateforme
  description: Découvrez comment la plateforme GitLab peut aider les équipes à collaborer et à créer des logiciels plus rapidement.
  hero: 
    note: La plateforme DevSecOps
    header: Alimentée par l'IA
    sub_header: La plus complète
    description: Développez plus rapidement des logiciels de meilleure qualité sur une seule plateforme pour l'ensemble du cycle de vie de livraison de vos logiciels
    image: 
      src: /nuxt-images/platform/loop-sheild-duo.svg
      alt: Le cycle de vie DevOps (planifier, coder, compiler, tester, livrer une release, déployer, exploiter et effectuer le suivi) est représenté par un symbole de l'infini chevauchant le bouclier de sécurité.
    button:
      text: Commencer un essai gratuit
      href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: En savoir plus sur la tarification
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero

  features:
    title: Des équipes performantes grâce à une plateforme unique
    subtitle: Un DevSecOps simplifié grâce à une application intégrée unique utilisable par toutes les équipes et à toutes les étapes du cycle de vie du logiciel
    view_all_link:
      view_all_text: Découvrir les solutions
      view_all_url: /solutions/
      data_ga_name: solutions
      data_ga_location: body
    cards: 
      - name: Intelligence artificielle et apprentissage automatique
        description: Rendez les flux de travail de livraison de logiciels existants plus intelligents, en améliorant la productivité et l'efficacité.
        icon: ai-summarize-mr-review
        href: /gitlab-duo/
        data_ga_name: gitlab duo
        data_ga_location: features cards
      - name: Sécurité de la chaîne logistique logicielle
        description: Créez une nomenclature des logiciels (SBOM), vérifiez la conformité des licences et automatisez la sécurité tout au long du processus de développement afin de réduire les risques et les retards.
        icon: shield-check-large
        href: /solutions/dev-sec-ops/
        data_ga_name: devsecops
        data_ga_location: features cards
      - name: La gestion de la chaîne de valeur
        description: Fournissez des analyses avec des données exploitables à tous les intervenants au sein de l'organisation, offrant une visibilité à chaque étape de l'idéation et du développement.
        icon: visibility
        href: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: features cards
      - name: Gestion du code source
        description: Contrôle de versions pour aider votre équipe de développement à collaborer et à maximiser la productivité, en accélérant la livraison et en augmentant la visibilité.
        icon: cog-code
        href: /stages-devops-lifecycle/source-code-management/
        data_ga_name: source code management
        data_ga_location: features cards
      - name: Intégration et livraison continues (CI/CD)
        description: Automatisation pour compiler, tester et déployer votre code dans votre environnement de production.
        icon: continuous-integration
        href: /features/continuous-integration/
        data_ga_name: continuous integration
        data_ga_location: features cards
      - name: GitOps
        description: Automatisation et collaboration au niveau de l'infrastructure pour les environnements cloud-native, multicloud et hérités.
        icon: digital-transformation
        href: /solutions/gitops/
        data_ga_name: digital transformation
        data_ga_location: features cards
      - name: Gestion de projet et de portefeuille en mode agile
        description: Planifiez, lancez, hiérarchisez et gérez les initiatives en matière d'innovation avec une visibilité complète sur le travail effectué par vos équipes.
        icon: agile
        href: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: features cards
    double_card:
      name: Découvrez le forfait qui convient le mieux à votre équipe
      icon: pricing-plans
      cta_one_text: GitLab Ultimate
      cta_one_link: /pricing/ultimate/
      data_ga_name_one: ultimate
      data_ga_location_one: features cards
      cta_two_text: GitLab Premium
      cta_two_link: /pricing/premium/
      data_ga_name_two: premium
      data_ga_location_two: features cards

  table_footer_text: En savoir plus sur nos fonctionnalités
  table_footer_link: /features/
  table_footer_data_ga_name: features
  table_footer_data_ga_location: table

  study_card:
    blurb: Étude Forrester
    header: GitLab a permis un retour sur investissement de 427%
    stats:
      - value: x12
        text: augmentation du nombre de versions de release annuelles
      - value: <6
        text: mois de période d'amortissement
      - value: 87%
        text: amélioration de l'efficacité du développement et de la livraison
    button: 
      href: https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html
      text: Lire l'étude
      data_ga_name: forrester study
      data_ga_location: body

  benefits:
    title: Une seule plateforme
    subtitle: pour les équipes dev, sec et ops
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: image du code source
    is_accordion: true
    cta:
      icon: time-is-money
      title: Combien vous coûte votre chaîne d'outils actuelle ?
      text: En savoir plus
      link: /calculator/roi/
      data_ga_name: roi calculator
      data_ga_location: body
    items:
      - header: Le meilleur DevSecOps de sa catégorie
        text: 
          Une solution DevSecOps tout-en-un avec sécurité intégrée sur toute la plateforme 
          

          Le modèle de données unique permet d'obtenir des insights sur l'ensemble du cycle de vie DevSecOps
      - header: Flux de travail alimentés par l'IA
        text: 
          Améliorez l'efficacité de vos processus et réduisez les durées de cycle de chaque utilisateur à l'aide de l'IA à chaque phase du cycle de vie du développement logiciel, de la planification et de la création de code aux tests, à la sécurité et à la surveillance
      - header: Flexibilité de déploiement
        text:
          Offre SaaS pour les clients qui souhaitent utiliser GitLab en tant que service  
          

          GitLab Auto-géré pour les clients qui souhaitent contrôler le déploiement
          

          GitLab Dedicated, une offre SaaS à locataire unique, pour les clients ayant des besoins en matière d'isolation et de résidence des données
      - header: Cloud agnostique
        text:
          Déployez n'importe où, activant une stratégie multi-cloud 
          

          Évitez l'enfermement propriétaire, aucun traitement préférentiel d'un seul fournisseur de services Cloud
      - header: Expérience utilisateur intégrée
        text:
          Réduisez le changement de contexte avec une expérience intégrée sur une seule plateforme 
          

          Intégration plus rapide et réduction des coûts de formation continue avec une seule plateforme sur l'ensemble du cycle de vie de livraison des logiciels
      - header: Modèle de base ouvert
        text:
          Innovez rapidement grâce à notre modèle de développement à noyau ouvert où tout le monde peut contribuer 
          

          Une itération plus rapide à mesure que nous construisons avec nos clients et notre communauté
      - header: Pas de coûts inattendus
        text:
          Tarification fixe sans frais supplémentaires pour les fonctionnalités complémentaires

  quotes_carousel_block:
    controls:
      prev: étude de cas précédente
      next: étude de cas suivante
    quotes:
      - main_video: https://www.youtube.com/embed/sT85nT9X2mM
        logo:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: 'Nasdaq'
        url: customers/nasdaq/
        header: Comment <a href='customers/nasdaq/' data-ga-name='nasdaq' data-ga-location='case studies'>Nasdaq</a> réalise sa transformation Cloud avec GitLab Ultimate
        data_ga_name: Nasdaq
        data_ga_location: case studies
        cta_text: Regarder la vidéo
        modal: true
        url: https://www.youtube.com/embed/sT85nT9X2mM
        company: Nasdaq
      - quote: "GitLab réalise tout ce que nous voulons, que ce soit en matière de sécurité, de performance, de tests, etc."
        author: Chintan Parmar
        logo:
          url: /nuxt-images/logos/dunelm.svg
          alt: 'logo dunelm'
        role: Ingénieur de plateforme principal
        statistic_samples:
          - data:
              highlight: 275%
              subtitle: augmentation des déploiements hebdomadaires
        url: /customers/dunelm/
        header: Pourquoi <a href='/customers/dunelm/' data-ga-name='dunelm' data-ga-location='case studies'>Dunelm</a> a choisi GitLab pour renforcer la sécurité
        data_ga_name: dunelm case study
        data_ga_location: case studies
        cta_text: En savoir plus
        company: Dunelm
      - logo:
          url: /nuxt-images/logos/deutsche-telekom-logo-01.jpg
          alt: 'logo Deutsche Telekom'
        role: Chef d'entreprise IT, hub CI/CD
        quote: Le délai de mise sur le marché était un enjeu important pour nous. Avant le début de notre transformation vers Agile et DevOps, nous avions des cycles de release de près de 18 mois dans certains cas. Nous avons été en mesure de réduire considérablement ce délai, passant à environ 3 mois.
        author: Thorsten Bastian
        statistic_samples:
          - data:
              highlight: 6x
              subtitle: plus rapide en matière de mise sur le marché
          - data:
              highlight: 13 000
              subtitle: utilisateurs GitLab actifs
        url: /customers/deutsche-telekom/
        header: <a href='/customers/deutsche-telekom/' data-ga-name='deutsche telekom' data-ga-location='case studies'>Deutsche Telekom :</a> transformation DevSecOps avec GitLab
        data_ga_name: deutsche telekom
        data_ga_location: case studies
        cta_text: En savoir plus
        company: Deutsche Telekom
      - logo:
          url: /nuxt-images/logos/carfax-logo.png
          alt: 'logo carfax'
        quote: L'approche DevSecOps met toujours l'accent sur la sécurité. Elle intervient au premier plan de chaque étape du processus.
        author: Mark Portofe
        role: Directeur de l'ingénierie plateforme
        statistic_samples:
          - data:
              highlight: 20%
              subtitle: augmentation des déploiements en glissement annuel
          - data:
              highlight: 30%
              subtitle: des vulnérabilités détectées plus tôt au cours du cycle de vie du développement logiciel
        url: /customers/carfax/
        header: <a href='/customers/carfax/' data-ga-name='carfax' data-ga-location='case studies'>CARFAX</a> améliore la sécurité, réduit la gestion des pipelines et les coûts grâce à GitLab
        data_ga_name: carfax
        data_ga_location: case studies
        cta_text: En savoir plus
        company: CARFAX
      - logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: 'logo iron mountain'
        role: Vice-président de la technologie d'entreprise
        quote: GitLab nous a fourni les bases et la plateforme nécessaires pour mettre en place notre cadre Agile à grande échelle. Nous sommes en mesure de collaborer au sein de nos équipes informatiques d'entreprise et de nos principales parties prenantes.
        author: Hayelom Tadesse
        statistic_samples:
          - data:
              highlight: 150 000 USD
              subtitle: d'économies de coûts réalisées par an en moyenne
          - data:
              highlight: 20 heures
              subtitle: de gain de temps d'intégration par projet
        url: /customers/iron-mountain/
        header: <a href='/customers/iron-mountain/' data-ga-name='iron mountain' data-ga-location='case studies'>Iron Mountain</a> fait évoluer son DevOps avec GitLab Ultimate
        data_ga_name: iron mountain
        data_ga_location: case studies
        cta_text: En savoir plus
        company: Iron Mountain

  report_cta:
    layout: "light"
    statistics:
      - number: 50%+
        text: Fortune 100
      - number: 30m+
        text: Utilisateurs enregistrés
    title: GitLab est la plateforme leader DevSecOps
    reports:
    - description: "GitLab reconnu comme unique leader dans The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2\_2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      image: /nuxt-images/logos/forrester-logo.svg
      data_ga_name: forrester
      data_ga_location: reports section
    - description: GitLab reconnu comme un leader dans le Gartner® Magic Quadrant 2023 dédié aux plateformes DevOps
      url: /gartner-magic-quadrant/
      image: /nuxt-images/logos/gartner-logo.svg
      data_ga_name: gartner
      data_ga_location: reports section
