---
  title: Déclaration de confidentialité de GitLab
  description: "Sur cette page, vous trouverez des informations sur la déclaration de confidentialité de GitLab. Apprendre encore plus!"
  side_menu:
    anchors:
      text: "Sur cette page"
      data:
        - text: Quelles Données à caractère personnel GitLab collecte-t-elle à mon sujet?
          href: "#what-personal-data-does-gitlab-collect-about-me"
          data_ga_name: what personal data does gitlab collect about me
          data_ga_location: side-navigation
        - text: Quelles Données à caractère personnel GitLab ne collecte-t-elle pas?
          href: "#what-personal-data-is-not-collected-by-gitlab"
          data_ga_name: what personal data is not collected by gitlab
          data_ga_location: side-navigation
        - text: Comment GitLab utilise-t-elle mes Données à caractère personnel?
          href: "#how-does-gitlab-use-my-personal-data"
          data_ga_name: how does gitlab use my personal data
          data_ga_location: side-navigation
        - text: À qui GitLab communique-t-elle mes Données à caractère personnel?
          href: "#with-whom-does-gitlab-share-my-personal-data"
          data_ga_name: with whom does gitlab share my personal data
          data_ga_location: side-navigation
        - text: Partage des Données à caractère personnel au-delà des frontières nationales
          href: "#sharing-personal-data-across-national-boarders"
          data_ga_name: sharing personal data across national boarders
          data_ga_location: side-navigation
        - text: Comment GitLab protège-t-elle mes Données à caractère personnel?
          href: "#how-does-gitlab-secure-my-personal-data"
          data_ga_name: how does gitlab secure my personal data
          data_ga_location: side-navigation
        - text: Conservation des données
          href: "#data-retention"
          data_ga_name: data retention
          data_ga_location: side-navigation
        - text: Quels sont mes droits et mes choix en matière de Données à caractère personnel?
          href: "#what-are-my-rights-and-choices-regarding-personal-data"
          data_ga_name: what are my rights and choices regarding personal data
          data_ga_location: side-navigation
        - text: Droits en matière de confidentialité en Californie
          href: "#us-state-privacy-rights"
          data_ga_name: us state privacy rights
          data_ga_location: side-navigation
        - text: Modifications de la Déclaration
          href: "#statement-changes"
          data_ga_name: statement changes
          data_ga_location: side-navigation
        - text: Nous contacter
          href: "#contact-us"
          data_ga_name: contact us
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
  hero_title: Déclaration de confidentialité de GitLab
  disclaimer:
    text: |
      Cette traduction est fournie à titre informatif uniquement.
    details: |
      En cas de divergence entre le texte anglais et cette traduction, la version anglaise prévaudra.
  last_updated: "Dernière mise à jour : 10 juin 2022"
  copy: |
    Chez GitLab, nous accordons beaucoup d’importance à la confidentialité et à la sécurité de vos informations. La présente déclaration de confidentialité (« Déclaration de confidentialité ») énonce comment GitLab B.V. et GitLab, Inc. (« GitLab », « nous », « notre », « nos ») traitent vos données à caractère personnel. Les « Données à caractère personnel », telles qu’utilisées dans la présente Déclaration de confidentialité, sont des informations qui identifient ou peuvent raisonnablement être liées directement ou indirectement à une personne identifiable. Les pratiques et normes de confidentialité détaillées dans la présente Déclaration de confidentialité s’appliquent à toutes les personnes concernées dans le monde, sauf indication contraire spécifique. En particulier, la présente Déclaration de confidentialité aborde les sections suivantes :

    - [Quelles Données à caractère personnel GitLab collecte-t-elle à mon sujet ?](https://about.gitlab.com/privacy/#what-personal-data-does-gitlab-collect-about-me)

    - [Quelles Données à caractère personnel GitLab ne collecte-t-elle pas ?](https://about.gitlab.com/privacy/#what-personal-data-is-not-collected-by-gitlab)

    - [Comment GitLab utilise-t-elle mes Données à caractère personnel ?](https://about.gitlab.com/privacy/#how-does-gitlab-use-my-personal-data)

    - [À qui GitLab communique-t-elle mes Données à caractère personnel ?](https://about.gitlab.com/privacy/#with-whom-does-gitlab-share-my-personal-data)

    - [Comment GitLab protège-t-elle mes Données à caractère personnel ?](https://about.gitlab.com/privacy/#how-does-gitlab-secure-my-personal-data)

    - [Quels sont mes droits et mes choix en matière de Données à caractère personnel ?](https://about.gitlab.com/privacy/#what-are-my-rights-and-choices-regarding-personal-data)

    - [Droits en matière de confidentialité en Californie](https://about.gitlab.com/privacy/#california-privacy-rights)

    - [Autres informations importantes sur la confidentialité](https://about.gitlab.com/privacy/#other-important-privacy-information)


    La présente Déclaration de confidentialité s’applique aux sites Web de GitLab (« Sites Web »), GitLab.com (« SaaS »), Autogérés (« Autogérés ») et aux produits et services logiciels supplémentaires, collectivement dénommés « Services ».
  sections:
    - header: Quelles Données à caractère personnel GitLab collecte-t-elle à mon sujet?
      id: what-personal-data-does-gitlab-collect-about-me
      text: |
        Les catégories de Données à caractère personnel collectées par GitLab changent en fonction des Services que vous utilisez et du caractère gratuit ou payant de ces Services. Nous avons précisé ci-après quels services sont en corrélation avec le traitement dans chaque catégorie de Données à caractère personnel.

        ### Informations que vous fournissez directement

        Nous collectons les Données à caractère personnel que vous nous fournissez, par exemple :

        _Informations de compte :_ lorsque vous créez un compte avec GitLab, nous collectons des informations qui vous identifient, telles que votre nom, votre nom d’utilisateur, votre adresse électronique et votre mot de passe. Ces données sont collectées pour les utilisateurs gratuits et payants du produit SaaS.

        _Informations de profil :_ nous recueillons des informations que vous fournissez volontairement dans votre profil d’utilisateur ; cela peut inclure votre avatar public (qui peut être une photo), d’autres adresses électroniques, le nom de l’entreprise/organisation, l’intitulé du poste, le pays, les poignées de réseaux sociaux et la biographie. Veuillez noter que ces informations peuvent être visibles aux autres utilisateurs des Services et au public en fonction des paramètres de confidentialité que vous appliquez. Ces données sont collectées pour les utilisateurs gratuits et payants du produit SaaS.

        _Informations de paiement :_ si vous achetez un abonnement payant auprès de GitLab, nous collecterons des informations de paiement auprès de vous, qui peuvent inclure votre nom, votre adresse de facturation et vos informations de carte de crédit ou de banque. Nous pouvons également utiliser vos informations de carte de crédit pour vérifier votre identité et prévenir l’utilisation abusive de nos pipelines. Veuillez noter que GitLab ne traite ni ne stocke directement votre numéro de carte de crédit, mais nous transmettons ces informations à nos processeurs de paiement tiers pour traitement. Ces données sont collectées pour les utilisateurs payants des produits Autogérés et SaaS.

        _Informations de contact marketing :_ si vous effectuez une requête pour que GitLab vous contacte, ou si vous vous inscrivez pour recevoir des supports marketing ou des événements, GitLab peut collecter des informations telles que le nom, l'adresse, l'adresse électronique, le numéro de téléphone, le nom de l'entreprise et la taille de l'entreprise. Ces données peuvent être collectées par le biais des Sites Web ainsi que par l'utilisation des produits Autogérés ou SaaS.

        _Informations sur le titulaire de la licence :_ nous collectons le nom du titulaire de la licence, son adresse électronique et d'autres informations similaires associées à la personne qui reçoit une clé de licence pour les utilisateurs payants du produit Autogéré.

        _Le contenu que vous fournissez dans le cadre de l'utilisation des Services :_  les exemples de contenu que nous collectons et stockons incluent, sans s'y limiter : le résumé et la description ajoutés à un ticket, vos dépôts, les validations, les contributions au projet, les métadonnées du profil, les données d'activité et les commentaires. Le contenu comprend également tout code, fichier et lien que vous téléversez sur les Services. Ces données sont collectées pour les utilisateurs gratuits et payants du produit SaaS.

        _Support client et Services professionnels :_ si vous contactez le support client de GitLab ou si vous recevez des services professionnels, nous collecterons des informations vous concernant associées à votre compte et aux requêtes que vous effectuez ou les services qui vous sont fournis. Les informations relatives au support client sont collectées par le biais des Sites Web, tels que le [forum de la communauté GitLab](https://forum.gitlab.com/) et le [portail de support GitLab](https://support.gitlab.com/hc/en-us).

        _Enregistrements d'appels :_ nous pouvons enregistrer et transcrire des appels de vente hébergés par diverses technologies de vidéoconférence afin de permettre à nos équipes de vente et de support de partager des insights sur les conversations, de créer des formations et des présentations, et d'améliorer leurs processus internes.

        _Autres contenus que vous envoyez :_ nous pouvons également collecter d'autres contenus que vous envoyez à nos Services. Par exemple : les avis, les commentaires et les articles de blog, ou lorsque vous participez à des fonctionnalités interactives, enquêtes, concours, promotions, tirages au sort, activités ou événements. Lorsque vous participez à des canaux interactifs, nous pouvons collecter et traiter des informations à des fins d'analyse démographique. Cette collecte n'est pas liée à des produits spécifiques, mais peut être effectuée par le biais des Sites Web.


        ### Informations à propos de votre utilisation des Services que nous collectons automatiquement

        Nous pouvons collecter certaines Données à caractère personnel automatiquement par le biais de votre utilisation des Services, par exemple :


        _Informations sur l'appareil et identifiants :_ lorsque vous accédez à nos Services et que vous les utilisez, nous collectons automatiquement des informations à propos de votre appareil, qui peuvent inclure : le type d'appareil, le système d'exploitation de votre appareil, le type et la version du navigateur, les préférences linguistiques, l'adresse IP, les identificateurs de matériel et les identificateurs mobiles. Ces informations peuvent être collectées lors de l'utilisation des Services.

        _Données d'abonnement :_ nous pouvons collecter automatiquement des informations à propos du nombre d'utilisateurs actifs, des calendriers de licence, de l'historique du nombre d'utilisateurs et de l'adresse IP. Ces données sont collectées pour les instances payantes Autogérées et SaaS. Les détails des données d'abonnement se trouvent dans le [dictionnaire des métriques](https://metrics.gitlab.com/?q=subscription).

        _Données d'utilisation des Services :_ les données d'utilisation des Services sont divisées en deux catégories : les Données facultatives et les Données opérationnelles. Les Données facultatives sont des métriques agrégées concernant l'activité et l'utilisation des fonctionnalités qui fournissent des insights sur le succès des étapes et des fonctionnalités. Les Données opérationnelles sont des métriques agrégées qui permettent de suivre la manière dont la valeur est fournie par l'utilisation des Services et de fournir des insights sur la mise en œuvre optimale pour le client. Les deux catégories de données d'utilisation des Services peuvent être liées au nom d'hôte de l'instance, mais les informations ne contiennent aucune autre donnée individuelle sur l'utilisateur. Ces données sont collectées pour les utilisateurs gratuits et payants des produits Autogérés et SaaS. Pour plus d'informations à propos des données collectées et de la manière de configurer vos préférences, veuillez consulter la page [Données d'utilisation des Services](https://about.gitlab.com/handbook/legal/privacy/services-usage-data/).

        _Données d'événements :_ outre les données d'utilisation des Services, GitLab utilise également des analyses d'événements, telles que la durée de navigation, les clics sur les pages et les pages vues, sous une forme pseudonymisée, afin de disposer d'insights sur le comportement des utilisateurs de bout en bout. Ces données sont collectées pour les produits SaaS et Autogérés.

        _Données d'utilisation du Site Web :_ lorsque vous visitez nos Sites Web, nous enregistrons automatiquement des informations à propos de votre interaction avec les sites, telles que le site de référence, la date et l'heure de la visite, ainsi que les pages que vous avez consultées ou les liens sur lesquels vous avez cliqué.

        _Cookies et technologies de suivi similaires :_ GitLab utilise des cookies et des technologies similaires pour fournir des fonctionnalités, telles que le stockage de vos paramètres, et pour vous reconnaître lorsque vous utilisez nos Services. En outre, nous utilisons des cookies pour collecter des informations afin de fournir des publicités basées sur les centres d'intérêt qui vous sont adaptées en fonction de votre activité en ligne. Veuillez consulter notre [Politique en matière de cookies](https://about.gitlab.com/privacy/cookies/) pour en savoir plus à propos de nos pratiques et des contrôles que nous vous offrons.

        _Marketing par courriel :_ lorsque nous vous envoyons des courriels, ils peuvent inclure une technologie telle qu'une balise Web, qui nous indique votre type d'appareil, votre client de messagerie et si vous avez reçu et ouvert un courriel, ou si vous avez cliqué sur les liens contenus dans le courriel.

        _Boutons, outils et contenu d'autres sociétés :_ les Services peuvent inclure des liens ou des boutons vers des services tiers tels que Facebook et Twitter. Nous pouvons collecter des informations à propos de l'utilisation que vous faites de ces fonctionnalités. En outre, lorsque vous voyez ou interagissez avec ces boutons, outils ou contenus, certaines informations de votre navigateur peuvent être automatiquement envoyées à l'autre société. Veuillez lire la déclaration de confidentialité de cette société pour plus d'informations.

        ### Informations provenant de tiers et de partenaires

        Nous pouvons collecter des Données personnelles auprès d'autres parties de la manière suivante :

        _Fournisseurs et partenaires :_ nous pouvons recevoir des informations vous concernant de la part de tiers tels que des fournisseurs, des revendeurs, des partenaires ou des sociétés affiliées. Par exemple, nous recevons de nos revendeurs des informations à propos de vous et de vos commandes, ou nous pouvons compléter les données que nous collectons par des informations démographiques obtenues sous licence auprès de tiers afin de personnaliser les Services et les offres que nous vous proposons. De même, nos équipes de vente, de marketing et de recrutement peuvent recevoir l'accès à des bases de données tierces contenant des informations permettant d'enrichir les contacts professionnels et d'autres données d'entreprise, ou nous pouvons recevoir des données d'écoute sociale de la part de sociétés qui surveillent les publications publiques.

        _Services de connexion de tiers :_ GitLab vous permet de vous connecter à nos Services en utilisant des comptes tiers, tels que Facebook ou Google. Lorsque vous donnez votre autorisation, GitLab recevra des informations à propos de vous à partir de votre compte tiers, telles que le nom, l'adresse électronique, la localisation et des informations démographiques.

        _Autres utilisateurs des Services :_ d'autres utilisateurs des Services peuvent fournir des informations à propos de vous lorsqu'ils envoient des tickets et des commentaires, ou nous pouvons recevoir des informations lorsque vous êtes désigné en tant que représentant ou administrateur sur le compte de votre société.

        Lorsque vous êtes invité à fournir des Données à caractère personnel, vous pouvez refuser. Vous pouvez également utiliser les commandes du navigateur Web ou du système d'exploitation pour empêcher certains types de collecte automatique de données. Toutefois, si vous choisissez de ne pas fournir ou autoriser les informations nécessaires à certains produits ou fonctionnalités, ces produits ou fonctionnalités peuvent ne pas être disponibles ou ne pas fonctionner correctement.
    - header: Quelles Données à caractère personnel GitLab ne collecte-t-elle pas?
      id: what-personal-data-is-not-collected-by-gitlab
      text: |
        GitLab ne collecte pas intentionnellement de Données à caractère personnel sensibles, telles que des numéros de sécurité sociale, des données génétiques, des informations sur la santé ou des informations religieuses. Bien que GitLab n'effectue pas de requête ou de collecte intentionnelle de Données à caractère personnel sensibles, nous sommes conscients que les utilisateurs peuvent stocker ce type d'informations dans un espace de stockage GitLab.

        GitLab ne collecte pas intentionnellement les Données à caractère personnel des individus qui sont stockées dans les espaces de stockage des utilisateurs ou dans d'autres entrées de contenu libre. Si les Données personnelles sont stockées dans un espace de stockage de l'utilisateur, le propriétaire de l'espace de stockage est responsable de leur traitement.

        Si vous êtes un enfant de moins de 13 ans, vous ne pouvez pas avoir de compte. À l'exception des licences éducatives, GitLab ne collecte pas de manière volontaire des informations auprès d'enfants de moins de 13 ans, et ne dirige aucun de ses Services vers eux. Si nous apprenons ou avons des raisons de soupçonner qu'un utilisateur a moins de 13 ans, nous fermerons le compte de l'enfant.
    - header: Comment GitLab utilise-t-elle mes Données à caractère personnel?
      id: how-does-gitlab-use-my-personal-data
      text: |
        GitLab utilise vos Données à caractère personnel aux fins suivantes :

        - Créer votre compte, identifier et authentifier votre accès aux Services et vous fournir les Services pour lesquels vous avez effectué une requête ;
        - Traiter votre paiement pour les Services que vous avez achetés ;
        - Comprendre comment nos Services sont utilisés et les améliorer ;
        - Fournir des expériences personnalisées ;
        - Effectuer de la recherche et du développement auprès des utilisateurs ;
        - Vous envoyer des informations importantes à propos des Services ;
        - Vous envoyer les informations concernant une requête que vous avez effectuée ;
        - Vous envoyer de la publicité, du contenu marketing, des offres, des promotions, des lettres d'information, des enquêtes ou d'autres informations ;
        - Vous fournir un forum pour commenter ou discuter des Services ;
        - Créer des productions numériques et gérer des événements ;
        - Fournir des formations et des possibilités d'apprentissage ;
        - Activer pour les partenaires la possibilité d'enregistrer et de suivre les transactions ;
        - Fournir des recommandations de charge de travail et de revue de code par le biais de l'apprentissage automatique ;
        - Répondre à vos requêtes de support client ;
        - Améliorer la sécurité de nos Services et résoudre les éventuels problèmes, le cas échéant, pour exécuter le contrat régissant votre utilisation de nos applications ou pour communiquer avec vous ;
        - Détecter, prévenir ou traiter la fraude et les abus afin de vous protéger, de protéger GitLab et les tiers ;
        - Faire respecter les conditions légales qui régissent nos Services ;
        - Nous conformer à nos obligations légales ;
        - Protéger les droits, la sécurité et la propriété de GitLab, de vous ou de tout tiers ; et
        - À d'autres fins, pour lesquelles nous obtenons votre consentement.

        ### Base légale pour le traitement de vos Données à caractère personnel

        Lorsque notre traitement est soumis aux lois internationales, y compris, mais sans s'y limiter, le Règlement général sur la protection des données (« RGPD ») qui régit les individus situés dans l'Espace économique européen (« EEE »), nous collectons et traitons vos Données à caractère personnel en utilisant une ou plusieurs des bases légales suivantes énoncées par la loi applicable :

        _Exécution d'un contrat :_ nous utilisons vos Données à caractère personnel pour fournir les Services auxquels vous avez souscrit, et pour compléter et administrer le contrat que vous avez conclu avec GitLab, qui comprend le [Contrat d'Abonnement](https://about.gitlab.com/handbook/legal/subscription-agreement/), les [Conditions d'utilisation du Site Web](https://about.gitlab.com/handbook/legal/policies/website-terms-of-use/), et tout accord de traitement des informations de paiement.

        _Intérêts légitimes :_ nous utilisons vos Données à caractère personnel dans le cadre de nos intérêts légitimes, notamment pour vous fournir un contenu pertinent, communiquer avec des prospects, améliorer nos produits et services, ainsi qu'à des fins administratives, de sécurité, de prévention de la fraude et à des fins juridiques. Vous pouvez vous opposer à tout moment au traitement de vos Données à caractère personnel à ces fins.

        _Consentement :_ nous pouvons utiliser vos Données à caractère personnel, avec votre consentement, à des fins spécifiques telles que le marketing, les enquêtes, les enregistrements d'appels et la recherche. Vous pouvez à tout moment retirer votre consentement pour une finalité spécifique ou vous opposer au traitement de vos Données à caractère personnel.

        _Respect d'une obligation légale :_ nous pouvons utiliser vos Données à caractère personnel dans le cadre de réclamations légales, de conformité réglementaire et d'audits.
    - header: À qui GitLab communique-t-elle mes Données à caractère personnel?
      id: with-whom-does-gitlab-share-my-personal-data
      text: |
        Nous pouvons partager chacune des catégories de Données à caractère personnel que nous collectons avec les types de tiers décrits ci-dessous, aux fins commerciales suivantes :

        _Partage avec les utilisateurs et le public :_ nous pouvons partager vos Données à caractère personnel avec d'autres utilisateurs des Services et avec le public si vous choisissez de rendre votre Profil SaaS public. Vous pouvez contrôler les informations qui sont rendues publiques. Pour modifier vos paramètres, accédez à la rubrique [Paramètres de l'utilisateur](https://gitlab.com/-/profile) dans votre profil. Vous devez également savoir que toute information que vous partagez dans le cadre d'un projet, d'un blog, d'un site Web, etc. peut être disponible publiquement. Vous devez en tenir pleinement compte lorsque vous interagissez avec les Services.

        _Partage avec les comptes gérés et les administrateurs :_ si vous avez créé un compte GitLab avec l'adresse électronique de votre société, nous pouvons partager vos Données à caractère personnel avec votre société si celle-ci entame une relation commerciale avec GitLab. Dans ce cas, votre utilisation du logiciel et votre compte sont soumis aux conditions et à tout accord de protection des données entre votre société et GitLab.

        Dans le cas où vous changez l'adresse électronique de votre compte en passant d'une adresse électronique professionnelle à une adresse électronique personnelle et que, par la suite, votre société entame une relation commerciale avec GitLab, vos Données à caractère personnel associées à ce compte ne seront pas partagées avec votre société. GitLab ne liera pas un compte à une société sur la base de l'utilisation rétroactive d'une adresse électronique professionnelle.

        En outre, si vous choisissez de devenir membre d'un projet, votre nom d'utilisateur, votre adresse électronique, votre adresse IP, la date à laquelle l'accès a été accordé, la date à laquelle l'accès expire et votre rôle d'accès seront partagés avec les propriétaires du groupe de ce projet.

        _Partage avec les prestataires de services :_ nous partageons vos Données à caractère personnel avec nos prestataires de services. Ce sont des sociétés qui fournissent des services en notre nom, tels que l'hébergement de nos Services, le marketing, la publicité, le social, l'analyse, la gestion des tickets de support, le traitement des cartes de crédit, la sécurité et d'autres services similaires. Ces sociétés sont soumises à des exigences contractuelles qui régissent la sécurité et la confidentialité de vos informations.

        Par exemple, nous utilisons des fournisseurs de services d'analyse, tels que Google Analytics, pour nous aider à comprendre le fonctionnement et les performances de nos Services. Pour en savoir plus à propos de la manière dont Google utilise et partage les données qu'il collecte par l'intermédiaire de ses services, veuillez consulter <https://www.google.com/policies/privacy/partners/>. Par ailleurs, veuillez consulter notre [page Sous-traitants](https://about.gitlab.com/privacy/subprocessors/#third-party-sub-processors) pour voir la liste de nos Sous-traitants auxquels nous faisons appel afin de fournir un support client et d'héberger les Services.

        _Partage avec des partenaires et des revendeurs :_ GitLab travaille avec des tiers qui fournissent des services de vente, de conseil, de support et des services techniques pour nos Services. Lorsque cela est autorisé et avec votre consentement (si nécessaire), nous pouvons partager vos données avec ces partenaires et revendeurs.

        _Partage avec des Sociétés affiliées :_ GitLab partagera les informations collectées avec des [sociétés détenues et gérées par nous](https://about.gitlab.com/company/visiting/).

        _Partage pour fraude et prévention des abus :_ nous pouvons partager vos informations lorsque nous pensons de bonne foi que la divulgation est nécessaire pour prévenir la fraude, l'abus de nos services, nous défendre contre les attaques, et protéger la sécurité de GitLab et de nos utilisateurs.

        _Application de la loi :_ GitLab peut divulguer des Données à caractère personnel ou d'autres informations que nous collectons à propos de vous aux forces de l'ordre si cela est requis en réponse à une citation à comparaître valide, une ordonnance du tribunal, un mandat de perquisition, un ordre gouvernemental similaire, ou lorsque nous pensons en toute bonne foi que la divulgation est nécessaire pour se conformer à nos obligations légales, pour protéger notre propriété ou nos droits, ou ceux de tiers ou du public en général.

        _Fusion ou acquisition :_ nous pouvons partager vos Données à caractère personnel si nous sommes impliqués dans une fusion, une vente ou une acquisition de personnes morales ou d'unités commerciales. Si un tel changement de propriétaire se produit, nous veillerons à ce que cela se fasse dans des conditions qui préservent la confidentialité de vos Données à caractère personnel, et nous vous en informerons sur notre site Web ou par courriel avant tout transfert de vos Données à caractère personnel.
    - header: Partage des Données à caractère personnel au-delà des frontières nationales
      id: sharing-personal-data-across-national-boarders
      text: |
        Nos Services sont hébergés aux États-Unis et les informations que nous collectons seront stockées et traitées sur nos serveurs aux États-Unis. Nos employés, contractants, organisations affiliées, prestataires de services et sous-traitants qui traitent les Données à caractère personnel peuvent être situés aux États-Unis ou dans d’autres pays en dehors de votre pays d’origine. Si vous résidez dans l'EEE, au Royaume-Uni ou en Suisse et que nous transférons des informations à propos de vous vers une juridiction qui n'a pas été jugée par la Commission européenne comme disposant de protections adéquates des données, nous utiliserons les garanties et les mécanismes juridiques disponibles pour contribuer à garantir vos droits et protections, y compris l'utilisation de Clauses contractuelles types ou l'obtention de votre consentement.
    - header: Comment GitLab protège-t-elle mes Données à caractère personnel ?
      id: how-does-gitlab-secure-my-personal-data
      text: |
        Nous déployons les plus grands efforts pour protéger vos Données à caractère personnel. Nous utilisons des contrôles de sécurité administratifs, techniques et physiques, le cas échéant, pour protéger vos informations. Pour plus d'informations sur nos pratiques de sécurité, veuillez consulter les [Mesures de sécurité techniques et organisationnelles pour GitLab.com](https://about.gitlab.com/handbook/security/security-assurance/technical-and-organizational-measures.html).
    - header: Conservation des données
      id: data-retention
      text: |
        Afin de protéger vos Données à caractère personnel, GitLab ne conservera vos Données à caractère personnel que tant que votre compte est actif ou si nécessaire pour exécuter nos obligations contractuelles, vous fournir les Services, nous conformer aux obligations légales, résoudre les litiges, préserver les droits légaux ou faire respecter nos accords.

        GitLab se réserve le droit de supprimer les comptes, projets, espaces de nommage et contenus associés inactifs. GitLab peut considérer un compte, un projet ou un espace de nommage comme inactif sur la base de divers critères, y compris, mais sans s'y limiter, la date de création du compte, la dernière fois qu'il y a eu une connexion valide et la date de la dernière contribution. Si nous prévoyons de supprimer votre compte ou vos projets, nous vous en informerons à l'avance en envoyant un message à l'adresse électronique enregistrée sur votre compte. GitLab vous encourage à utiliser votre compte régulièrement pour éviter le risque d'être considéré comme inactif.
    - header: Quels sont mes droits et mes choix en matière de Données à caractère personnel?
      id: what-are-my-rights-and-choices-regarding-personal-data
      text: |
        Vous disposez d'un droit d'accès, de rectification, de limitation ou de suppression de vos Données à caractère personnel, ainsi que de portabilité de vos Données à caractère personnel vers une autre société. Bien que ces droits puissent varier d'une juridiction à l'autre, GitLab vous offre les mêmes droits et les mêmes choix, quel que soit votre lieu de résidence. Nous accordons ces droits gratuitement, sauf si vos requêtes sont manifestement infondées et excessives.

        Vous pouvez exercer vos choix et vos droits de la manière suivante :

        _Vous désinscrire du marketing par courriel :_ vous pouvez vous désinscrire du marketing par courriel en cliquant sur le lien « se désabonner » situé au bas de tout courriel de marketing que vous recevez ou en visitant notre [centre de préférences](https://about.gitlab.com/company/preference-center/) et en vous désabonnant. Vous pouvez continuer à recevoir des courriels transactionnels à propos de votre compte et le Service après vous être désabonné.

        _Désactiver la publicité basée sur les centres d'intérêt :_ si vous souhaitez désactiver la publicité basée sur les centres d'intérêt, veuillez consulter la Politique en matière de cookies pour voir vos options.

        _Demander une copie de vos informations :_ vous pouvez [effectuer une requête pour obtenir une copie des Données à caractère personnel](https://about.gitlab.com/handbook/gdpr/) dont dispose GitLab à propos de vous.

        _Mettre à jour vos informations :_ si vous avez déjà un compte, vous pouvez accéder à vos informations de profil d'utilisateur, les mettre à jour ou les modifier en vous connectant à votre compte et en mettant à jour les paramètres de votre profil.

        _Supprimer votre compte :_ si vous souhaitez uniquement supprimer votre compte SaaS, vous pouvez le faire en vous connectant à votre compte et en accédant à l'option « Supprimer mon compte » dans les paramètres de votre profil. Si vous souhaitez supprimer vos Données à caractère personnel dans tous les systèmes, y compris votre compte, vous devez remplir un [formulaire pour effectuer une requête de Données à caractère personnel](https://support.gitlab.io/account-deletion/) et sélectionner « Suppression de compte (complète) » dans la liste déroulante « Type de demande ». Si votre compte est lié à une Société qui a établi une relation commerciale avec GitLab, vous devrez demander à l'administrateur de votre Société de supprimer votre compte de l'espace de nommage de cette société avant que nous puissions le supprimer. Une fois que votre compte n'est plus associé à l'espace de nommage de cette société, GitLab traitera votre demande de suppression conformément à la procédure décrite dans les présentes.

        Veuillez noter qu'en raison de la nature open source de nos Services, nous pouvons conserver indéfiniment des Données à caractère personnel limitées afin de fournir un historique transactionnel. Par exemple, si vous fournissez vos informations dans le cadre d'un article de blog ou d'un commentaire, nous pouvons afficher ces informations même si vous avez supprimé votre compte, car nous ne supprimons pas automatiquement les articles de la communauté. De même, si vous contribuez à un projet public (qui n'appartient pas à GitLab) et que vous fournissez vos Données à caractère personnel dans le cadre de cette contribution, vos Données à caractère personnel seront intégrées et affichées publiquement avec votre contribution, et nous ne serons pas en mesure de les supprimer ou de les effacer, car cela entraînerait une rupture du projet.

        Une exception aux informations intégrées dans un projet public se produit lorsque vos Données à caractère personnel sont ajoutées par vous ou quelqu'un d'autre à la section des commentaires d'un projet public. Dans ce cas, vos Données à caractère personnel seront expurgées, car la suppression de ces informations dans la seule section des commentaires n'interrompra pas le projet.

        Si vous contribuez à un projet appartenant à GitLab en commentant ou en créant un ticket ou une requête de fusion et que vous fournissez vos Données à caractère personnel dans le cadre de cette contribution, vos Données à caractère personnel associées à votre contribution seront supprimées et attribuées à un utilisateur fantôme. Toutefois, veuillez noter que si le contenu de la contribution contient des Données à caractère personnel, ces informations demeureraient et vous devrez effectuer une requête spécifique pour que ces informations soient supprimées.

        _Transférer vos projets :_ vous pouvez transférer vos projets en utilisant la [fonctionnalité d'exportation](https://docs.gitlab.com/ee/user/project/settings/import_export.html) fournie dans le produit SaaS, qui inclura également toutes les métadonnées, ou en clonant vos espaces de stockage. Pour transférer les informations de votre profil, vous pouvez utiliser l'[API](https://docs.gitlab.com/ee/api/users.html#for-user).

        _Informer votre autorité de surveillance :_ dans le cas improbable où vous ne seriez pas d'accord avec le traitement de votre requête, vous avez le droit de déposer une plainte auprès de l'autorité de contrôle compétente dans votre juridiction.
    - header: Droits en matière de confidentialité en Californie
      id: us-state-privacy-rights
      text: |
        Si vous résidez en Californie, veuillez consulter l'[avis de confidentialité de GitLab en Californie](https://about.gitlab.com/handbook/legal/privacy/california-privacy-notice.html) pour une description spécifique de vos droits en matière de confidentialité et des pratiques de collecte en vertu de la loi californienne sur la protection de la vie privée des consommateurs (California Consumer Privacy Act).
    - header: Modifications de la Déclaration
      id: statement-changes
      text: |
        GitLab peut modifier sa Déclaration de confidentialité de façon ponctuelle. Le cas échéant, nous mettrons à jour la date figurant en haut de la présente Déclaration. Si nous décidons d'apporter une modification importante à notre Déclaration de confidentialité, nous publierons un avis de mise à jour sur la page d'accueil de notre Site Web. Nous pouvons également vous informer par courriel de toute modification significative apportée à notre Déclaration de confidentialité.
    - header: Nous contacter
      id: contact-us
      text: |
        Vos informations sont gérées par GitLab B.V. et GitLab Inc. Si vous avez des questions ou des préoccupations à propos de la manière dont nous traitons vos Données à caractère personnel, veuillez nous envoyer un courriel avec pour objet « Préoccupation en matière de confidentialité » à `DPO@gitlab.com`.
