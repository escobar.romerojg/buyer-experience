---
  title: Die DevSecOps-Plattform
  description: 'Von der Planung bis zur Produktion: Bringe Teams in einer einzigen Anwendung zusammen. Stelle sicheren Code effizienter bereit, um schneller Mehrwert zu liefern.'
  schema_org: >
    {"@context":"https://schema.org","@type": "Corporation","name":"GitLab","legalName":"GitLab Inc.","tickerSymbol":"GTLB","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png", "description" : "GitLab ist die DevOps-Plattform, die es Unternehmen ermöglicht, die Gesamtrendite aus der Softwareentwicklung zu maximieren, indem sie Software schneller und effizienter bereitstellen und gleichzeitig Sicherheit und Konformität stärken. Mit GitLab kann jedes Team in deinem Unternehmen gemeinsam Software planen, erstellen, sichern und bereitstellen, um Geschäftsergebnisse schneller, mit vollständiger Transparenz, Konsistenz und Rückverfolgbarkeit zu erzielen. GitLab ist ein Open Core-Unternehmen, das Software für den Softwareentwicklungslebenszyklus mit 30 Millionen geschätzten registrierten Benutzer(inne)n und mehr als 1 Million aktiven Lizenzbenutzer(inne)n entwickelt und über eine aktive Community von mehr als 2.500 Mitwirkenden verfügt. GitLab teilt offen mehr Informationen als die meisten Unternehmen und ist standardmäßig öffentlich. Dies bedeutet, dass unsere Projekte, Strategie, Richtung und Metriken offen diskutiert werden und auf unserer Website zu finden sind. Unsere Werte sind Collaboration (Zusammenarbeit), Results (Ergebnisse), Efficiency (Effizienz), Diversity (Vielfalt), Inclusion (Inklusion) & Belonging (Zugehörigkeit), Iteration und Transparancy (Transparenz) (CREDIT). Diese Werte prägen unsere Kultur.","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"slogan": "Wir haben es uns zur Aufgabe gemacht, alle kreativen Arbeiten vom reinen Lesezugriff auf den Lese-/Schreibzugriff umzustellen, damit jeder seinen Beitrag leisten kann.","address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"awards": "Comparably's Best Engineering Team 2021, 2021 Gartner Magic Quadrant für Application Security Testing – Challenger, DevOps Dozen Award für den besten DevOps-Lösungsanbieter im Jahr 2019, 451 Firestarter Award von 451 Research","knowsAbout": [{"@type": "Thing","name": "DevOps"},{"@type": "Thing","name": "CI/CD"},{"@type": "Thing","name": "DevSecOps"},{"@type": "Thing","name": "GitOps"},{"@type": "Thing","name": "DevOps Platform"}],"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}
  hero_scroll_gallery:
    controls:
      prev: previous slide
      next: next slide
    slides:
      - type: block
        title: Software. Schneller.
        blurb: GitLab ist die umfassendste KI-gestützte DevSecOps-Plattform.
        primary_button:
          text: Kostenlos testen
          link: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "hero"
      - type: card
        image:
          src: "/nuxt-images/home/hero/hero-1.png"
          alt: "Markenimage von GitLab-Boards"
        blurb: Branchenführer entscheiden sich für GitLab, um missionskritische Software zu entwickeln
        button:
          text: Gründe, die für GitLab sprechen
          link: https://about.gitlab.com/why-gitlab/
          data_ga_name: why gitlab
          data_ga_location: hero
      - type: card
        theme: dark
        image:
          src: "/nuxt-images/home/hero/hero-2.png"
          alt: "Markenimage von GitLab-Boards"
        blurb: KI-gestützte Workflows steigern die Effizienz und reduzieren die Bearbeitungszeiten
        button:
          text: Das ist GitLab Duo
          link: https://about.gitlab.com/gitlab-duo/
          data_ga_name: duo
          data_ga_location: hero
      - type: card
        image:
          src: "/nuxt-images/home/hero/hero-3.png"
          alt: "Markenimage von GitLab-Boards"
        blurb: GitLab zum Marktführer für DevOps-Plattformen ernannt
        button:
          text: Bericht lesen
          link: https://about.gitlab.com/gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: hero
      - type: card
        image:
          src: "/nuxt-images/home/hero/value-stream-img.svg"
          alt: ""
        blurb: Erfahre mehr darüber, was dein Team mit der KI-gestützten DevSecOps-Plattform erreichen könnte.
        button:
          text: "Vertrieb kontaktieren"
          link: /sales
          data_ga_name: "sales"
          data_ga_location: "hero"
  customer_logos_block:
    text:
      legend: Diese Unternehmen vertrauen uns
      url: "/customers/"
      data_ga_name: "trusted by"
      data_ga_location: "home case studies block"
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link zur T-Mobile and GitLab Webcast-Startseite"
        alt: "T-Mobile Logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
        size: sm
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link zur Kunden-Fallstudie von Goldman Sachs
        alt: "Goldman Sachs Logo"
        url: "/customers/goldman-sachs/"
        size: sm
      - image_url: "/nuxt-images/software-faster/airbus-logo.png"
        link_label: Link zur Kunden-Fallstudie von Airbus
        alt: "Airbus Logo"
        url: "/customers/airbus/"
      - image_url: "/nuxt-images/logos/lockheed-martin.png"
        link_label: Link zur Kunden-Fallstudie von Lockheed Martin
        alt: "Lockheed Martin Logo"
        url: /customers/lockheed-martin/
        size: lg
      - image_url: "/nuxt-images/logos/carfax-logo.png"
        link_label: Link zur Carfax Kunden-Fallstudie
        alt: "Carfax-Logo"
        url: /customers/carfax/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link zur Kunden-Fallstudie von Nvidia
        alt: "Nvidia Logo"
        url: /customers/nvidia/
      - image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        link_label: Link zum Blogbeitrag Wie UBS mithilfe von GitLab eine eigene DevOps-Plattform erstellt hat
        alt: "UBS logo"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
  intro_block:
    title: Balance zwischen Geschwindigkeit und Sicherheit auf einer einzigen Plattform
    blurb: Automatisiere die Softwarebereitstellung, steigere die Produktivität und schütze deine gesamte Software-Lieferkette.
    primary_button:
      href: "/platform/"
      text: "Entdecke die Plattform"
      data_ga_name: "discover the platform"
      data_ga_location: "body"
    secondary_button:
      href: "#"
      text: "Was ist GitLab?"
      data_ga_name: "what is gitlab"
      data_ga_location: "body"
      modal:
          video_link: https://player.vimeo.com/video/799236905?h=4eee39a447&badge=0&autopause=0&player_id=0&app_id=58479
          video_title: Was ist GitLab?
    image:
      src: "/nuxt-images/home/hero/developer-productivity-img.svg"
      alt: "Markenimage von GitLab-Boards"
  blurb_grid:
    - icon:
        name: devsecops
        size: md
        variant: marketing
      title: Vereinfache deine Toolchain
      blurb: Alle <a data-ga-name="platform" data-ga-location="body" href="/platform/">wichtigen DevSecOps-Tools</a> an einem Ort.
    - icon:
        name: agile-large
        size: md
        variant: marketing
      title: Beschleunige die Softwarebereitstellung
      blurb: Automatisierung, <a data-ga-name="gitlab-duo" data-ga-location="body" href="/gitlab-duo/">KI-gestützte Workflows</a> und mehr.
    - icon:
        name: shield-check-large
        size: md
        variant: marketing
      title: Integriere Sicherheit
      blurb: Sicherheit, die <a data-ga-name="security-compliance" data-ga-location="body" href="/solutions/security-compliance/">integriert ist, nicht aufgesetzt</a>.
    - icon:
        name: gitlab-cloud
        size: md
        variant: marketing
      title: Überall bereitstellen
      blurb: Verabschiede dich von der <a data-ga-name="multicloud" data-ga-location="body" href="/topics/multicloud/ ">Bindung an einen bestimmten Cloud-Anbieter</a>.
  feature_showcase:
    - logo:
        src: /nuxt-images/logos/lockheed-martin.png
        alt: ''
      icon:
        name: stopwatch
        size: md
        variant: marketing
      blurb: Erfahre, wie <a data-ga-name="lockheed-martin" data-ga-location="body" href="/customers/lockheed-martin/">Lockheed Martin</a> mit GitLab Zeit, Geld und technische Ressourcen spart
      button:
        text: Geschichte lesen
        href: "/customers/lockheed-martin/"
        data_ga_name: read story lockheed martin
        data_ga_location: body
      stats:
        - value: 80 x
          blurb: schnellere CI-Pipeline-Builds
          icon:
            name: arrow-up
            variant: product
            size: md
        - value: 90 %
          blurb: weniger Zeitaufwand für die Systemwartung
          icon:
            name: arrow-down
            size: md
            variant: product
    - title: Wie viel kostet dich deine Toolchain?
      button:
        text: Teste unseren ROI-Rechner
        href: "/calculator/roi/"
        data_ga_name: roi calculator
        data_ga_location: body
      icon:
        name: time-is-money-alt
        size: md
        variant: marketing
    - title: Du bist neu bei GitLab und weißt nicht, wo du anfangen sollst? 
      button:
        text: Ressourcen erkunden
        href: "/get-started/"
        data_ga_name: get started
        data_ga_location: body
      icon:
        name: documents
        size: md
        variant: marketing
  tabbed_features:
    header: DevSecOps
    blurb: Von der Planung bis zur Produktion – GitLab bringt dein Team zusammen
    tabs:
      - header: Entwicklung
        header_mobile: Dev
        data_ga_name: development
        data_ga_location: devsecops
        blurb: Automatisierte Aufgaben verbessern die Effizienz und entlasten die Entwickler – ohne die Sicherheit zu beeinträchtigen.
        features:
          - icon:
              name: continuous-integration
              size: md
              variant: marketing
            text: Kontinuierliche Integration und Bereitstellung
            href: "/solutions/continuous-integration/"
            data_ga_name: ci
            data_ga_location: development
          - text: KI-gestützte Workflows mit GitLab Duo
            icon:
              name: ai-code-suggestions
              size: md
              variant: marketing
            href: "/gitlab-duo/"
            data_ga_name: ai
            data_ga_location: development
          - text: Sourcecodeverwaltung
            icon:
              name: cog-code
              size: md
              variant: marketing
            href: "/solutions/source-code-management/"
            data_ga_name: source code management
            data_ga_location: development
          - text: Automatisierte Softwarebereitstellung
            icon:
              name: automated-code
              size: md
              variant: marketing
            href: "/solutions/delivery-automation/"
            data_ga_name: automated software delivery
            data_ga_location: development
      - header: Sicherheit
        header_mobile: Sec
        blurb: Verwalte Bedrohungsvektoren und stelle die Einhaltung von Konformitätsstandards sicher, ohne die Geschwindigkeit zu beeinträchtigen.
        data_ga_name: security
        data_ga_location: devsecops
        features:
          - icon:
              name: shield-check-light
              size: md
              variant: marketing
            text: Sicherheit und Konformität
            href: "/solutions/security-compliance/"
            data_ga_name: security and compliance
            data_ga_location: security
          - text: Sicherheit der Software-Lieferkette
            icon:
              name: clipboard-check-alt
              size: md
              variant: marketing
            href: "/solutions/supply-chain/"
            data_ga_name: supply chain
            data_ga_location: security
          - text: Integriertes Testen und Beheben
            icon:
              name: monitor-test
              size: md
              variant: marketing
            href: "/solutions/continuous-software-security-assurance/"
            data_ga_name: continuous software security assurance
            data_ga_location: security
          - text: Verwaltung von Sicherheitslücken und Abhängigkeiten
            icon:
              name: monitor-pipeline
              size: md
              variant: marketing
            href: "/solutions/compliance/"
            data_ga_name: compliance
            data_ga_location: security
        image:
          src: "/nuxt-images/home/devsecops/security.svg"
          alt: "Markenimage von GitLab-Boards"
      - header: Betrieb
        header_mobile: Ops
        blurb: 'Automatisiere die Software-Bereitstellung und optimiere Prozesse, egal in welcher Umgebung: Cloud Native, Multi-Cloud oder Legacy.'
        data_ga_name: operations
        data_ga_location: devsecops
        image:
          src: "/nuxt-images/home/devsecops/operations.svg"
          alt: "Markenimage von GitLab-Boards"
        features:
          - icon:
              name: digital-transformation
              size: md
              variant: marketing
            text: GitOps und Infrastructure-as-Code
            href: "/solutions/gitops/"
            data_ga_name: gitops
            data_ga_location: operations
          - text: Automatisierte Softwarebereitstellung
            icon:
              name: automated-code
              size: md
              variant: marketing
            href: "/solutions/delivery-automation/"
            data_ga_name: delivery automation
            data_ga_location: operations
          - text: Wertschöpfungsketten-Management
            icon:
              name: ai-value-stream-forecast
              size: md
              variant: marketing
            href: "/solutions/value-stream-management/"
            data_ga_name: value stream management
            data_ga_location: operations
  recognition_spotlight:
    heading: GitLab ist die führende DevSecOps-Plattform
    stats:
      - value: Mehr als 50 %
        blurb: Fortune 100
      - value: Über 30 Mio.
        blurb: registrierte Benutzer(innen)
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: Gartner Logo
        text: 'GitLab ist ein Leader im Gartner® Magic Quadrant™ für DevOps-Plattformen 2023'
        link:
          text: Bericht lesen
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: Forrester Logo
        text: "GitLab ist der einzige Leader der Studie The Forrester Wave™: Integrierte Software-Entwicklungsplattformen, Q2 2023"
        link:
          text: Bericht lesen
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
      - header: 'GitLab zählt in allen DevSecOps-Kategorien zu den G2-Leadern'
        link:
          text: Was Branchenanalysten über GitLab sagen
          href: /analysts/
          data_ga_name: g2
          data_ga_location: analyst
        badges:
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 Enterprise Leader – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MomentumLeader_Leader.png
            alt: G2 Momentum Leader – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MostImplementable_Total.svg
            alt: G2 Most Implementable – Sommer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestResults_Total.png
            alt: G2 Best Results – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Enterprise_Total.png
            alt: G2 Best Relationship Enterprise – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Mid-Market_Total.svg
            alt: G2 Best Relationship Mid-Market – Sommer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 Easiest to use – Sommer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestUsability_Total.png
            alt: G2 Best Usability – Sommer 2023
  quotes_carousel_block:
    controls:
      prev: previous case study
      next: next case study
    quotes:
      - main_img:
          url: /nuxt-images/home/JasonManoharan.png
          alt: Bild von Jason Monoharan
        quote: "Die Vision, die GitLab in Bezug auf die Verknüpfung von Strategie mit Umfang und Code hat, ist sehr mächtig. Ich schätze die kontinuierlichen Investitionen in die Plattform."
        author: Jason Monoharan
        logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: ''
        role: VP of Technology
        statistic_samples:
          - data:
              highlight: 150.000 USD
              subtitle: ungefähre Kosteneinsparung pro Jahr
          - data:
              highlight: 20 Stunden
              subtitle: eingesparte Einarbeitungszeit pro Projekt
        url: /customers/iron-mountain/
        header: <a href='/customers/iron-mountain/' data-ga-name='iron mountain' data-ga-location='home case studies'>Iron Mountain</a> treibt die DevOps-Entwicklung mit GitLab Ultimate voran
        data_ga_name: iron mountain
        data_ga_location: home case studies
        cta_text: Mehr lesen
        company: Iron Mountain
      - main_img:
          url: /nuxt-images/home/HohnAlan.jpg
          alt: Bild von Alan Hohn
        quote: "Durch den Wechsel zu GitLab und die Automatisierung der Bereitstellung können unsere Teams statt monatlichen oder wöchentlichen Lieferungen jetzt täglich oder mehrmals täglich Ergebnisse liefern."
        author: Alan Hohn
        logo:
          url: /nuxt-images/logos/lockheed-martin.png
          alt: ''
        role: Director of Software Strategy
        statistic_samples:
          - data:
              highlight: 80 x
              subtitle: schnellere CI-Pipeline-Builds
          - data:
              highlight: 90 %
              subtitle: weniger Zeitaufwand für die Systemwartung
        url: /customers/lockheed-martin/
        header: <a href='/customers/lockheed-martin/' data-ga-name='lockheed martin' data-ga-location='home case studies'>Lockheed Martin</a> spart mit GitLab Zeit, Geld und technische Ressourcen
        data_ga_name: lockheed martin case study
        data_ga_location: home case studies
        cta_text: Mehr lesen
        company: Lockheed Martin
      - main_img:
          url: /nuxt-images/home/EvanO_Connor.png
          alt: Bild von Evan O’Connor
        quote: "GitLabs Engagement für eine Open Source-Community bedeutete, dass wir direkt mit Ingenieuren zusammenarbeiten konnten, um schwierige technische Probleme zu lösen."
        author: Evan O’Connor
        logo:
          url: /nuxt-images/home/havenTech.png
          alt: ''
        role: Platform Engineering Manager
        statistic_samples:
          - data:
              highlight: 62 %
              subtitle: der monatlichen Benutzer(innen) haben Jobs zur Geheimniserkennung ausgeführt
          - data:
              highlight: 66 %
              subtitle: der monatlichen Benutzer(innen) haben sichere Scanner-Jobs ausgeführt
        url: /customers/haven-technologies/
        header: <a href='/customers/haven-technologies/' data-ga-name='haven technology' data-ga-location='home case studies'>Haven Technologies</a> ist mit GitLab zu Kubernetes migriert
        data_ga_name: haven technology
        data_ga_location: home case studies
        cta_text: Mehr lesen
        company: Haven Technologies
      - main_img:
          url: /nuxt-images/home/RickCarey.png
          alt: Bild von Rick Carey
        quote: "Wir haben bei UBS den Ausdruck „alle Entwickler(innen) warten mit der gleichen Geschwindigkeit“. Alles, was ihre Wartezeit verkürzt, ist ein Mehrwert. GitLab ermöglicht es uns, diese integrierte Erfahrung zu haben."
        author: Rick Carey
        logo:
          url: /nuxt-images/home/logo_ubs_mono.svg
          alt: ''
        role: Group Chief Technology Officer
        statistic_samples:
          - data:
              highlight: 1 Million
              subtitle: erfolgreiche Builds in den ersten sechs Monaten
          - data:
              highlight: 12.000
              subtitle: aktive GitLab-Benutzer(innen)
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        header: <a href='https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/' data-ga-name='ubs' data-ga-location='home case studies'>UBS</a> hat mithilfe von GitLab eine eigene DevOps-Plattform erstellt
        data_ga_name: ubs
        data_ga_location: home case studies
        cta_text: Mehr lesen
        company: UBS
      - main_img:
          url: /nuxt-images/home/LakshmiVenkatrama.png
          alt: Bild von Lakshmi Venkatraman
        quote: "Durch GitLab können Teammitglieder auch zwischen verschiedenen Teams optimal zusammenzuarbeiten. Wenn man als Projektmanager ein Projekt oder die Arbeitsbelastung eines Teammitglieds verfolgen kann, lassen sich Verzögerungen bei einem Projekt leichter vermeiden. Wenn ein Projekt abgeschlossen ist, können wir einfach einen Verpackungsprozess automatisieren und die Ergebnisse an den Kunden senden. Mit GitLab befindet sich alles an einem Ort."
        author: Lakshmi Venkatraman
        logo:
          url: /nuxt-images/home/singleron.svg
          alt: ''
        role: Projektmanager
        url: https://www.youtube.com/embed/22nmhrlL-FA
        header: Dank GitLab kann Singleron auf einer einzigen Plattform zusammenzuarbeiten, um die Patientenversorgung zu verbessern
        data_ga_name: singleron video
        data_ga_location: home case studies
        cta_text: Sieh dir das Video an
        modal: true
        comapny: Singleron Biotechnologies
  top-banner:
    text: Das Ergebnis der DevSecOps-Umfrage 2022 mit den Erkenntnissen von 5.000 DevOps-Profis ist eingetroffen.
    link:
      text: Umfrage ansehen
      href: '/developer-survey/'
      ga_name: devsecops survey
      icon: gl-arrow-right