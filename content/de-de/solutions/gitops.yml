---
  title: GitLab für GitOps
  description: Fördere die Zusammenarbeit zwischen deinen Infrastruktur-, Betriebs- und Entwicklungsteams. Setze deine Software mit größerer Sicherheit häufiger ein und erhöhe gleichzeitig die Stabilität, Zuverlässigkeit und Sicherheit deiner Softwareumgebungen. 
  template: 'industry'
  next_step_alt: true
  no_gradient: true
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab für GitOps
        subtitle: Infrastrukturautomatisierung und Zusammenarbeit für Cloud Native-, Multicloud- und Legacy-Umgebungen
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Kostenlose Testversion starten
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        video:
          video_purple_background: true
          hide_in_mobile: true
          video_url: https://www.youtube.com/embed/onFpj_wvbLM?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: 'side-navigation-variant'
      slot_enabled: true
      links:
        - title: Was ist GitOps? 
          href: '#what-is-gitops'
        - title: GitLab für GitOps
          href: '#gitlab-for-gitops'
        - title: Vorteile
          href: '#advantages'
        - title: Leistungen
          href: '#capabilities'
        - title: Enablement
          href: "#enablement"
        - title: Anerkennung
          href: '#recognition'
        - title: Ressourcen
          href: '#resources'
      slot_content:
        - name: 'div'
          id: 'what-is-gitops'
          slot_enabled: true
          slot_content:
            - name: by-solution-intro
              data:
                header: Was ist GitOps? 
                text:
                  description: GitOps ist ein operatives Framework, das die bewährten DevSecOps-Praktiken für die Anwendungsentwicklung wie Versionskontrolle, Zusammenarbeit, Konformität und CI/CD auf die Automatisierung der Infrastruktur anwendet.
                cta:
                  text: Erfahre mehr über GitOps
                  url: /topics/gitops/
                  data_ga_name: Learn more about GitOps
                  data_ga_location: body
        - name: 'div'
          id: 'gitlab-for-gitops'
          slot_enabled: true
          slot_content:
            - name: by-solution-intro
              data:
                header: Gründe für GitLab für GitOps
                text:
                  description: Fördere die Zusammenarbeit zwischen deinen Infrastruktur-, Betriebs- und Entwicklungsteams. Setze deine Software mit größerer Sicherheit häufiger ein und erhöhe gleichzeitig die Stabilität, Zuverlässigkeit und Sicherheit deiner Softwareumgebungen. Nutze die Funktionen von GitLab für Versionskontrolle, Code Review und CI/CD in einer einzigen Anwendung für ein nahtloses Erlebnis. Nutze die enge Integration von GitLab mit HashiCorp Terraform und Vault sowie Multi-Cloud-Fähigkeiten. So erhältst du die beste Plattform für die Automatisierung und Verwaltung deiner Infrastruktur.
                cta:
                  text: Demo ansehen
                  url: /demo/
                  data_ga_name: Watch a demo
                  data_ga_location: body
        - name: 'div'
          id: 'advantages'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Vorteile von GitLab
                is_accordion: true
                is_full_width: true
                items:
                  - icon:
                      name: check-circle-alt
                      alt: "Symbol: Häkchen"
                      variant: marketing
                    header: Eine einzige App für SCM, CI/CD und GitOps
                    text: Quellcodemanagement-, CI/CD- und GitOps-Workflows sind der Kern der Automatisierung und Verwaltung deiner Infrastruktur. Darüber hinaus können unsere KI-gestützten Funktionen dir über den gesamten DevSecOps-Lebenszyklus hinweg dabei helfen, effizienter zu werden und deine Bereitstellungsfrequenz zu erhöhen.
                  - icon:
                      name: terraform
                      alt: "Symbol: Terraform"
                      variant: product
                    header: Eine enge Integration mit Terraform
                    text: Terraform hat sich zum Industriestandard für die Bereitstellung von Umgebungen entwickelt. GitLab kooperiert mit HashiCorp, um sicherzustellen, dass deine Tools optimal zusammenarbeiten.
                  - icon:
                      name: collaboration-alt-4-thin
                      alt: collaboration Icon
                      variant: marketing
                    header: Die größten Ingenieurteams der Welt vertrauen auf uns
                    text: Von Goldman Sachs und Verizon bis hin zu Ticketmaster und Siemens vertrauen mehr große Unternehmen ihren Code GitLab an als jedem anderen Anbieter.
        - name: 'by-solution-value-prop'
          id: capabilities
          data:
            title: Leistungen
            cards:
              - title: Git-basierte Versionskontrolle
                description: Nutze die Git-Werkzeuge, die du bereits als Schnittstelle für den Betrieb verwendest. Versioniere deine Infrastruktur als Code zusammen mit Konfiguration und Richtlinien, um reproduzierbare Umgebungen zu schaffen. Bei Vorfällen kannst du den letzten bekannten funktionierenden Zustand wiederherstellen, um die Zeit für die Wiederherstellung der Services zu senken.
                icon:
                  name: merge
                  alt: merge Icon
                  variant: marketing
              - title: Code-Review
                description: Verbessere die Code-Qualität, verteile Best Practices und fange Fehler ab, bevor sie mit Merge Requests live gehen – mit Merge Requests, die Threads verfolgen und auflösen, Inline-Vorschläge anwenden und asynchron mit Inline- und allgemeinen Thread-Kommentaren arbeiten.
                icon:
                  name: ai-merge-request
                  alt: merge request icon
                  variant: marketing
              - title: Geschützte Branches und Umgebungen
                description: Erlaube allen, mit gemeinsamen Repositorys zum Code beizutragen. Dabei kannst du einschränken, wer in Umgebungen mit eindeutigen Berechtigungen für geschützte und nicht standardmäßige Branches bereitstellen darf.
                icon:
                  name: stop
                  alt: stop Icon
                  variant: marketing
              - title: CI/CD- und GitOps-Workflows
                description: GitLab bietet ein leistungsstarkes und skalierbares CI/CD, das von Grund auf in dieselbe Anwendung wie deine agile Planung und Quellcodeverwaltung integriert ist und somit eine nahtlose Erfahrung ermöglicht. GitLab umfasst statische und dynamische Tests von Infrastruktur als Code, um Sicherheitslücken zu erkennen, bevor sie in die Produktion gehen. GitLab integriert Flux, um Pull-basierte GitOps-Workflows zu unterstützen.
                icon:
                  name: continuous-delivery
                  alt: continuous delivery Icon
                  variant: marketing
              - title: Terraform-Integration
                description: GitLab speichert deine Terraform-Statusdatei und Module zeigt die Terraform-Planausgabe direkt im Merge Request an.
                icon:
                  name: terraform
                  alt: "Symbol: Terraform"
                  variant: product
              - title: Überall bereitstellen
                description: Von Containern und VMs bis hin zu Bare Metal - mit GitLab kannst du überall bereitstellen. Nutze mehrere Clouds mit AWS, Azure, Google Cloud und mehr.
                icon:
                  name: cloud-server
                  alt: cloud Icon
                  variant: marketing
        - name: 'div'
          id: 'enablement'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Wie GitLab GitOps ermöglicht
                is_accordion: true
                is_full_width: true
                items:
                  - icon:
                      name: code
                      alt: code Icon
                      variant: marketing
                    header: Umgebung als Code
                    text: In der GitLab Versionskontrolle als eine einzige Quelle der Wahrheit gespeichert.
                  - icon:
                      name: idea-collaboration
                      alt: idea Icon
                      variant: marketing
                    header: Teams arbeiten zusammen
                    text: Mit der agilen Planung und Code-Review von GitLab.
                  - icon:
                      name: devsecops
                      alt: devsecops Icon
                      variant: marketing
                    header: Das gleiche Tool
                    text: für die Planung, Versionierung und Bereitstellung deines Anwendungscodes funktioniert auch für deinen Betriebscode.
                  - icon:
                      name: microservices-cog
                      alt: cog Icon
                      variant: marketing
                    header: CI/CD- und GitOps-Workflows für die Automatisierung und Verwaltung der Infrastruktur
                    text: Stimmt deine Umgebungen mit deinem SSoT in der Versionskontrolle ab.
        - name: 'report-cta'
          id: 'recognition'
          slot_enabled: true
          data:
            layout: "dark"
            title: Analystenberichte
            reports:
            - description: "GitLab als einziger Marktführer in The Forrester Wave™ anerkannt: Integrierte Software-Entwicklungsplattformen, Q2 2023"
              url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
            - description: "GitLab als Marktführer im Gartner® Magic Quadrant 2023™ für DevOps-Plattformen anerkannt"
              url: /gartner-magic-quadrant/
        
      
        - name: 'solutions-resource-carousel'
          slot_enabled: true
          id: resources
          data:
            column_size: 4
            title: Zugehörige Ressourcen
            carousel: true
            cards:
              - icon:
                  name: video
                  alt: Video Icon
                  variant: marketing
                event_type: Video
                header: Was ist GitOps? Why is it important? Wie fängt man an?
                link_text: Jetzt ansehen
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
                href: https://www.youtube.com/watch?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo&v=JtZfnrwOOAw
              - icon:
                  name: web
                  alt: web Icon
                  variant: marketing
                event_type: Web
                header: Was ist GitOps? 
                link_text: Erfahre mehr über GitOps
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
                href: /topics/gitops/
                data_ga_name: "GitOps: The Future of Infrastructure Automation"
                data_ga_location: resource cards
              - icon:
                  name: web
                  alt: web Icon
                  variant: marketing
                event_type: Web
                header: Warum Kollaborationstechnologie für GitOps wichtig ist
                link_text: Erfahre mehr über GitOps
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
                href: /topics/gitops/gitops-gitlab-collaboration/
                data_ga_name: "Why collaboration technology is critical for GitOps"
                data_ga_location: resource cards
              - icon:
                  name: blog
                  alt: blog Icon
                  variant: marketing
                event_type: Blog
                header: Wie man mit GitLab und Ansible Infrastruktur als Code erstellt
                link_text: Blogbeitrag lesen
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
                href: /blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/
                data_ga_name: How to use GitLab and Ansible to create infrastructure as code
                data_ga_location: resource cards
              - icon:
                  name: article
                  alt: article Icon
                  variant: marketing
                event_type: Artikel
                header: "GitOps: The Future of Infrastructure Automation"
                link_text: Erfahre mehr über GitOps
                image: /nuxt-images/features/resources/resources_webcast.png
                href: /why/gitops-infrastructure-automation/
                data_ga_name: "GitOps: The Future of Infrastructure Automation"
                data_ga_location: resource cards
