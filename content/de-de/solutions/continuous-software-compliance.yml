---
  title: Kontinuierliche Software-Compliance
  description: Integrierte Compliance mit geringerem Risiko und mehr Effizienz
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Kontinuierliche Software-Compliance
        subtitle: Integrierte Compliance mit geringerem Risiko und mehr Effizienz
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Kostenlose Testversion starten
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Fragen? Kontaktiere uns
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: GitLab für kontinuierliche Software-Compliance"
    - name: 'by-solution-intro'
      data:
        text:
          highlight: Bei Software-Compliance geht es nicht länger nur darum, Kästchen anzukreuzen.
          description: Cloud-native Anwendungen bieten völlig neue Angriffsflächen über Container, Orchestratoren, Web-APIs und andere Infrastruktur-as-a-Code. Diese neuen Angriffsflächen haben zusammen mit komplexen DevOps-Toolchains zu berüchtigten Software-Supply-Chain-Angriffen und neuen gesetzlichen Anforderungen geführt. Die kontinuierliche Software-Compliance ist eine wichtige Methode zur Verwaltung der Risiken, die Cloud-nativen Anwendungen und der DevOps-Automatisierung innewohnen – über die bloße Reduzierung von Sicherheitslücken im eigentlichen Code hinaus.
    - name: 'by-solution-benefits'
      data:
        title: Compliance. Sicherheit. Ganz einfach.
        image:
          image_url: "/nuxt-images/resources/resources_11.jpeg"
          alt: GitLab für den öffentlichen Sektor
        is_accordion: true
        items:
          - header: Vertrauen bei jedem Commit
            icon:
              name: release
              alt: Schildprüfsymbol
              variant: marketing
            text: Lizenzkonformität und Sicherheitsscans erfolgen automatisch bei jedem Codeänderungs-Commit.
          - header: Compliance-Pipelines zur Sicherstellung der Durchsetzung
            icon:
              name: pipeline-alt
              alt: Pipeline-Symbol
              variant: marketing
            text: Stelle sicher, dass wichtige Richtlinien durchgesetzt werden — unabhängig davon, ob es sich um standardmäßige behördliche Kontrollen oder um deinen eigenen Richtlinienrahmen handelt.
          - header: Vereinfachung von Audits
            icon:
              name: magnifying-glass-code
              alt: Lupe-Glas-Code-Symbol
              variant: marketing
            text: Automatisierte Richtlinien vereinfachen Audits mit Transparenz und Rückverfolgbarkeit.
          - header: Alles zentral
            icon:
              name: devsecops
              alt: DevSecOps-Symbol
              variant: marketing
            text: Mit GitLab musst du nur noch eine Anwendung verwalten. Keine Silos, und bei der Übersetzung geht auch nichts verloren.
    - name: 'by-solution-value-prop'
      data:
        title: Eine DevOps-Plattform für Compliance
        cards:
          - title: Integrierte Kontrolle
            description: Software-Compliance kann ein schwieriges Thema sein, wenn sie vom Softwareentwicklungsprozess getrennt ist. Unternehmen benötigen ein Compliance-Programm, das in ihre vorhandenen Workflows und Prozesse integriert ist und nicht darüber gestülpt wird. Erfahre mehr, indem du den <a href="https://page.gitlab.com/resources-ebook-modernsoftwarefactory.html" data-ga-name="Guide to Software Supply Chain Security" data-ga-location="body">Leitfaden zum Schutz der Software-Lieferkette</a> herunterlädst.
            icon:
              name: visibility
              alt: Sichtbarkeitssymbol
              variant: marketing
          - title: Richtlinienautomatisierung
            description: Compliance-Leitlinien ermöglichen eine schnelle Softwareentwicklung und verringern gleichzeitig das Risiko von Verstößen und Projektverzögerungen. Das Auditing wird dadurch vereinfacht, dass alles an einem Ort ist.
            icon:
              name: continuous-integration
              alt: Symbol für kontinuierliche Integration
              variant: marketing
          - title: Schiebe die Compliance zur Seite
            description: So wie du Sicherheitsmängel frühzeitig finden und beheben möchtest, ist es am effizientesten, Compliance-Mängeln ebenso zu verfahren. Wenn du sicherstellst, dass die Compliance in die Entwicklung integriert ist, kannst du auch die Compliance zur Seite schieben.
            icon:
              name: less-risk
              alt: Symbol für weniger Risiko
              variant: marketing
          - title: Compliance-Rahmen
            description: Wende gängige Compliance-Einstellungen mühelos mit einem Label auf ein Projekt an.
            list:
              - text: <a href="https://about.gitlab.com/solutions/pci-compliance/" data-ga-name="PCI Compliance" data-ga-location="body" >PCI-Compliance</a>
              - text: <a href="https://about.gitlab.com/solutions/hipaa-compliance/" data-ga-name="HIPAA Compliance" data-ga-location="body" >HIPAA-Compliance</a>
              - text: <a href="https://about.gitlab.com/solutions/financial-services-regulatory-compliance/" data-ga-name="Financial Services Regulatory Compliance" data-ga-location="body" >Einhaltung gesetzlicher Vorschriften für Finanzdienstleistungen</a>
              - text: <a href="/privacy/privacy-compliance/" data-ga-name="GDPR" data-ga-location="body" >DSGVO</a>
              - text: <a href="https://about.gitlab.com/solutions/iec-62304/" data-ga-name="IEC 62304:2006" data-ga-location="body" >IEC 62304:2006</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-13485/" data-ga-name="ISO 13485:2016" data-ga-location="body" >ISO 13485:2016</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-26262/" data-ga-name="ISO 26262-6:2018" data-ga-location="body" >ISO 26262-6:2018</a>
              - text: Dein eigener Richtlinienrahmen
            icon:
              name: web-alt
              alt: Web-Symbol
              variant: marketing
    - name: 'by-industry-solutions-block'
      data:
        subtitle: Vereinfache die kontinuierliche Software-Compliance
        sub_description: "Die Compliance-Management-Funktionen von GitLab sollen eine Erfahrung erschaffen, die einfach, freundlich und so reibungslos wie möglich verläuft, indem du Compliance-Richtlinien und -Rahmenbedingungen definierst, durchsetzt und Berichte dazu erstellst."
        white_bg: true
        sub_image: /nuxt-images/solutions/showcase.png
        solutions:
          - title: Richtlinienverwaltung
            description: Definiere Regeln und Richtlinien, um Compliance-Rahmenbedingungen sowie gemeinsame Kontrollen einzuhalten.
            icon:
              name: clipboard-checks
              alt: Zwischenablage-Überprüfungs-Symbol
              variant: marketing
            link_text: Erfahre mehr
            link_url: https://docs.gitlab.com/ee/administration/compliance.html
            data_ga_name: common controls
            data_ga_location: solutions block
          - title: Konforme Workflow-Automatisierung
            icon:
              name: automated-code
              alt: Symbol für automatisierten Code
              variant: marketing
            description:  Die Compliance-Automatisierung hilft dir dabei, die definierten Regeln und Richtlinien sowie die Aufgabentrennung durchzusetzen und gleichzeitig das allgemeine Geschäftsrisiko zu reduzieren.
            link_text: Erfahre mehr
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
            data_ga_name: Compliant Workflow Automation
            data_ga_location: solutions block
          - title: Auditmanagement
            icon:
              name: magnifying-glass-code
              alt: Lupe-Glas-Code-Symbol
              variant: marketing
            description: Protokolliere Aktivitäten in deiner gesamten DevOps-Automatisierung, um Vorfälle zu identifizieren und die Einhaltung von Compliance-Regeln sowie definierten Richtlinien nachzuweisen. Die Sichtbarkeit ist mit einer Plattform und ohne Toolchain-Silos größer.
            link_text: Erfahre mehr
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#audit-management
            data_ga_name: Audit management
            data_ga_location: solutions block
          - title: Sicherheitstests und Schwachstellenmanagement
            description: Stelle sicher, dass die Sicherheits-Scans und die Lizenzkonformität bei jeder Codeänderung eingehalten werden, und ermögliche es DevOps-Ingenieuren und Sicherheitsprofis gleichermaßen, Schwachstellen zu verfolgen und zu verwalten.
            icon:
              name: approve-dismiss
              alt: Symbol "Ablehnen / genehmigen"
              variant: marketing
            link_text: Erfahre mehr
            link_url: https://about.gitlab.com/solutions/security-compliance/
            data_ga_name: Security testing and vulnerability management
            data_ga_location: solutions block
          - title: Schutz der Software-Lieferkette
            description: Verwalte die End-to-End-Angriffsoberflächen von Cloud-nativen Anwendungen und die DevOps-Automatisierung — über herkömmliche Anwendungssicherheitstests hinaus.
            link_text: Erfahre mehr
            icon:
              name: release
              alt: Schild-Prüf-Symbol
              variant: marketing
            link_url: https://about.gitlab.com/direction/supply-chain/
            data_ga_name: offline environment
            data_ga_location: solutions block
    - name: 'solutions-resource-cards'
      data:
        title: Ressourcen
        column_size: 4
        link:
          text: "Mehr erfahren"
        cards:
          - icon:
              name: video
              alt: Video-Symbol
              variant: marketing
            event_type: "Video"
            header: "Konforme Pipelines"
            link_text: "Jetzt ansehen"
            image: "/nuxt-images/solutions/compliance/compliant-pipelines.jpeg"
            href: "https://www.youtube.com/embed/jKA_e_jimoI"
            data_ga_name: "Compliant pipelines"
            data_ga_location: Ressourcenkarten
          - icon:
              name: blog
              alt: Blog-Symbol
              variant: marketing
            event_type: "Blog"
            header: "Drei Dinge, die du vielleicht noch nicht über GitLab-Sicherheitsfunktionen weißt"
            link_text: "Mehr erfahren"
            href: "https://about.gitlab.com/blog/2021/11/23/three-things-you-might-not-know-about-gitlab-security/"
            image: "/nuxt-images/resources/resources_1.jpeg"
            data_ga_name: "Three things you may not know about GitLab security"
            data_ga_location: Ressourcenkarten
          - icon:
              name: webcast
              alt: Webcast-Symbol
              variant: marketing
            event_type: "Webinar"
            header: "Schütze deine Software-Lieferkette"
            link_text: "Mehr erfahren"
            href: "https://www.youtube.com/watch?v=dZfS4Wt5ZRE"
            fallback_image: "/nuxt-images/resources/fallback/img-fallback-cards-gitops.png"
            data_ga_name: "Secure your software supply chain"
            data_ga_location: Ressourcenkarten
          - icon:
              name: ebook
              alt: E-Book-Symbol
              variant: marketing
            event_type: "E-Book"
            header: "Schutz der Software-Lieferkette"
            link_text: "Mehr erfahren"
            href: "https://learn.gitlab.com/devsecops-aware/software-supply-chain-security-ebook"
            fallback_image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            data_ga_name: "Software Supply Chain Security"
            data_ga_location: Ressourcenkarten
          - icon:
              name: case-study
              alt: Fallstudien-Symbol
              variant: marketing
            event_type: "Fallstudie"
            header: "Fallstudie Chorus"
            link_text: "Mehr erfahren"
            image: "/nuxt-images/resources/resources_2.jpeg"
            href: "/customers/chorus/"
            data_ga_name: "Chorus case study"
            data_ga_location: Ressourcenkarten
