title: GitLabs KI-gestützte Code-Vorschläge
og_title: GitLabs KI-gestützte Code-Vorschläge
description: GitLabs KI-gestützte Code-Vorschläge Sichert deinen geschützten Code. Hilft dir, Code produktiver zu schreiben. Macht Milliarden von Code-Zeilen für dich zugänglich.
twitter_description: GitLabs KI-gestützte Code-Vorschläge Sichert deinen geschützten Code. Hilft dir, Code produktiver zu schreiben. Macht Milliarden von Code-Zeilen für dich zugänglich.
og_description: GitLabs KI-gestützte Code-Vorschläge Sichert deinen geschützten Code. Hilft dir, Code produktiver zu schreiben. Macht Milliarden von Code-Zeilen für dich zugänglich.
og_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
twitter_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
hero: 
  note: GitLabs KI-gestützte Code-Vorschläge
  header: (beta)
  description: 
    - typed:
        - Schützt deinen proprietären Code
      is_description_inline: true
      highlighted: secure
    - typed:
        - Hilft dir dabei, Code
      highlighted: produktiver zu schreiben
    - typed:
        - Macht Milliarden von Code-Zeilen
      highlighted: für dich zugänglich
  image: 
    src: /nuxt-images/solutions/ai/GitLab Logo.png
    alt: KI-Logo
  button:
    text: Teste Code-Vorschläge kostenlos
    href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
    variant: secondary
copyBlocks:
  blocks: 
    - title: Beschleunige dein Coding
      description: Mit Hilfe von generativer KI, die während des Programmierens Code vorschlägt, helfen wir Teams dabei, Software schneller und effizienter zu erstellen. Vervollständige eine ganze Code-Zeile mit einem einzigen Tastendruck, starte schnell eine Funktion, fülle Boilerplate-Code aus, generiere Tests und vieles mehr.
    - title: Halte deinen Quellcode geschützt
      description: Datenschutz ist in unsere Code-Vorschläge integriert. Dein proprietärer Code bleibt in der Enterprise-Cloud-Infrastruktur von GitLab gschützt. Dein Code wird nicht als Datensatz für Schulungen verwendet. Die Quellcode-Rückschlüsse aus dem Code-Vorschlagsmodell werden nicht gespeichert. [Erfahre mehr über die Datennutzung](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#code-suggestions-data-usage){data-ga-name="data usage code suggestions" data-ga-location="body"}, wenn du Code-Vorschläge benutzt.
      img:
        src: /nuxt-images/solutions/code-suggestions/secure-code.svg
        alt: secure code logo
    - title: Milliarden von Code-Zeilen auf Knopfdruck
      description: Code-Vorschläge verwenden vortrainierte Open-Source-Modelle, die kontinuierlich mit einem angepassten Open-Source-Datensatz verfeinert werden, um die Unterstützung mehrerer Sprachen und zusätzlicher Anwendungsfälle zu ermöglichen.
      languageSelected: 'go'
    - title: Unterstützung in der Sprache deiner Wahl
      description: 'KI-gestützte Code-Vorschläge, die sich an deine Arbeitsweise anpassen, sind in 13 Sprachen verfügbar: C/C++, C#, Go, Java, JavaScript, Python, PHP, Ruby, Rust, Scala, Kotlin und TypeScript '
      logos: [
        "/nuxt-images/logos/c-logo.svg",
        "/nuxt-images/logos/cpp-logo.svg",
        "/nuxt-images/logos/c-sharp-logo.svg",
        "/nuxt-images/logos/javascript-logo.svg",
        "/nuxt-images/logos/typescript-logo.svg",
        "/nuxt-images/logos/python-logo.svg",
        "/nuxt-images/logos/java-logo.svg",
        "/nuxt-images/logos/ruby-logo.svg",
        "/nuxt-images/logos/rust-logo.svg",
        "/nuxt-images/logos/scala-logo.svg",
        "/nuxt-images/logos/kotlin-logo.svg",
        "/nuxt-images/logos/go-logo.svg",
        "/nuxt-images/logos/php-logo.svg",
      ]
resources:
    data:
      title: Was ist neu in GitLab KI
      column_size: 4
      cards:
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: AI/ML in DevSecOps Series
          link_text: Mehr lesen
          image: /nuxt-images/blogimages/ai-ml-in-devsecops-blog-series.png
          href: /blog/2023/04/24/ai-ml-in-devsecops-series/
          data_ga_name: AI/ML in DevSecOps Series
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: >-
            GitLab details AI-assisted features in the DevSecOps platform
          link_text: Mehr lesen
          href: https://about.gitlab.com/blog/2023/05/03/gitlab-ai-assisted-features/
          image: /nuxt-images/blogimages/ai-fireside-chat.png
          data_ga_name: GitLab details AI-assisted features in the DevSecOps platform
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: How AI-assisted code suggestions will advance DevSecOps
          link_text: Mehr lesen
          href: https://about.gitlab.com/blog/2023/03/23/ai-assisted-code-suggestions/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: How AI-assisted code suggestions will advance DevSecOps
          data_ga_location: body
ctaBlock:
    title: Was kommt als Nächstes auf deine(n) KI-Paar-Programmierer(in) zu?
    cards:
      - header: Code-Vorschläge für selbstverwaltete Instanzen
        description: |
          Code-Vorschläge werden bald für selbstverwaltete Instanzen über eine sichere Verbindung zu GitLab.com verfügbar sein. Wenn du besondere Anforderungen an deine selbstverwalteten Instanzen hast, kannst du dein Interesse in unserem [selbstverwalteten Support-Ticket](https://gitlab.com/gitlab-org/gitlab/-/issues/409183) bekunden.
        icon: ai-code-suggestions
      - header: Verbessertes Benutzererlebnis
        description: | 
          Wir arbeiten an zusätzlicher IDE-Unterstützung, einschließlich JetBrains IntelliJ-basierter IDEs und Visual Studio-Unterstützung für Code-Vorschläge. Außerdem planen wir, das Benutzererlebnis bei der Darstellung und Annahme von Vorschlägen in den IDEs zu verbessern und den Entwickler(inne)n mehr Kontrolle über die Arbeitsweise der Funktion zu geben.
        icon: user-laptop