---
  title: GitLab-Cookie-Richtlinie
  description: Auf dieser Seite finden Sie Informationen zur GitLab-Cookie-Richtlinie. Erfahren Sie mehr!
  side_menu:
    anchors:
      text: "Auf dieser Seite"
      data:
        - text: Warum verwenden wir Cookies?
          href: "#why-do-we-use-cookies"
          data_ga_name: why do we use cookies
          data_ga_location: side-navigation
        - text: Wer setzt Cookies auf GitLab?
          href: "#who-sets-cookies-on-gitlab"
          data_ga_name: who sets cookies on gitlab
          data_ga_location: side-navigation
        - text: Wie verwalte ich meine Cookies?
          href: "#how-do-i-manage-my-cookies"
          data_ga_name: how do i manage my cookies
          data_ga_location: side-navigation
        - text: Do Not Track-Signale
          href: "#do-not-track-signals"
          data_ga_name: do not track signals
          data_ga_location: side-navigation
        - text: Global Privacy Control (Globale Datenschutzkontrolle)
          href: "#global-privacy-control"
          data_ga_name: global privacy control
          data_ga_location: side-navigation
        - text: Selbstregulierungsprogramme
          href: "#self-regulatory-programs"
          data_ga_name: self regulatory programs
          data_ga_location: side-navigation
        - text: Web-Beacons
          href: "#web-beacons"
          data_ga_name: web beacons
          data_ga_location: side-navigation
        - text: Werbe-IDs auf mobilen Geräten
          href: "#mobile-advertising-ids"
          data_ga_name: mobile advertising ids
          data_ga_location: side-navigation
        - text: Welche Arten von Cookies verwenden wir?
          href: "#what-types-of-cookies-do-we-use"
          data_ga_name: what types of cookies do we use
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
  hero_title: GitLab-Cookie-Richtlinie
  disclaimer:
    text: |
      Diese Übersetzung dient nur zu Informationszwecken.
    details: |
      Im Falle einer Diskrepanz zwischen dem englischen Text und dieser Übersetzung ist die englische Version maßgebend.
  copy: |
    Diese Cookie-Richtlinie ergänzt die in unserer Datenschutzerklärung enthaltenen Informationen und erklärt, wie wir Cookies und verwandte Technologien verwenden, um unsere GitLab-Websites, -Produkte und -Dienste zu verwalten und bereitzustellen; zusammenfassend als „Dienste“ bezeichnet.

    Cookies sind kleine Textdateien, die auf deinem Computer gespeichert werden. Pixel sind kleine Mengen an Code auf einer Webseite oder in einer E-Mail, die eine Methode zur Bereitstellung von Inhalten bieten, z. B. ein grafisches Bild auf einer Webseite.
  sections:
    - header: Warum verwenden wir Cookies?
      id: why-do-we-use-cookies
      text: |
        GitLab verwendet Cookies für folgende Zwecke:

        - um dir zu helfen, auf die Dienste zuzugreifen und sie zu nutzen,
        - um zu verstehen, wie Besucher(innen) unsere Dienste nutzen und sich damit beschäftigen,
        - um deine Einstellungen festzulegen,
        - um relevante interessenbezogene Werbung bereitzustellen,
        - um zu verstehen, ob du eine E-Mail geöffnet und darauf reagiert hast, und
        - um unsere Dienste zu analysieren und zu verbessern.
    - header: Wer setzt Cookies auf GitLab?
      id: who-sets-cookies-on-gitlab
      text: |
        Cookies werden manchmal von GitLab platziert, bekannt als Erstanbieter-Cookies, und manchmal werden Cookies von Drittanbietern von unseren Dienstleistern wie Google Analytics gesetzt, die Analyse und interessenbasierte Werbung bereitstellen. Wenn Drittanbieter-Cookies von Dienstleistern gesetzt werden, stellen sie GitLab einen Dienst oder eine Funktion zur Verfügung und erfüllen die eigenen Zwecke des Dienstleisters. GitLab kann nicht steuern, wie Cookies von Drittanbietern verwendet werden. Du kannst die verfügbaren Opt-Outs von Google Analytics, einschließlich der Browser-Ad-On von Google Analytics, unter folgendem Link einsehen: <https://tools.google.com/dlpage/gaoptout/>.
    - header: Wie verwalte ich meine Cookies?
      id: how-do-i-manage-my-cookies
      text: |
       GitLab hat unser Cookie-Management-Tool in allen GitLab-Domains vereinheitlicht und bietet Benutzer(innen) einen zentralen Ort, um ihre Cookie-Einstellungen zu verwalten. Mit Ausnahme von „unbedingt erforderlichen“ Cookies, die für den Betrieb der Dienste unerlässlich sind, kannst du deine Cookie-Einstellungen für alle anderen Cookie-Kategorien aktivieren, deaktivieren oder anpassen. Bitte beachte, dass das Deaktivieren einiger Cookies dazu führen kann, dass bestimmte Funktionen der Dienste nicht ordnungsgemäß funktionieren. Auf dieses Tool kann zugegriffen werden, indem du auf den **Link „Cookie-Einstellungen“** (mit dem Titel „**Meine personenbezogenen Daten nicht verkaufen oder weitergeben**“ für Einwohner Kaliforniens) klickst, der sich in der Fußzeile jeder Webseite unter „Unternehmen“ befindet.

        Die meisten Webbrowser ermöglichen es dir auch, bereits gesetzte Cookies zu löschen, wodurch die von diesen Cookies kontrollierten Einstellungen und Präferenzen, einschließlich Werbepräferenzen, gelöscht werden können. Eine Anleitung zum Entfernen von erstellten Cookies findest du im Cookie-Ordner deines Browsers unter <https://allaboutcookies.org/manage-cookies/>.
    - header: Do Not Track-Signale
      id: do-not-track-signals
      text: |
        „Do Not Track“ (DNT) ist eine Datenschutzeinstellung, die du in deinem Webbrowser festlegen kannst, um anzuzeigen, dass du nicht verfolgt werden möchtest. GitLab reagiert auf DNT-Signale und respektiert den Track-Präferenzausdruck von Benutzer(innen) für clientseitige Ereignisse.
    - header: Global Privacy Control (Globale Datenschutzkontrolle)
      id: global-privacy-control
      text: |
        Ähnlich wie DNT-Signale ist „Global Privacy Control“ (GPC) eine Datenschutzeinstellung, die du in deinem Webbrowser festlegen kannst, um Websites darüber zu informieren, dass du nicht möchtest, dass deine personenbezogenen Daten ohne deine Einwilligung an unabhängige Dritte weitergegeben oder verkauft werden. GitLab erkennt GPC in den Ländern an, in denen seine Anerkennung nach geltendem Recht erforderlich ist.
    - header: Selbstregulierungsprogramme
      id: self-regulatory-programs
      text: |
        Dienstleister können an Selbstregulierungsprogrammen teilnehmen, die Möglichkeiten bieten, sich von Analysen und interessenbasierter Werbung abzumelden, auf die du unter folgender Adresse zugreifen kannst:

        - Vereinigte Staaten: NAI (<http://optout.networkadvertising.org>) und DAA (<http://optout.aboutads.info/>)
        - Kanada: Digital Advertising Alliance of Canada (<https://youradchoices.ca/>)
        - Europa: European Digital Advertising Alliance (<http://www.youronlinechoices.com>)
    - header: Web-Beacons
      id: web-beacons
      text: |
        Die meisten E-Mail-Clients haben Einstellungen, mit denen du das automatische Herunterladen von Bildern verhindern kannst, wodurch Web-Beacons in den E-Mail-Nachrichten, die du liest, deaktiviert werden.
    - header: Werbe-IDs auf mobilen Geräten
      id: mobile-advertising-ids
      text: |
        Auf mobilen Geräten können von der Plattform bereitgestellte Werbe-IDs ähnlich wie Cookie-IDs erhoben und verwendet werden. Du kannst die Steuerelemente auf iOS- und Android-Betriebssystemen verwenden, mit denen du das Tracking einschränkst und/oder die Werbe-IDs zurücksetzen kannst.
    - header: Welche Arten von Cookies verwenden wir?
      id: what-types-of-cookies-do-we-use
      text: |
        GitLab verwendet vier Kategorien von Cookies: unbedingt erforderliche Cookies, funktionale Cookies, Leistungs- und Analyse-Cookies sowie Targeting- und Werbe-Cookies. Du kannst mehr über diese Arten von Cookies lesen und die Cookies in jeder Kategorie anzeigen, die in den Diensten gefunden werden können, indem du auf den **Link „Cookie-Einstellungen“** (mit dem Titel „**Meine personenbezogenen Daten nicht verkaufen oder weitergeben**“ für Einwohner Kaliforniens) klickst, der sich in der Fußzeile jeder Webseite unter „Unternehmen“ befindet.
    