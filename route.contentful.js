// File used to generate routes for content files living in Contentful
// This is done to generate those routes during yarn generate
import { createClient } from 'contentful';
import { CONTENT_TYPES } from './common/content-types';

const client = createClient({
  space: process.env.CTF_SPACE_ID,
  accessToken: process.env.CTF_CDA_ACCESS_TOKEN,
  host: 'cdn.contentful.com',
});

export async function fetchCustomerRoutes() {
  try {
    const caseStudies = await client.getEntries({
      content_type: CONTENT_TYPES.CASE_STUDY,
      select: 'fields.slug',
      limit: 100,
    });

    return caseStudies.items.map(
      (caseStudy) => `/customers/${caseStudy.fields.slug}`,
    );
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching customer routes from Contentful:', error);
    return [];
  }
}

export async function fetchTopicsRoutes() {
  try {
    const topics = await client.getEntries({
      content_type: CONTENT_TYPES.TOPICS,
      select: 'fields.slug',
      limit: 100,
    });

    return topics.items.map((topic) => `${topic.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching topics routes from Contentful:', error);
    return [];
  }
}

export async function fetchSolutionsRoutes() {
  try {
    const caseStudies = await client.getEntries({
      content_type: 'page',
      select: 'fields.slug',
      limit: 100,
    });

    const regex = /^solutions\/.+/;

    return caseStudies.items
      .filter((caseStudy) => regex.test(caseStudy.fields.slug))
      .map((caseStudy) => `/${caseStudy.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching solution routes from Contentful:', error);
    return [];
  }
}

// eslint-disable-next-line import/no-default-export
export default [
  '/install/ce-or-ee/',
  '/free-trial/',
  '/dedicated/',
  '/customers/',
  '/topics/',
  '/solutions/code-suggestions/',
  '/events/summit-las-vegas/',
  '/events/devsecops-world-tour/',
  '/events/devsecops-world-tour/atlanta/',
  '/events/devsecops-world-tour/berlin/',
  '/events/devsecops-world-tour/chicago/',
  '/events/devsecops-world-tour/dallas/',
  '/events/devsecops-world-tour/irvine/',
  '/events/devsecops-world-tour/london/',
  '/events/devsecops-world-tour/melbourne/',
  '/events/devsecops-world-tour/mountain-view/',
  '/events/devsecops-world-tour/new-york/',
  '/events/devsecops-world-tour/paris/',
  '/events/devsecops-world-tour/washington-dc/',
  '/events/devsecops-world-tour/executive/berlin/',
  '/events/devsecops-world-tour/executive/dallas/',
  '/events/devsecops-world-tour/executive/irvine/',
  '/events/devsecops-world-tour/executive/london/',
  '/events/devsecops-world-tour/executive/mountain-view/',
  '/events/devsecops-world-tour/executive/new-york/',
  '/events/devsecops-world-tour/executive/paris/',
  '/events/devsecops-world-tour/executive/washington-dc/',
];
