export interface MetadataDTO {
  title?: string;
  description?: string;
  image_title?: string;
  twitter_image?: string;
  image_alt?: string;
  schema_faq?: [];
  schema_org?: string;
  topic_name?: string;
  time_required?: string;
  topics_header?: any;
  date_published?: string;
  date_modified?: string;
  no_index?: boolean;
}

export interface PageDTO extends MetadataDTO {
  components?: any[];
  // Place any other common property for pages
}
