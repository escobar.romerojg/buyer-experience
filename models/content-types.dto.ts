/**
 * Generic ContentfulEntry type
 */
export interface CtfEntry<T> {
  fields: T;
  sys: any;
}

/**
 * Contentful generic Image ContentType
 */
export interface CtfImage {
  title: string;
  description: string;
  file: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/headerAndText/fields
 */
export interface CtfHeaderAndText {
  header: string;
  text: string;
  headerAnchorId: string;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/asset/fields
 */
export interface CtfAsset {
  image: any;
  altText: string;
  link: string;
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/button/fields
 */
export interface CtfButton {
  internalName: string;
  text: string;
  externalUrl: string;
  dataGaName: string;
  dataGaLocation: string;
  iconName: string;
  iconVariant: string;
  openInNewTab: boolean;
  variation: string;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/externalVideo/fields
 */
export interface CtfExternalVideo {
  title: string;
  url: string;
  thumbnail: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/eventHero/fields
 */
export interface CtfHero {
  internalName: string;
  title: string;
  subheader: string;
  componentName: string;
  variant: string;
  description: string;
  backgroundImage: CtfEntry<CtfImage>;
  primaryCta: CtfEntry<CtfButton>;
  secondaryCta: CtfEntry<CtfButton>;
  video: CtfEntry<CtfExternalVideo>;
  crumbs: CtfEntry<CtfButton>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/twoColumnBlock/fields
 */
export interface CtfTwoColumnBlock {
  internalName: string;
  header: string;
  subheader: string;
  text: string;
  readMoreText: string;
  readMoreControlsText: string;
  assets: CtfEntry<CtfExternalVideo>[] | CtfEntry<CtfAsset>[];
  cta: CtfEntry<CtfButton>;
  secondaryCta: CtfEntry<CtfButton>;
  blockGroup: CtfEntry<CtfButton>[];
  customFields: any;
}

export interface CtfCard {
  internalName: string;
  title: string;
  componentName: string;
  subtitle: string;
  description: string;
  list: string[];
  pills: string[];
  variant: string;
  iconName: string;
  image: CtfEntry<CtfImage>;
  video: CtfEntry<CtfExternalVideo>;
  button: CtfEntry<CtfButton>;
  secondaryButton: CtfEntry<CtfButton>;
  column2Title: string;
  column2Description: string;
  column2List: string[];
  column2Cta: CtfEntry<CtfButton>;
  cardLink: string;
  cardLinkDataGaName: string;
  cardLinkDataGaLocation: string;
  contentArea: CtfEntry<any>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/cardGroup/fields
 */
export interface CtfCardGroup {
  internalName: string;
  header: string;
  componentName: string;
  subheader: string;
  description: string;
  image: CtfEntry<CtfAsset>;
  video: CtfEntry<CtfExternalVideo>;
  cta: CtfEntry<CtfButton>;
  card: CtfEntry<CtfCard>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/blockGroup/fields
 */
export interface CtfBlockGroup {
  internalName: string;
  header: string;
  componentName: string;
  subheader: string;
  description: string;
  blocks: CtfEntry<CtfTwoColumnBlock>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/anchorLink/fields
 */
export interface CtfAnchorLink {
  internalName: string;
  linkText: string;
  anchorLink: string;
  nodes: any;
  dataGaName: string;
  dataGaLocation: string;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/sideMenu/fields
 */
export interface CtfSideNavigation {
  internalName: string;
  header: string;
  footer: string;
  anchorsText: string;
  anchors: CtfEntry<CtfAnchorLink>[];
  hyperlinksText: string;
  hyperlinks: CtfEntry<any>[];
  content: CtfEntry<any>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/faq/fields
 */
export interface CtfFaq {
  id: string;
  title: string;
  subtitle: string;
  dataInboundAnalytics: string;
  background: boolean;
  showAllButton: boolean;
  expandAllCarouselItemsText: string;
  hideAllCarouselItemsText: string;
  accordionType: string;
  singleAccordionGroupItems:
    | CtfEntry<CtfCardGroup>[]
    | CtfEntry<CtfHeaderAndText>[];
  multipleAccordionGroups: CtfEntry<CtfFaq>[];
}
