/* eslint-disable class-methods-use-this */
import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';

export class FreeTrialService {
  private readonly $ctx: Context;

  private readonly componentNames = {
    CTA: 'call-to-action',
    FREE_TRIAL_PLANS: 'free-trial-plans',
    FAQ: 'faq',
    HERO: 'hero',
    QUOTES_CAROUSEL_BLOCK: 'quotes_carousel_block',
    G2_CATEGORIES: 'g2_categories',
    INFORMATION_SQUARES: 'information_squares',
    NEXT_STEPS: 'next_steps',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns components to the /free-trial page
   * @param slug
   * @param isIndexSlug
   */
  async getContent(slug: string, isIndexSlug: string) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      try {
        return this.getContentfulData(slug);
      } catch (e) {
        throw new Error(e);
      }
    }

    try {
      const content = this.$ctx
        .$content(
          this.$ctx.i18n.locale,
          `${slug}${isIndexSlug ? '/index' : ''}`,
        )
        .fetch();

      // Localized data - YML files
      return content;
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string) {
    try {
      const content = await getClient().getEntries({
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
      });

      if (content.items.length === 0) {
        throw new Error('Not found');
      }

      const [freeTrial] = content.items;

      return this.transformContentfulData(freeTrial);
    } catch (e) {
      throw new Error(e);
    }
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getPageComponents(pageContent);

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      pageContent,
      components: mappedContent,
    };

    return transformedPage;
  }

  private getPageComponents(pageContent: any[]) {
    const components: any[] = [];

    pageContent.forEach((ctfComponent) => {
      components.push(this.mapCtfComponent(ctfComponent));
    });

    return components;
  }

  private mapCtfComponent(ctfComponent: any) {
    let component;

    const { id } = ctfComponent.sys.contentType.sys;

    switch (id) {
      // FREE TRIAL LANDING PAGE COMPONENTS

      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapCta(ctfComponent);
        break;
      }

      case CONTENT_TYPES.FAQ: {
        component = this.mapFaq(ctfComponent);
        break;
      }

      case CONTENT_TYPES.TAB_CONTROLS_CONTAINER: {
        component = this.mapFreeTrialPlans(ctfComponent);
        break;
      }

      // FREE TRIAL DEVSECOPS PAGE COMPONENTS

      case CONTENT_TYPES.HERO: {
        component = this.mapHero(ctfComponent);
        break;
      }

      case CONTENT_TYPES.TWO_COLUMN_BLOCK: {
        component = this.mapTwoColumnBlock(ctfComponent);
        break;
      }

      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroup(ctfComponent);
        break;
      }

      case CONTENT_TYPES.NEXT_STEPS: {
        component = this.mapNextSteps(ctfComponent);
        break;
      }

      default:
        break;
    }

    return component;
  }

  private mapFaq(ctfFaq: any) {
    const { title, multipleAccordionGroups } = ctfFaq.fields;

    const groups = multipleAccordionGroups.map((group: any) => {
      const questions = group.fields.singleAccordionGroupItems.map(
        (element: any) => ({
          question: element.fields.header,
          answer: element.fields.text,
        }),
      );

      return {
        header: group.fields.title,
        questions,
      };
    });

    return {
      name: this.componentNames.FAQ,
      data: {
        header: title,
        groups,
      },
    };
  }

  private mapCta(ctfCta: any) {
    const { fields } = ctfCta;

    return {
      name: this.componentNames.CTA,
      data: {
        title: fields.header,
        subtitle: fields.text,
        centered_by_default: true,
        aos_animation: 'fade-down',
        aos_duration: 500,
      },
    };
  }

  private mapFreeTrialPlans(ctfFreeTrialPlans: any) {
    const { fields } = ctfFreeTrialPlans;

    let gitlabSaas: Object = {};
    let selfManaged: Object = {};

    fields.tabs.forEach((tab) => {
      const id = tab.fields.tabId.trim();

      if (id === 'gitlab-self-managed') {
        selfManaged = {
          form_id: tab.fields.tabPanelContent[0].fields.formId,
          form_header: tab.fields.tabPanelContent[0].fields.header,
        };
      } else if (id === 'gitlab-saas') {
        gitlabSaas = {
          text: tab.fields.tabPanelContent[0].fields.title,
          tooltip_text:
            tab.fields.tabPanelContent[0].fields.contentArea[0].fields.text,
          bottom_text: tab.fields.tabPanelContent[0].fields.description,
          link: {
            text: tab.fields.tabPanelContent[0].fields.button.fields.text,
            href: tab.fields.tabPanelContent[0].fields.button.fields
              .externalUrl,
            data_ga_name:
              tab.fields.tabPanelContent[0].fields.button.fields.dataGaName,
            data_ga_location:
              tab.fields.tabPanelContent[0].fields.button.fields.dataGaLocation,
          },
        };
      }
    });

    return {
      name: this.componentNames.FREE_TRIAL_PLANS,
      data: {
        saas: { ...gitlabSaas },
        self_managed: { ...selfManaged },
      },
    };
  }

  private mapHero(ctfHero: any) {
    const {
      internalName,
      title,
      subheader,
      description,
      video,
      primaryCta,
      customFields,
    } = ctfHero.fields;

    let key = 'default';

    if (internalName.includes('SMB')) {
      key = 'smb';
    } else if (internalName.includes('CI')) {
      key = 'ci';
    } else if (internalName.includes('Security')) {
      key = 'security';
    }

    return {
      name: this.componentNames.HERO,
      internalName,
      data: {
        video_url: video.fields.url,
        repeated_button_text: primaryCta.fields.text,
        [key]: {
          heading: title,
          subtitle: subheader,
          terms: description,
          icon_list: customFields.icon_list,
        },
      },
    };
  }

  private mapTwoColumnBlock(ctfTwoColumnBlock: any) {
    const { header, subheader, cta, customFields } = ctfTwoColumnBlock.fields;

    return {
      name: this.componentNames.G2_CATEGORIES,
      data: {
        header,
        subtitle: subheader,
        cta_text: cta.fields.text,
        data_ga_name: cta.fields.dataGaName,
        data_ga_location: cta.fields.dataGaLocation,
        badges: customFields.badges,
      },
    };
  }

  private mapCardGroup(ctfCardGroup: any) {
    const cards = ctfCardGroup.fields.card.map((card) => {
      return {
        heading: card.fields.title,
        text: card.fields.description,
        icon_src: card.fields.customFields.icon_src,
      };
    });

    return {
      name: this.componentNames.INFORMATION_SQUARES,
      data: {
        information_squares: cards,
      },
    };
  }

  private mapNextSteps(ctfNextSteps: any) {
    const { heading, tagline, secondaryTagline, primaryButton } =
      ctfNextSteps.fields;

    return {
      name: this.componentNames.NEXT_STEPS,
      data: {
        heading,
        subtitle: tagline,
        text: secondaryTagline,
        cta_text: primaryButton.fields.text,
      },
    };
  }
}
