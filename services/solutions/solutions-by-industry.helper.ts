import { COMPONENT_NAMES } from '../../common/constants';
import { getUrlFromContentfulImage } from '../../common/util';

export function mapIndustryIntro(ctfCustomerLogo: any) {
  const logos = ctfCustomerLogo.logo.map((logo: any) => ({
    ...logo.fields?.customFields,
    name: logo.fields?.altText,
    image: getUrlFromContentfulImage(logo.fields?.image),
    url: logo.fields?.link,
  }));

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_INTRO,
    data: {
      intro_text: ctfCustomerLogo.text,
      logos,
    },
  };
}

export function mapByIndustrySolutionsBlock(ctfCardGroup: any) {
  const solutions = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    title: card.fields?.title,
    description: card.fields?.description || '',
    list: card.fields?.list,
    link_text: card.fields?.button?.fields?.text,
    link_url: card.fields?.button?.fields?.externalUrl,
    data_ga_name: card.fields?.button?.fields?.dataGaName,
    data_ga_location: card.fields?.button?.fields?.dataGaLocation,
  }));

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_SOLUTIONS_BLOCK,
    data: {
      ...ctfCardGroup.customFields,
      subtitle: ctfCardGroup.header,
      sub_description: ctfCardGroup.description,
      sub_image: getUrlFromContentfulImage(ctfCardGroup.image?.fields?.image),
      alt: ctfCardGroup.image?.fields?.altText,
      solutions,
    },
  };
}

export function mapByIndustryQuotesCarousel(ctfBlockGroup: any) {
  const quotes = ctfBlockGroup.blocks.map((block: any) => {
    const { fields } = block;
    const title_img = fields.authorHeadshot && {
      url: getUrlFromContentfulImage(fields.authorHeadshot.fields.image),
      alt: fields.authorHeadshot.fields.altText,
    };

    return {
      ...fields.customFields,
      quote: fields.quoteText,
      author: fields.author,
      position: `${fields.authorTitle}, ${fields.authorCompany}`,
      url: fields.cta?.fields.externalUrl,
      title_img,
    };
  });

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_QUOTES_CAROUSEL,
    data: {
      ...ctfBlockGroup.customFields,
      header: ctfBlockGroup.header,
      quotes,
    },
  };
}

export function mapByIndustryCaseStudies(ctfCardGroup: any) {
  const rows = ctfCardGroup.card.map((card: any) => {
    const { fields } = card;
    const image = fields.image && {
      url: getUrlFromContentfulImage(fields.image),
      alt: fields.image.description,
    };

    const button = fields.button && {
      href: fields.button.fields.externalUrl,
      text: fields.button.fields.text,
      data_ga_name: fields.button.fields.dataGaName,
      data_ga_location: fields.button.fields.dataGaLocation,
    };

    return {
      ...fields.customFields,
      title: fields.title,
      subtitle: fields.description,
      button,
      image,
    };
  });

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_CASE_STUDIES,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      rows,
    },
  };
}

export function mapHomeSolutions(ctfCardGroup: any) {
  const solutions = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    title: card.fields?.title,
    description: card.fields?.description || '',
    list: card.fields?.list,
    link_text: card.fields?.button?.fields?.text,
    link_url: card.fields?.button?.fields?.externalUrl,
    data_ga_name: card.fields?.button?.fields?.dataGaName,
    data_ga_location: card.fields?.button?.fields?.dataGaLocation,
    image: getUrlFromContentfulImage(card.fields?.image),
    alt: card.fields?.image?.fields.altText,
  }));

  return {
    name: COMPONENT_NAMES.HOME_SOLUTIONS_CONTAINER,
    data: {
      ...ctfCardGroup.customFields,
      subtitle: ctfCardGroup.subtitle,
      solutions,
    },
  };
}
