import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '../../common/content-types';
import { getClient } from '../../plugins/contentful.js';
import { COMPONENT_NAMES, TEMPLATES } from '../../common/constants';
import { getUrlFromContentfulImage } from '../../common/util';
import {
  mapBenefits,
  mapCopy,
  mapCopyMedia,
  mapLogoLinks,
  mapFeaturedMedia,
  mapQuote,
  mapReportCta,
  mapSolutionsHero,
  mapVideoCarousel,
  mapTierBlock,
  mapSolutionsResourceCards,
  mapSolutionsCards,
  mapGroupButtons,
  mapSolutionsFeatureList,
  mapSolutionsVideo,
} from './solutions-default.helper';
import {
  mapByIndustryCaseStudies,
  mapByIndustryQuotesCarousel,
  mapByIndustrySolutionsBlock,
  mapHomeSolutions,
  mapIndustryIntro,
} from './solutions-by-industry.helper';
import {
  mapBySolutionBenefits,
  mapBySolutionIntro,
  mapBySolutionLink,
  mapBySolutionList,
  mapBySolutionShowcase,
  mapBySolutionValueProp,
} from './solutions-by-solution.helper';

export class SolutionsService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns Solutions content for the pages
   * @param slug
   * @param isIndexSlug
   */
  getContent(slug: string, isIndexSlug: boolean) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      return this.getContentfulData(slug);
    }

    // Localized data - YML files
    return this.$ctx
      .$content(this.$ctx.i18n.locale, `${slug}${isIndexSlug ? '/index' : ''}`)
      .fetch();
  }

  private async getContentfulData(slug: string) {
    const solutionEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 4,
    });

    if (solutionEntry.items.length === 0) {
      throw new Error('Not found');
    }

    const [solutions] = solutionEntry.items;

    return this.transformContentfulData(solutions);
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata, schema } = ctfData.fields;

    const mappedContent = this.mapPageContent(pageContent, schema?.template);
    const seoImage = getUrlFromContentfulImage(
      seoMetadata[0]?.fields?.ogImage?.fields?.image,
    );

    return {
      ...seoMetadata[0]?.fields,
      ...schema,
      twitter_image: seoImage,
      og_image: seoImage,
      image_title: seoImage,
      ...mappedContent,
    };
  }

  private mapPageContent(pageContent: any[], template: string) {
    const ctfReport = pageContent.find(
      (item) =>
        item.fields.componentName === 'report-cta' &&
        template !== TEMPLATES.Industry,
    );
    const ctfComponents = pageContent.filter(
      (item) =>
        item.fields.componentName !== 'report-cta' ||
        template === TEMPLATES.Industry,
    );

    const components = ctfComponents
      .map((component) => this.getComponentFromEntry(component))
      .filter((component) => component);
    const report_cta: any = ctfReport ? mapReportCta(ctfReport.fields) : {};

    return { components, report_cta: report_cta.data };
  }

  /**
   * Main method that switches which components should be used depending on the Content Type and Component Name
   * @param ctfEntry
   */
  private getComponentFromEntry(ctfEntry: any) {
    let component;

    switch (ctfEntry.sys.contentType.sys.id) {
      case CONTENT_TYPES.HERO: {
        component = mapSolutionsHero(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.BLOCK_GROUP: {
        component = this.mapBlockGroupComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.VIDEO_CAROUSEL: {
        component = mapVideoCarousel(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroupComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.QUOTE: {
        component = mapQuote(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CUSTOMER_LOGOS: {
        component = this.mapCustomerLogoComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CARD: {
        component = this.mapCardComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.EXTERNAL_VIDEO: {
        component = mapSolutionsVideo(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CUSTOM_COMPONENT: {
        component = { ...ctfEntry.fields };
        break;
      }
    }

    return component;
  }

  private mapCustomerLogoComponent(ctfCustomerLogo: any) {
    let component;

    switch (ctfCustomerLogo.componentName) {
      case COMPONENT_NAMES.LOGO_LINKS: {
        component = mapLogoLinks(ctfCustomerLogo);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_INTRO: {
        component = mapIndustryIntro(ctfCustomerLogo);
        break;
      }
    }

    return component;
  }

  private mapBlockGroupComponent(ctfBlockGroup: any) {
    let component;

    switch (ctfBlockGroup.componentName) {
      case COMPONENT_NAMES.COPY_MEDIA: {
        component = mapCopyMedia(ctfBlockGroup);
        break;
      }
      case COMPONENT_NAMES.COPY: {
        component = mapCopy(ctfBlockGroup);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_QUOTES_CAROUSEL: {
        component = mapByIndustryQuotesCarousel(ctfBlockGroup);
        break;
      }
    }

    return component;
  }

  private mapCardGroupComponent(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.componentName) {
      case COMPONENT_NAMES.BENEFITS: {
        component = mapBenefits(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.FEATURED_MEDIA: {
        component = mapFeaturedMedia(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_BENEFITS: {
        component = mapBySolutionBenefits(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_SOLUTIONS_BLOCK: {
        component = mapByIndustrySolutionsBlock(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.TIER_BLOCK: {
        component = mapTierBlock(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_RESOURCE_CARDS: {
        component = mapSolutionsResourceCards(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_CARDS: {
        component = mapSolutionsCards(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP: {
        component = mapBySolutionValueProp(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_CASE_STUDIES: {
        component = mapByIndustryCaseStudies(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.HOME_SOLUTIONS_CONTAINER: {
        component = mapHomeSolutions(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.GROUP_BUTTONS: {
        component = mapGroupButtons(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.REPORT_CTA: {
        component = mapReportCta(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_SHOWCASE: {
        component = mapBySolutionShowcase(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_FEATURE_LIST: {
        component = mapSolutionsFeatureList(ctfCardGroup);
        break;
      }
    }
    return component;
  }

  private mapCardComponent(ctfCard: any) {
    let component;
    switch (ctfCard.componentName) {
      case COMPONENT_NAMES.BY_SOLUTION_INTRO: {
        component = mapBySolutionIntro(ctfCard);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_LINK: {
        component = mapBySolutionLink(ctfCard);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_LIST: {
        component = mapBySolutionList(ctfCard);
        break;
      }
    }

    return component;
  }

  private mapSideNavigation(ctfSideMenu: any) {
    const links = ctfSideMenu.anchors
      .map((anchor: any) => anchor.fields)
      .map((anchor: any) => ({
        title: anchor.linkText,
        href: anchor.anchorLink,
        data_ga_name: anchor.dataGaName,
        data_ga_location: anchor.dataGaLocation,
      }));

    return {
      ...ctfSideMenu.customFields,
      name: COMPONENT_NAMES.SIDE_NAVIGATION_VARIANT,
      links,
      slot_content: ctfSideMenu.content.map((item) =>
        this.getComponentFromEntry(item),
      ),
    };
  }
}
