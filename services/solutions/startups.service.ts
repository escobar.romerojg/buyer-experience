/* eslint-disable babel/camelcase */
import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';

export class SolutionsStartupsService {
  private readonly $ctx: Context;

  private readonly componentNames = {
    SIDE_NAVIGATION: 'side-navigation-variant',
    CUSTOMER_LOGOS: 'customer-logos',
    HERO: 'solutions-hero',
    CTA_BLOCK: 'cta-block',
    OVERVIEW: 'solutions-video-feature',
    BY_SOLUTION_VALUE_PROP: 'by-solution-value-prop',
    CUSTOMERS: 'education-case-study-carousel',
  };

  private readonly componentInternalNames = {
    OVERVIEW: 'Solutions - Startups video feature',
    CUSTOMER_LOGOS: 'Solutions - Startups customers logo',
    CTA_BLOCK: 'Solutions - Startups CTA block',
    BENEFITS: 'Solutions - Startups by solution value prop',
    CUSTOMERS: 'Solutions - Startups case study carousel',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns components to the /solutions/startups/* pages
   * @param slug
   */
  async getContent(slug: string) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;
    // Default locale data - Contentful
    if (isDefaultLocale) {
      try {
        return this.getContentfulData(slug);
      } catch (e) {
        throw new Error(e);
      }
    }

    try {
      const content = this.$ctx
        .$content(this.$ctx.i18n.locale, `${slug}/index`)
        .fetch();
      // Localized data - YML files
      return content;
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string) {
    try {
      const content = await getClient().getEntries({
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
      });

      if (content.items.length === 0) {
        throw new Error('Not found');
      }

      const [solutions] = content.items;

      return this.transformContentfulData(solutions);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getPageComponents(pageContent);

    const sideNav: any = mappedContent.filter(
      (component) => component.name === this.componentNames.SIDE_NAVIGATION,
    )[0];

    const hero: any = mappedContent.filter(
      (component) => component.name === this.componentNames.HERO,
    )[0];

    const customerLogos: any = mappedContent.filter(
      (component) => component.name === this.componentNames.CUSTOMER_LOGOS,
    )[0];

    const ctaBlock: any = mappedContent.filter(
      (component) => component.name === this.componentNames.CTA_BLOCK,
    )[0];

    const otherComponents = mappedContent.filter(
      (component) =>
        component.name !== this.componentNames.HERO &&
        component.name !== this.componentNames.CUSTOMER_LOGOS &&
        component.name !== this.componentNames.CTA_BLOCK &&
        component.name !== this.componentNames.SIDE_NAVIGATION,
    );

    const newSideNav = {
      name: this.componentNames.SIDE_NAVIGATION,
      slot_enabled: true,
      links: sideNav.links,
      slot_content: [...otherComponents],
    };

    const newArray = [];

    newArray.push(hero);
    newArray.push(customerLogos);
    newArray.push(newSideNav);
    newArray.push(ctaBlock);

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      template: 'industry',
      components: newArray,
    };

    return transformedPage;
  }

  private getPageComponents(pageContent: any[]) {
    const components: any[] = [];

    pageContent.forEach((ctfComponent) => {
      const mappedComponent = this.mapCtfComponent(ctfComponent);
      components.push(mappedComponent);
    });

    return components;
  }

  private mapCtfComponent(ctfComponent: any) {
    let component;

    const { id } = ctfComponent.sys.contentType.sys;

    switch (id) {
      case CONTENT_TYPES.HERO: {
        component = this.mapHero(ctfComponent);
        break;
      }

      case CONTENT_TYPES.SIDE_NAVIGATION: {
        component = this.mapSideNavigation(ctfComponent);
        break;
      }

      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroup(ctfComponent);
        break;
      }

      default:
        break;
    }

    return component;
  }

  private mapSideNavigation(ctfSideNavigation: any) {
    const { anchors } = ctfSideNavigation.fields;

    return {
      name: this.componentNames.SIDE_NAVIGATION,
      slot_enabled: true,
      links: anchors.map((anchor) => {
        return {
          title: anchor.fields.linkText,
          href: anchor.fields.anchorLink,
        };
      }),
    };
  }

  private mapHero(ctfHero: any) {
    const { title, subheader, primaryCta, backgroundImage } = ctfHero.fields;

    return {
      name: this.componentNames.HERO,
      data: {
        title,
        subtitle: subheader,
        aos_animation: 'fade-down',
        aos_duration: 500,
        title_variant: 'display1',
        mobile_title_variant: 'heading1-bold',
        img_animation: 'zoom-out-left',
        img_animation_duration: 1600,
        primary_btn: {
          url: primaryCta.fields.externalUrl,
          text: primaryCta.fields.text,
          data_ga_name: primaryCta.fields.dataGaName,
          data_ga_location: primaryCta.fields.dataGaLocation,
        },
        image: {
          bordered: true,
          image_url: backgroundImage.fields.file.url,
          alt: '',
        },
      },
    };
  }

  private mapCardGroup(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.fields.internalName.trim()) {
      case this.componentInternalNames.CUSTOMER_LOGOS:
        component = this.mapCustomerLogos(ctfCardGroup);
        break;

      case this.componentInternalNames.OVERVIEW:
        component = this.mapVideoFeature(ctfCardGroup);
        break;

      case this.componentInternalNames.BENEFITS:
        component = this.mapBySolutionValueProp(ctfCardGroup);
        break;

      case this.componentInternalNames.CUSTOMERS:
        component = this.mapCaseStudyCarousel(ctfCardGroup);
        break;

      case this.componentInternalNames.CTA_BLOCK:
        component = this.mapCtaBlock(ctfCardGroup);
        break;

      default:
        break;
    }
    return component;
  }

  private mapCustomerLogos(ctfCustomerLogos: any) {
    const { card, customFields } = ctfCustomerLogos.fields;

    return {
      name: this.componentNames.CUSTOMER_LOGOS,
      data: {
        text_variant: customFields.text_variant,
        companies: card.map((logo) => {
          return {
            image_url: logo.fields.image.fields.file.url,
            link_label: logo.fields.button.fields.text,
            alt: logo.fields.title,
            url: logo.fields.button.fields.externalUrl,
          };
        }),
      },
    };
  }

  private mapVideoFeature(ctfVideoFeature: any) {
    const { title, description, video } = ctfVideoFeature.fields.card[0].fields;
    return {
      name: 'div',
      id: 'overview',
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.OVERVIEW,
          data: {
            header: title,
            description,
            video: {
              url: video.fields.url,
            },
          },
        },
      ],
    };
  }

  private mapCaseStudyCarousel(ctfCaseStudyCarousel: any) {
    const { header, cta, card: cards } = ctfCaseStudyCarousel.fields;

    return {
      name: 'div',
      id: 'customers',
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.CUSTOMERS,
          data: {
            header,
            customers_cta: true,
            cta: {
              href: cta.fields.externalUrl,
              text: cta.fields.text,
              data_ga_name: cta.fields.dataGaName,
            },
            case_studies: cards.map((card) => {
              return {
                logo_url: card.fields.image.fields.file.url,
                instituion_name:
                  card.fields.contentArea[0].fields.authorCompany,
                quote: {
                  img_url:
                    card.fields.contentArea[0].fields.authorHeadshot.fields
                      .image.fields.file.url,
                  quote_text: card.fields.contentArea[0].fields.quoteText,
                  author: card.fields.contentArea[0].fields.author,
                  author_title: card.fields.contentArea[0].fields.authorTitle,
                },
                case_study_url: card.fields.cardLink,
                data_ga_name: card.fields.cardLinkDataGaName,
                data_ga_location: card.fields.cardLinkDataGaLocation,
              };
            }),
          },
        },
      ],
    };
  }

  private mapCtaBlock(ctfCtaBlock: any) {
    return {
      name: this.componentNames.CTA_BLOCK,
      data: {
        cards: ctfCtaBlock.fields.card.map((card) => {
          return {
            header: card.fields.title,
            icon: card.fields.iconName,
            link: {
              text: card.fields.button.fields.text,
              url: card.fields.button.fields.externalUrl,
              data_ga_name: card.fields.button.fields.dataGaName,
            },
          };
        }),
      },
    };
  }

  private mapBySolutionValueProp(ctfBySolutionValueProp: any) {
    const {
      header,
      componentName,
      card: cards,
      customFields,
    } = ctfBySolutionValueProp.fields;

    return {
      name: 'div',
      id: 'benefits',
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.BY_SOLUTION_VALUE_PROP,
          data: {
            title: header,
            componentName,
            light_background: customFields.light_background,
            large_card_on_bottom: customFields.large_card_on_bottom,
            cards: cards.map((card) => {
              const { title, description, iconName } = card.fields;

              const data = {
                title,
                description,
                icon: {
                  name: iconName,
                  alt: `${iconName} icon`,
                  variant: 'marketing',
                },
              };

              if (card.fields.button) {
                return {
                  ...data,
                  href: card.fields.button.fields.externalUrl,
                  data_ga_name: card.fields.button.fields.dataGaName,
                  cta: card.fields.button.fields.text,
                };
              }

              if (card.fields.cardLink) {
                return {
                  ...data,
                  href: card.fields.cardLink,
                  data_ga_name: card.fields.cardLinkDataGaName,
                };
              }

              return data;
            }),
          },
        },
      ],
    };
  }
}
