export function partnerBenefitsService(data: any[]) {
  let pageData = {
    components: [],
    next_steps_variant: null,
  };

  // Hero
  const heroData = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );

  const hero = {
    name: 'hero',
    data: {
      title: heroData[0].fields.title,
      text: heroData[0].fields.description,
      aos_animation: 'fade-down',
      aos_duration: 500,
      img_animation: 'zoom-out-left',
      img_animation_duration: 1600,
      primary_btn: {
        text: heroData[0].fields.primaryCta.fields.text,
        url: heroData[0].fields.primaryCta.fields.externalUrl,
        data_ga_name: heroData[0].fields.primaryCta.fields.dataGaName
          ? heroData[0].fields.primaryCta.fields.dataGaName
          : null,
        data_ga_location: heroData[0].fields.primaryCta.fields.dataGaLocation
          ? heroData[0].fields.primaryCta.fields.dataGaLocation
          : 'hero',
      },
      image: {
        url: heroData[0].fields.backgroundImage.fields.file.url,
        alt: '',
      },
    },
  };

  // Next Steps Variant
  const nextSteps = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'nextSteps',
  );

  pageData.next_steps_variant = nextSteps[0].fields.variant;

  // Create body
  const rawContent = data.filter(
    (obj) =>
      obj.sys.contentType.sys.id !== 'eventHero' &&
      obj.sys.contentType.sys.id !== 'nextSteps',
  );

  pageData.components = rawContent.map((section, index) => {
    if (
      section.sys.contentType.sys.id == 'headerAndText' &&
      rawContent[index + 1].sys.contentType.sys.id === 'button'
    ) {
      return {
        name: 'topics-copy-block',
        data: {
          header: section.fields.header,
          column_size: 9,
          top_margin: index == 0 ? 'slp-mt-md-64' : '',
          blocks: [
            {
              text: section.fields.text,
              link: {
                url: rawContent[index + 1].fields.externalUrl,
                text: rawContent[index + 1].fields.text,
                variant: rawContent[index + 1].fields.variation
                  ? rawContent[index + 1].fields.variation
                  : 'primary',
                data_ga_name: rawContent[index + 1].fields.dataGaName
                  ? rawContent[index + 1].fields.dataGaName
                  : null,
                data_ga_location: rawContent[index + 1].fields.dataGaLocation
                  ? rawContent[index + 1].fields.dataGaLocation
                  : 'body',
              },
            },
          ],
        },
      };
    } else if (
      section.sys.contentType.sys.id == 'headerAndText' &&
      rawContent[index + 1].sys.contentType.sys.id !== 'button'
    ) {
      return {
        name: 'topics-copy-block',
        data: {
          header: section.fields.header,
          top_margin: index == 0 ? 'slp-mt-md-64' : '',
          column_size: 9,
          blocks: [
            {
              text: section.fields.text,
            },
          ],
        },
      };
    } else if (section.sys.contentType.sys.id == 'cardGroup') {
      return {
        name: section.fields.componentName,
        data: {
          padding: 'slp-mt-32 slp-mt-md-48',
          items: section.fields.card.map((card) => {
            return {
              title: card.fields.title,
              icon: {
                name: card.fields.iconName,
                alt: '',
                variant: 'marketing',
              },
            };
          }),
        },
      };
    } else if (section.sys.contentType.sys.id == 'blockGroup') {
      return {
        name: section.fields.componentName,
        data: {
          header: section.fields.header,
          ...section.fields.customFields,
          quotes: section.fields.blocks.map((quote) => {
            return {
              quote: quote.fields.quoteText,
              author: quote.fields.author,
              title: quote.fields.authorTitle,
              company: quote.fields.authorCompany,
            };
          }),
        },
      };
    } else return {};
  });

  pageData.components.unshift(hero);
  return pageData;
}
