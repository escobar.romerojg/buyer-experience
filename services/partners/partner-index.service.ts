export function partnerIndexService(data: any[]) {
  let pageData = {
    components: null,
    next_steps_variant: null,
  };

  // Hero
  const heroData = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );

  const hero = {
    name: 'hero',
    data: {
      title: heroData[0].fields.title,
      text: heroData[0].fields.description,
      aos_animation: 'fade-down',
      aos_duration: 500,
      img_animation: 'zoom-out-left',
      img_animation_duration: 1600,
      primary_btn: {
        text: heroData[0].fields.primaryCta.fields.text,
        url: heroData[0].fields.primaryCta.fields.externalUrl,
        data_ga_name: heroData[0].fields.primaryCta.fields.dataGaName
          ? heroData[0].fields.primaryCta.fields.dataGaName
          : null,
        data_ga_location: heroData[0].fields.primaryCta.fields.dataGaLocation
          ? heroData[0].fields.primaryCta.fields.dataGaLocation
          : 'hero',
      },
      secondary_btn: {
        text: heroData[0].fields.secondaryCta.fields.text,
        url: heroData[0].fields.secondaryCta.fields.externalUrl,
        data_ga_name: heroData[0].fields.secondaryCta.fields.dataGaName,
        data_ga_location: heroData[0].fields.secondaryCta.fields.dataGaLocation,
      },
      tertiary_btn: {
        text: heroData[0].fields.customFields.tertiary_btn.text,
        url: heroData[0].fields.customFields.tertiary_btn.url,
        data_ga_name: heroData[0].fields.customFields.tertiary_btn.dataGaName,
        data_ga_location:
          heroData[0].fields.customFields.tertiary_btn.dataGaLocation,
      },
      image: {
        url: heroData[0].fields.backgroundImage.fields.file.url,
        alt: '',
      },
    },
  };

  // Next Steps Variant
  const nextSteps = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'nextSteps',
  );

  pageData.next_steps_variant = nextSteps[0].fields.variant;

  // Create body
  const rawContent = data.filter(
    (obj) =>
      obj.sys.contentType.sys.id !== 'eventHero' &&
      obj.sys.contentType.sys.id !== 'nextSteps',
  );

  pageData.components = rawContent.map((section, index) => {
    if (section.sys.contentType.sys.id === 'headerAndText') {
      if (
        rawContent[index + 1] &&
        rawContent[index + 1].sys.contentType.sys.id === 'button'
      ) {
        return {
          name: 'copy-block',
          data: {
            header: section.fields.header,
            no_decoration: section.fields.header ? false : true,
            top_margin: index == 0 ? 'slp-mt-md-64' : '',
            column_size: 9,
            blocks: [
              {
                text: section.fields.text,
                link: {
                  url: rawContent[index + 1].fields.externalUrl,
                  text: rawContent[index + 1].fields.text,
                  variant: rawContent[index + 1].fields.variation
                    ? rawContent[index + 1].fields.variation
                    : 'primary',
                  data_ga_name: rawContent[index + 1].fields.dataGaName
                    ? rawContent[index + 1].fields.dataGaName
                    : null,
                  data_ga_location: rawContent[index + 1].fields.dataGaLocation
                    ? rawContent[index + 1].fields.dataGaLocation
                    : 'body',
                },
              },
            ],
          },
        };
      } else {
        return {
          name: 'copy-block',
          data: {
            header:
              section.fields.header != undefined ? section.fields.header : null,
            no_decoration: section.fields.header == undefined ? true : false,
            top_margin: index == 0 ? 'slp-mt-md-64' : '',
            column_size: 9,
            blocks: [
              {
                text: section.fields.text,
              },
            ],
          },
        };
      }
    } else if (section.sys.contentType.sys.id === 'cardGroup') {
      if (section.fields.componentName === 'partners-showcase') {
        return {
          name: 'partners-showcase',
          data: {
            padding: 'slp-my-32 slp-my-md-48',
            clickable: section.fields.card[0].fields.button ? true : false,
            items: section.fields.card.map((item) => {
              return {
                image: {
                  src: item.fields.image.fields.file.url,
                  alt: item.fields.image.fields.title,
                },
                text: item.fields.description,
                url: item.fields.button
                  ? {
                      text: item.fields.button.fields.text,
                      href: item.fields.button.fields.externalUrl,
                      ga: {
                        data_ga_name: item.fields.button.fields.dataGaName,
                        data_ga_location:
                          item.fields.button.fields.dataGaLocation,
                      },
                    }
                  : null,
              };
            }),
          },
        };
      } else if (section.fields.componentName === 'resources') {
        return {
          name: 'resources',
          data: {
            header: section.fields.header,
            links: section.fields.card.map((item) => {
              return {
                name: item.fields.title,
                link: item.fields.cardLink,
                description: item.fields.description,
                ga: {
                  name: item.fields.cardLinkDataGaName,
                  location: item.fields.cardLinkDataGaLocation,
                },
              };
            }),
          },
        };
      } else if (section.fields.componentName === 'intro') {
        return {
          name: 'intro',
          data: {
            ...section.fields.customFields,
            logos: section.fields.card.map((item) => {
              return {
                name: item.fields.internalName,
                image: item.fields.image.fields.file.url,
                url: item.fields.cardLink,
                aria_label: item.fields.customFields.aria_label,
              };
            }),
          },
        };
      }
    } else if (section.sys.contentType.sys.id === 'quote') {
      return {
        name: 'quotes-carousel',
        data: {
          ...section.fields.customFields,
          quotes: [
            {
              main_img: {
                url: section.fields.authorHeadshot.fields.image.fields.file.url,
                alt: section.fields.authorHeadshot.fields.altText,
              },
              quote: section.fields.quoteText,
              author: section.fields.author,
              job: `${section.fields.authorTitle}, ${section.fields.authorCompany}`,
            },
          ],
        },
      };
    } else return {};
  });

  pageData.components.unshift(hero);
  return pageData;
}
