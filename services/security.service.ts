import { Context } from '@nuxt/types';
import { getUrlFromContentfulImage } from '../common/util';
import { CONTENT_TYPES } from '../common/content-types';
import { getClient } from '~/plugins/contentful.js';

export class SecurityService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  getContent(slug: string) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      return this.getContentfulData(slug);
    }

    // Localized data - YML files
    return this.$ctx.$content(this.$ctx.i18n.locale, slug).fetch();
  }

  private async getContentfulData(slug: string) {
    const securityEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 3,
    });

    const [security] = securityEntry.items;
    return this.transformContentfulData(security);
  }

  private transformContentfulData(entryData) {
    const { pageContent, seoMetadata } = entryData.fields;

    const components = pageContent.map((component) =>
      this.getComponentFromEntry(component),
    );

    return { ...seoMetadata[0].fields, components };
  }

  private getComponentFromEntry(entry: any) {
    let component;

    if (entry.sys.contentType.sys.id === 'customComponent') {
      console.log('asigning custom component', entry);
      return { data: entry.fields.data, name: entry.fields.name };
    }

    switch (entry.fields.componentName) {
      case 'solutions-hero': {
        component = this.mapHero(entry.fields);
        break;
      }
      case 'security-compliance-cards': {
        component = this.mapComplianceCards(entry.fields);
        break;
      }
      case 'solutions-resource-cards': {
        component = this.mapResourceCards(entry.fields);
        break;
      }
      case 'security-banner-spotlight-card': {
        component = this.mapSecurityBannerSpotlight(entry.fields);
        break;
      }
      case 'security-cta-section': {
        component = this.mapSecurityBannerCard(entry.fields);
        break;
      }
      case 'solutions-cards': {
        component = this.mapSolutionsCards(entry.fields);
        break;
      }
    }

    return component;
  }

  private mapHero({ subheader, title, description, backgroundImage }) {
    return {
      name: 'solutions-hero',
      data: {
        note: [subheader],
        title,
        subtitle: description,
        aos_animation: 'fade-down',
        aos_duration: 500,
        aos_offset: 200,
        img_animation: 'zoom-out-left',
        img_animation_duration: 1600,
        image: {
          image_url: backgroundImage.fields.file.url,
          image_url_mobile: backgroundImage.fields.file.url,
          alt: backgroundImage.fields.title,
          bordered: true,
        },
      },
    };
  }

  private mapComplianceCards({ header, card }) {
    return {
      name: 'security-compliance-cards',
      data: {
        background: 'purple',
        column_size: 4,
        header,
        cards: card.map((card: any) => ({
          title: card.fields.title,
          description: card.fields.description,
          link: {
            url: card.fields.cardLink,
            text: 'Learn More',
          },
          image: {
            alt: card.fields.image.fields.title,
            src: card.fields.image.fields.file.url,
          },
        })),
      },
    };
  }
  private mapResourceCards({ header, card }) {
    return {
      name: 'solutions-resource-cards',
      data: {
        title: header,
        link: {
          data_ga_location: 'security',
          data_ga_name: 'more security post',
          href: '/blog/categories/open-source/',
          text: 'More security post',
        },
        cards: card.map((card: any) => ({
          data_ga_location: 'security',
          data_ga_name: card.fields.title,
          description: card.fields.description,
          event_type: 'Blog',
          href: card.fields.cardLink,
          icon: {
            name: 'blog',
            variant: 'marketing',
            alt: 'Blog Icon',
          },
          image: card.fields.image.fields.file.url,
          link_text: 'Read post',
          title: card.fields.title,
        })),
      },
    };
  }
  private mapSecurityBannerSpotlight({ title, description, cardLink, image }) {
    return {
      name: 'security-banner-spotlight-card',
      data: {
        button: {
          href: cardLink,
          text: 'Learn More',
          data_ga_name: 'Spotlight Learn more',
          data_ga_location: 'body',
        },
        description,
        image: {
          alt: image.fields.title,
          src: image.fields.file.url,
        },
        title,
      },
    };
  }
  private mapSecurityBannerCard({ card }) {
    return {
      name: 'security-cta-section',
      data: {
        layout: 'slim',
        cards: card.map((card: any) => ({
          title: card.fields.title,
          link: {
            url: card.fields.button.fields.externalUrl,
            text: card.fields.button.fields.text,
          },
          icon: {
            name: card.fields.iconName,
            slp_color: 'surface-700',
          },
        })),
      },
    };
  }
  private mapSolutionsCards({ header, card }) {
    return {
      name: 'solutions-cards',
      data: {
        title: header,
        column_size: 4,
        link: {
          data_ga_location: 'security solutions',
          data_ga_name: 'all solutions',
          text: 'Explore more Solutions',
          url: '/solutions/',
        },
        cards: card.map((card: any) => ({
          title: card.fields.title,
          description: card.fields.description,
          href: card.fields.cardLink,
          icon: {
            variant: 'marketing',
            name: card.fields.iconName,
            alt: `${card.fields.title} icon`,
          },
        })),
      },
    };
  }
}
