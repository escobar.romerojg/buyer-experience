interface Hero {
  name: string;
  data: {
    title: string;
    [key: string]: any;
  };
}

interface CopyMedia {
  name: string;
  data: {
    block: {
      header: string;
      miscellaneous: string;
      metadata: {
        id_tag: string;
      };
    }[];
  };
}

interface PageData {
  side_menu: {
    links: {
      text: string;
      href: string;
      data_ga_name: string;
      data_ga_location: string;
    }[];
  };
  components: (Hero | CopyMedia)[];
}

const filterDataByContentType = (data: any[], contentType: string) => {
  return data.filter((obj) => obj.sys.contentType.sys.id === contentType);
};

export function getHelpDataHelper(data: any[]) {
  let pageData: PageData = {
    side_menu: {
      links: [],
    },
    components: [],
  };

  // Hero
  const heroData = filterDataByContentType(data, 'eventHero');
  if (heroData.length > 0) {
    const hero: Hero = {
      name: heroData[0].fields.componentName,
      data: {
        title: heroData[0].fields.title,
        ...heroData[0].fields.customFields,
      },
    };
    pageData.components.push(hero);
  }

  // Side Navigation
  const sideNav = filterDataByContentType(data, 'sideMenu');
  if (sideNav.length > 0) {
    pageData.side_menu.links = sideNav[0].fields.anchors.map((anchor) => ({
      text: anchor.fields.linkText,
      href: anchor.fields.anchorLink,
      data_ga_name: anchor.fields.dataGaName,
      data_ga_location: anchor.fields.dataGaLocation,
    }));
  }

  // Content
  const contentData = filterDataByContentType(data, 'headerAndText');
  pageData.components.push(
    ...contentData.map((section) => ({
      name: 'copy-media',
      data: {
        block: [
          {
            header: section.fields.header,
            miscellaneous: section.fields.text,
            metadata: {
              id_tag: section.fields.headerAnchorId,
            },
          },
        ],
      },
    })),
  );

  return pageData;
}
