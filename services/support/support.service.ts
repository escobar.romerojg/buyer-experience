import { SupportChildData } from './support-interfaces';
import { toKebabCase } from '@/lib/utils';

export function supportDataHelper(data: any[]) {
  let supportData: SupportChildData = {
    supportHero: {
      title: undefined,
      content: undefined,
    },
    side_menu: {
      anchors: {
        text: '',
        data: [],
      },
      hyperlinks: {
        text: '',
        data: [],
      },
    },
    components: [],
  };

  // Take in raw data from Contentful.
  let heroObjects = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );
  let sideNavObjects = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'sideMenu',
  );
  let rawContent = data.filter(
    (obj) =>
      obj.sys.contentType.sys.id === 'headerAndText' ||
      obj.sys.contentType.sys.id === 'tabControlsContainer' ||
      obj.sys.contentType.sys.id === 'card',
  );

  const createHero = (content: any) => {
    supportData.supportHero = {
      title: content.fields.title || null,
      content: content.fields.description || null,
    };
  };

  const createSideNav = (rawObj: any) => {
    const content = rawObj.fields;

    supportData.side_menu = {
      anchors: {
        text: content.anchorsText,
        data: content.anchors.map((anchor) => {
          let anchorData = {
            text: anchor.fields.linkText,
            href: anchor.fields.anchorLink,
          };

          if (anchor.fields.nodes) {
            anchorData.nodes = anchor.fields.nodes.nodes.map((node) => ({
              text: node.text,
              href: node.href,
            }));
          }

          return anchorData;
        }),
      },
      hyperlinks: {
        text: content.hyperlinksText,
        data: content.hyperlinks.map((hyperlink) => {
          return {
            text: hyperlink.fields.text,
            href: hyperlink.fields.externalUrl,
            variant: hyperlink.fields.variation,
            data_ga_name: hyperlink.fields.dataGaName,
            data_ga_location: hyperlink.fields.dataGaLocation,
            icon: true,
          };
        }),
      },
    };
  };

  const createBody = (rawBody: any[]) => {
    const sections = rawBody.map((section) => {
      if (section.sys.contentType.sys.id === 'headerAndText') {
        return {
          name: 'SupportCopyMarkdown',
          header: section.fields.header || null,
          headerId: section.fields.headerAnchorId || null,
          text: section.fields.text,
        };
      } else if (section.sys.contentType.sys.id === 'tabControlsContainer') {
        return {
          name: 'SupportNote',
          icon: 'magnifying-glass',
          title: section.fields.header,
          id: toKebabCase(section.fields.header),
          options: section.fields.tabs.map((tab) => {
            return tab.fields.tabButtonText;
          }),
          select_menu: {
            placeholder: section.fields.tabControlsSubtext,
            options: section.fields.tabs.map((tab, index) => {
              return {
                id: index,
                name: tab.fields.tabButtonText,
                text: tab.fields.tabPanelContent[0].fields.text,
              };
            }),
          },
        };
      } else if (section.sys.contentType.sys.id === 'card') {
        return {
          name: 'SupportNote',
          icon: section.fields.iconName,
          title: section.fields.title,
          text: section.fields.description,
          cta: {
            text: section.fields.button.fields.text,
            href: section.fields.button.fields.externalUrl,
            data_ga_name: section.fields.button.fields.dataGaName,
            data_ga_location: section.fields.button.fields.dataGaLocation,
          },
        };
      } else return;
    });

    supportData.components = sections;
  };

  // Build out /support landing page structure
  if (heroObjects[0]) createHero(heroObjects[0]);
  if (sideNavObjects[0]) createSideNav(sideNavObjects[0]);
  createBody(rawContent);

  // Return data to Vue template
  return supportData;
}
