interface Section {
  header: string;
  text?: string;
  cta?: string;
  image?: object;
  sponsors?: object[];
  speakers?: object[];
}

interface BaseEventData {
  metadata: any;
  name: string;
  internalName: string;
  slug: string;
  eventType?: string;
  description?: string;
  date?: string;
  endDate?: string;
  location?: string;
  region?: string;
  hero: any;
  agenda: any;
  featuredContent?: any[];
  booth?: any[];
  social?: any[];
  sessions?: any[];
  careers?: any;
  footnote?: Section[];
  nextSteps?: any;
}
export interface StandardEventData extends BaseEventData {
  booth?: Section[];
  featuredContent?: Section[];
  social?: Section[];
  sessions?: Section[];
  careers?: any;
}

function imageBuilder(data) {
  return {
    alt: data?.altText || data?.title || '',
    src: data?.image?.fields?.file?.url || data?.file?.url || '',
    description: data?.image?.fields?.description || '',
  };
}
function speakerBuilder(speaker) {
  return {
    name: speaker.author,
    title: speaker.authorTitle,
    company: speaker.authorCompany,
    image:
      speaker.authorHeadshot && imageBuilder(speaker.authorHeadshot?.fields),
    biography: speaker.quoteText,
  };
}
function mapArray(array) {
  return array
    .filter((element) => element.fields)
    .map((element) => {
      if (element.fields.assets) {
        element.fields.assets = element.fields.assets.map(
          (asset) => asset.fields,
        );
      }
      if (element.fields.cta) {
        element.fields.cta = element.fields.cta.fields;
      }
      return element.fields;
    });
}
function dataCleanUp(data) {
  const mappedData = {};

  Object.keys(data).forEach((key) => {
    if (Array.isArray(data[key])) {
      mappedData[key] = mapArray(data[key]);
    } else {
      mappedData[key] = data[key];
    }
  });

  return mappedData as StandardEventData;
}

function heroHelper(data) {
  const herofields = data.hero?.fields || null;
  const hero = {
    headline: data.name || '',
    accent: herofields?.title || '',
    subHeading: herofields?.subheader || '',
    list: (data.hero && herofields?.description?.split('\n')) || [],
    cta: herofields?.primaryCta?.fields || {},
    icons: data.staticFields && data.staticFields.icons,
  };

  return hero;
}
function sectionsHelper(data, key?) {
  let sections = [];
  if (data) {
    sections = data.map((section) => {
      const { header, text, subheader, cta, assets } = section;
      let parsedAssets = assets;
      if (assets) {
        parsedAssets = assets.map((asset) => {
          if (asset.image) {
            return imageBuilder(asset.image?.fields);
          }
          if (asset.authorHeadshot) {
            return speakerBuilder(asset);
          }
          return parsedAssets;
        });
      }
      return { header, text, subheader, cta, [key || 'assets']: parsedAssets };
    });
  }
  return sections;
}

function metaDataHelper(data) {
  const seo = data.seo?.fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    no_index: seo.noIndex,
  };
}
function agendaHelper(data) {
  let agenda = {};
  if (data) {
    agenda = { ...data?.shift(), schedule: [...data] };
  }
  return agenda;
}

function careersHelper(data) {
  let careersContent = null;
  if (data) {
    careersContent = { ...data[0] };
  }
  return careersContent;
}
export function standardEventsDataHelper(data) {
  const rawData = dataCleanUp(data);

  const transformedData: StandardEventData = {
    metadata: metaDataHelper(rawData),
    name: rawData.name || '',
    internalName: rawData.internalName || '',
    slug: rawData.slug || '',
    eventType: rawData.eventType || '',
    description: rawData.description || '',
    date: rawData.date || '',
    endDate: rawData.endDate || '',
    location: rawData.location || '',
    region: rawData.region || '',
    hero: rawData.hero && heroHelper(rawData),
    booth: sectionsHelper(rawData.booth),
    featuredContent: rawData.featuredContent,
    social: sectionsHelper(rawData.social),
    agenda: agendaHelper(rawData.agenda),
    sessions: sectionsHelper(rawData.sessions, 'speakers'),
    careers: careersHelper(rawData.careers),
    footnote: rawData.footnote,
    nextSteps: rawData.nextSteps?.fields.variant,
  };

  return transformedData;
}
