import {
  dataCleanUp,
  sectionsHelper,
  metaDataHelper,
  worldTourHeroHelper,
  formHelper,
  objectCleanUp,
  dateHelper,
} from '../../lib/eventsHelpers';

export interface WorldTourExecutiveEventsData {
  metadata: any;
  name: string;
  internalName: string;
  slug: string;
  eventType?: string;
  description?: string;
  date?: string;
  endDate?: string;
  location?: string;
  region?: string;
  hero: any;
  blurb: any;
  featuredContent?: any[];
  footnote?: any[];
  nextSteps?: any;
  sponsors?: any;
  breadcrumbs?: any;
  form?: any;
  staticFields?: any;
}

export function worldTourExecutiveEventsDataHelper(data) {
  const rawData = dataCleanUp(data) as WorldTourExecutiveEventsData;
  const date = rawData.date && dateHelper(rawData.date);

  const transformedData: WorldTourExecutiveEventsData = {
    metadata: metaDataHelper(rawData),
    name: rawData.name || '',
    internalName: rawData.internalName || '',
    slug: rawData.slug || '',
    eventType: rawData.eventType || '',
    description: rawData.description || '',
    date,
    endDate: rawData.endDate || '',
    location: rawData.location || '',
    region: rawData.region || '',
    hero:
      rawData.hero &&
      worldTourHeroHelper(
        rawData.hero?.fields,
        rawData.staticFields.breadcrumbs,
      ),
    blurb: objectCleanUp(rawData.blurb),
    form: formHelper(rawData.form.fields),
    sponsors:
      rawData.featuredContent &&
      sectionsHelper(rawData.featuredContent, 'sponsors'),
    footnote: rawData.footnote,
    nextSteps: rawData.nextSteps?.fields.variant,
  };

  return transformedData as WorldTourExecutiveEventsData;
}
