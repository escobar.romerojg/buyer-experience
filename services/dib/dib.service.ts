import {
  faqHelper,
  metaDataHelper,
  imageBuilder,
} from '../../lib/eventsHelpers';

interface DIBData {
  title: any;
  og_title?: any;
  description?: any;
  twitter_description?: any;
  og_description?: any;
  dib_hero?: any;
  dib_intro_highlight?: any;
  dib_copy_block_0?: any;
  dib_copy_block_1?: any;
  dib_workforce?: any;
  dib_workplace?: any;
  dib_marketplace?: any;
  quote?: any;
  faq?: any;
  dib_conclusion?: any;
  nextSteps?: any;
}

function arrayCleanUp(array) {
  return array.map((item) => item.fields);
}

function sectionBuilder(section) {
  const mappedData = {};

  Object.keys(section).forEach((key) => {
    if (Array.isArray(section[key])) {
      mappedData[key] = arrayCleanUp(section[key]);
    } else if (typeof section[key] === 'object') {
      mappedData[key] = section[key].fields;
    } else {
      mappedData[key] = section[key];
    }
  });
  return mappedData;
}
function readMoreBuilder(readMoreRaw) {
  return {
    button_text:
      (readMoreRaw.readMoreControlsText &&
        readMoreRaw.readMoreControlsText[0]) ||
      'Read More',
    button_text_open:
      (readMoreRaw.readMoreControlsText &&
        readMoreRaw.readMoreControlsText[1]) ||
      'Read Less',
    text: readMoreRaw.readMoreText,
  };
}

function dibCustomQuoteBuilder(quoteRaw) {
  return {
    text: quoteRaw.quoteText,
    name: quoteRaw.author,
    role: quoteRaw.authorTitle,
    headshot:
      quoteRaw.authorHeadshot && imageBuilder(quoteRaw.authorHeadshot?.fields),
    image:
      quoteRaw.authorHeadshot && imageBuilder(quoteRaw.authorHeadshot.fields),
  };
}
function copyBlockBuilder(section) {
  return {
    header: section.header,
    sub_header: section.subheader,
    text: section.text,
    read_more: section.readMoreText && readMoreBuilder(section),
    author: section.assets && {
      name: section.assets[0].fields.author,
      role: section.assets[0].fields.authorTitle,
    },
    image:
      section.assets &&
      imageBuilder(section.assets[0].fields.authorHeadshot.fields),
    ...section.customFields,
  };
}
function tabAccordion(section) {
  return section.map((item) => {
    return {
      header: item.fields.tabButtonText,
      icon: {
        name: item.fields.tabIconName,
        alt: item.fields.tabIconName,
        variant: 'marketing',
      },
      text: item.fields.tabPanelContent[0].fields.text,
      quote:
        item.fields.tabPanelContent[1] &&
        dibCustomQuoteBuilder(item.fields.tabPanelContent[1].fields),
    };
  });
}
function tabBuilder(tabContent) {
  const tabs = tabContent.map((tab) => {
    const sectionFields = tab.fields;
    const tabContentArray =
      sectionFields.tabPanelContent &&
      sectionFields.tabPanelContent.map((block) => block.fields);
    const tabContentObjects =
      tabContentArray &&
      tabContentArray.reduce((previous: any, current: any) => {
        const mergedArray = {
          ...previous,
          ...current,
        };

        return mergedArray;
      });
    const customFields = tabContentObjects?.customFields ?? {};

    return {
      header: sectionFields.tabButtonText,
      icon: sectionFields.tabIconName,
      content: tabContentObjects && tabContentObjects.text,
      read_more:
        tabContentObjects.readMoreText && readMoreBuilder(tabContentObjects),
      quote: tabContentObjects && dibCustomQuoteBuilder(tabContentObjects),
      accordion:
        tabContentObjects.singleAccordionGroupItems &&
        tabAccordion(tabContentObjects.singleAccordionGroupItems),
      ...customFields,
    };
  });
  return tabs;
}
function contentBuilder(blockContent) {
  const content = blockContent.map((block) => {
    const id = block.sys?.contentType?.sys?.id;
    switch (id) {
      case 'tabControlsContainer':
        return { tabs: tabBuilder(block.fields.tabs) };
      case 'quote':
        return { quote: { ...dibCustomQuoteBuilder(block.fields) } };
      default:
        return { [id]: { ...block.fields } };
    }
  });
  const tabContentObjects =
    content &&
    content.reduce((previous: any, current: any) => {
      const mergedArray = {
        ...previous,
        ...current,
      };

      return mergedArray;
    });
  return tabContentObjects;
}
function workForceBuilder(section) {
  const blockContent = section.blockGroup && contentBuilder(section.blockGroup);
  return {
    header: section.header,
    text: section.text,
    read_more: section.readMoreText && readMoreBuilder(section),
    image: section.assets && imageBuilder(section.assets[0].fields),
    ...blockContent,
  };
}
function introBuilder(section) {
  return {
    header: section.title,
    image: section.image && imageBuilder(section.image.fields),
    video: {
      image: section.video?.fields?.thumbnail.fields.image.fields.file.url,
      src: section.video?.fields.url,
      title: section.video?.fields.title,
    },
    copy: {
      header: section.subtitle,
      text: section.description,
    },
  };
}
function heroBuilder(section) {
  return {
    header: section.title,
    primary_btn: section.primaryCta && {
      text: section.primaryCta.fields.text,
      href: section.primaryCta.fields.externalUrl,
      data_ga_name: section.primaryCta.fields.dataGaName,
      data_ga_location: section.primaryCta.fields.dataGaLocation,
      icon: section.primaryCta.fields.iconName,
    },
    image:
      section.backgroundImage && imageBuilder(section.backgroundImage.fields),
  };
}
function pageCleanUp(page) {
  const sections = page.map((section) => {
    const id =
      section.fields?.componentId ||
      section.fields?.componentName ||
      section.sys?.contentType?.sys?.id;
    switch (id) {
      case 'dib_copy_block_0':
        return { [id]: { ...copyBlockBuilder(section.fields) } };
      case 'dib_copy_block_1':
        return { [id]: { ...copyBlockBuilder(section.fields) } };
      case 'dib_workforce':
        return { [id]: { ...workForceBuilder(section.fields) } };
      case 'dib_workplace':
        return { [id]: { ...workForceBuilder(section.fields) } };
      case 'dib_marketplace':
        return { [id]: { ...workForceBuilder(section.fields) } };
      case 'faq':
        return { [id]: { ...faqHelper(section.fields) } };
      case 'quote':
        return { quote: { ...dibCustomQuoteBuilder(section.fields) } };
      case 'eventHero':
        return { dib_hero: { ...heroBuilder(section.fields) } };
      case 'dib_intro_highlight':
        return { [id]: { ...introBuilder(section.fields) } };
      case 'nextSteps':
        return { nextSteps: section.fields.variant || '1' };
      default:
        return { [id]: { ...sectionBuilder(section.fields) } };
    }
  });

  const pagecontentObject = sections.reduce((previous: any, current: any) => {
    const mergedArray = {
      ...previous,
      ...current,
    };

    return mergedArray;
  });
  return pagecontentObject as DIBData;
}

export function dibHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = pageCleanUp(pageContent);
  return { ...metaDataHelper(seoMetadata[0]), ...cleanPagecontent };
}
