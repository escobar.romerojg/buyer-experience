/* eslint-disable babel/camelcase */
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { toKebabCase } from '@/lib/utils';

export async function fetchTopicBySlug(slug) {
  const completeSlug = `/topics/${slug}/`;

  const client = getClient();
  const entries = await client.getEntries({
    'fields.slug': completeSlug,
    content_type: CONTENT_TYPES.TOPICS,
    include: 2,
  });

  if (entries.items.length > 0) {
    return entries.items[0].fields;
  }
  return null;
}

export function handleTopicFromContentful(fields) {
  function generateBreadcrumbs() {
    const baseHref = '/';
    const crumbs = [];

    const parts = fields.slug.split('/').filter(Boolean);

    let currentPath = baseHref;

    parts.forEach((part, index) => {
      currentPath += `${part}/`;
      const title = part.replace(/-/g, ' ');
      const formattedTitle = title.charAt(0).toUpperCase() + title.slice(1);

      if (index !== parts.length - 1) {
        crumbs.push({
          title: formattedTitle,
          href: currentPath,
          data_ga_name: part,
          data_ga_location: 'breadcrumb',
        });
      }
    });

    crumbs.push({ title: fields?.title });

    return crumbs;
  }

  try {
    const topLevelFields = {
      title: fields.seo.fields.ogTitle,
      description: fields.seo.fields.ogDescription,
      date_published: fields.datePublished,
      date_modified: fields.dateModified,
      topic_name: fields.topicName,
      icon: fields.icon,
      topics_breadcrumb: true,
    };

    const topics_header = {
      data: {
        title: fields.topicName,
        block: [
          {
            text: fields.headerText,
          },
        ],
      },
    };

    if (fields.headerCta) {
      topics_header.data.block[0].link_text = fields.headerCta.fields.text;
      topics_header.data.block[0].link_href =
        fields.headerCta.fields.externalUrl;
    }

    const crumbs = generateBreadcrumbs();

    const modifiedBodyContent = [];

    fields.bodyContent.forEach((entry, index) => {
      if (entry.fields && entry.fields.url) {
        if (index > 0) {
          modifiedBodyContent[modifiedBodyContent.length - 1].fields.video_url =
            entry.fields.url;
        }
      } else {
        modifiedBodyContent.push(entry);
      }
    });

    const side_menu = {
      anchors: {
        text: fields.bodyContent.every((entry) => !entry.fields.header)
          ? ''
          : 'On this page',
        data: fields.bodyContent
          .filter((entry) => entry.fields && entry.fields.header)
          .map((entry) => {
            return {
              text: entry.fields.header,
              href: entry.fields.headerAnchorId
                ? entry.fields.headerAnchorId
                : `#${toKebabCase(entry.fields.header)}`,
              'data-ga-title': entry.fields.header.toLowerCase(),
              'data-ga-location': 'side-navigation',
            };
          }),
      },
      content: modifiedBodyContent
        .filter((entry) => entry.fields && entry.fields.text)
        .map((entry) => {
          if (entry.fields.video_url) {
            return {
              name: 'topics-copy-block',
              data: {
                header: entry.fields.header,
                column_size: 10,
                blocks: [
                  {
                    text: entry.fields.text,
                    video: { video_url: entry.fields.video_url },
                  },
                ],
              },
            };
          }
          return {
            name: 'topics-copy-block',
            data: {
              header: entry.fields.header,
              column_size: 10,
              blocks: [{ text: entry.fields.text }],
            },
          };
        }),
    };

    if (fields.sideMenuHyperlinks) {
      side_menu.hyperlinks = {
        text: fields.sideMenuTitle,
        data: fields.sideMenuHyperlinks
          .filter((entry) => entry.fields && entry.fields.text)
          .map((entry) => {
            return {
              text: entry.fields.text,
              href: entry.fields.externalUrl,
              variant: entry.fields.variation,
              'data-ga-name': entry.fields.text.toLowerCase(),
              'data-ga-location': 'side-navigation',
            };
          }),
      };
    }

    if (fields.footerCta) {
      const component = {
        name: 'topics-cta',
        data: {
          title: fields.footerCta.fields?.title,
          subtitle: fields.footerCta.fields?.subtitle,
          text: fields.footerCta.fields?.description,
          column_size: 10,
        },
      };

      if (fields.footerCta.fields?.button) {
        component.data.cta_one = {
          text: fields.footerCta.fields.button.fields.text,
          link: fields.footerCta.fields.button.fields.externalUrl,
          data_ga_name: fields.footerCta.fields.button.fields.text,
          data_ga_location: 'body',
        };
      }

      if (fields.footerCta.fields?.secondaryButton) {
        component.data.cta_two = {
          text: fields.footerCta.fields.secondaryButton.fields.text,
          link: fields.footerCta.fields.secondaryButton.fields.externalUrl,
          data_ga_name: fields.footerCta.fields.secondaryButton.fields.text,
          data_ga_location: 'body',
        };
      }
      side_menu.content.push(component);
    }

    const components = [];

    if (fields.groupedResourceTitle) {
      components.push({
        name: 'solutions-resource-cards',
        data: {
          title: fields.groupedResourceTitle,
          column_size: 4,
          grouped: true,
          cards: fields.groupedResourceCards.map((entry) => {
            const card = {
              icon: {
                name: entry.fields.iconName,
                variant: 'marketing',
                alt: '',
              },
              event_type: entry.fields.subtitle,
              header: entry.fields?.title,
              link_text: entry.fields?.button?.fields?.text,
              href: entry.fields?.button?.fields?.externalUrl,
              data_ga_name: entry.fields.title,
              data_ga_location: 'resource cards',
            };

            if (entry.fields.image?.fields?.file?.url) {
              card.image = `${entry.fields.image?.fields?.file?.url}?h=400&fl=progressive`;
            }
            return card;
          }),
        },
      });
    }

    if (fields.resourceCardTitle) {
      components.push({
        name: 'solutions-resource-cards',
        data: {
          title: fields.resourceCardTitle,
          column_size: 4,
          cards: fields.resourceCards.map((entry) => {
            const card = {
              icon: {
                name: entry.fields.iconName,
                variant: 'marketing',
                alt: '',
              },
              event_type: entry.fields.subtitle,
              header: entry.fields?.title,
              text: entry.fields.description,
              link_text: entry.fields.button?.fields?.text,
              href: entry.fields.button?.fields?.externalUrl,
              data_ga_name: entry.fields.title,
              data_ga_location: 'resource cards',
            };
            if (entry.fields.image?.fields?.file?.url) {
              card.image = `${entry.fields.image?.fields?.file?.url}?h=400&fl=progressive`;
            }
            return card;
          }),
        },
      });
    }

    const document = {
      ...topLevelFields,
      topics_header,
      crumbs,
      side_menu,
      components,
    };

    return document;
  } catch (e) {
    console.log(e);
    return null;
  }
}
