interface homePageData {
  title: string;
  description: string;
  /* eslint-disable babel/camelcase */
  hero_scroll_gallery?: any;
  customer_logos_block?: any;
  intro_block?: any;
  blurb_grid?: any;
  feature_showcase?: any;
  tabbed_features?: any;
  recognition_spotlight?: any;
  quotes_carousel_block?: any;
}

function buttonBuilder(buttonFields) {
  const isModal =
    buttonFields.externalUrl && buttonFields.externalUrl.includes('vimeo');
  return {
    text: buttonFields.text,
    link: (!isModal && buttonFields.externalUrl) || '',
    href: buttonFields.externalUrl || '',
    data_ga_name: buttonFields.dataGaName,
    data_ga_location: buttonFields.dataGaLocation,
    modal: isModal &&
      buttonFields.externalUrl.includes('vimeo') && {
        video_link: buttonFields.externalUrl,
        video_title: buttonFields.dataGaName,
      },
  };
}
function imageBuilder(imageData) {
  return {
    src: imageData.file.url,
    alt: imageData.title,
  };
}
function cardBuilder(cards) {
  return cards.map((card, index) => {
    const button =
      card.fields.button?.fields && buttonBuilder(card.fields.button.fields);
    return {
      type: index === 0 ? 'block' : 'card',
      title: index === 0 && card.fields.title,
      blurb: index === 0 ? card.fields.description : card.fields.title,
      primary_button: index === 0 && button,
      button: index !== 0 && button,
      image:
        card.fields.image?.fields && imageBuilder(card.fields.image?.fields),
    };
  });
}
function customerBuilder(customers) {
  return customers.map((customer) => {
    return {
      image_url: customer.fields.image?.fields?.file?.url,
      alt: customer.fields.altText || '',
      url: customer.fields.link,
      size: customer.fields.customFields?.size,
    };
  });
}
function heroBuilder(section) {
  const heroData = {
    controls: { prev: 'previous slide', next: 'next slide' },
    slides: cardBuilder(section.card),
  };

  return heroData;
}
function customerLogosBuilder(section) {
  const sectionData = {
    text: {
      legend: section.text,
      url: section.link[0],
      data_ga_name: section.text,
      data_ga_location: section.componentName,
    },
    showcased_enterprises: customerBuilder(section.logo),
  };

  return sectionData;
}
function introBuilder(section) {
  return {
    title: section.header,
    blurb: section.text,
    primary_button: section.cta.fields && buttonBuilder(section.cta.fields),
    secondary_button:
      section.secondaryCta.fields && buttonBuilder(section.secondaryCta.fields),
    image:
      section.assets?.length &&
      imageBuilder(section.assets[0].fields.image.fields),
  };
}
function blurbGridBuilder(section) {
  return section.card.map((card) => {
    return {
      icon: { name: card.fields.iconName, size: 'md', variant: 'marketing' },
      title: card.fields.title,
      blurb: card.fields.description,
    };
  });
}
function featureShowcaseBuilder(section) {
  return section.card.map((card) => {
    const stats =
      card.fields.pills &&
      card.fields.pills.map((pill, index) => {
        const getStatArray = pill.split(',');
        return {
          value: getStatArray[0],
          blurb: getStatArray[1],
          icon:
            card.fields.customFields.icons[index] &&
            card.fields.customFields?.icons[index],
        };
      });
    return {
      title: card.fields.title,
      blurb: card.fields.description || '',
      button:
        card.fields.button?.fields && buttonBuilder(card.fields.button.fields),
      icon: { name: card.fields.iconName, size: 'md', variant: 'marketing' },
      stats,
    };
  });
}
function featuresBuilder(features) {
  return features.map((feature) => {
    return {
      text: feature.fields.text,
      icon: {
        name: feature.fields.iconName,
        size: 'md',
        variant: 'marketing',
      },
      href: feature.fields.externalUrl,
      data_ga_name: feature.fields.dataGaName,
      data_ga_location: feature.fields.dataGaLocation,
    };
  });
}
function tabbedFeatures(section) {
  const tabs = section.tabs.map((tab) => {
    const tabPanel =
      tab.fields.tabPanelContent && tab.fields.tabPanelContent[0];
    const features =
      tabPanel.fields.blockGroup && featuresBuilder(tabPanel.fields.blockGroup);

    const asset = (tabPanel.fields.assets && tabPanel.fields.assets[0]) || null;
    const codeBlocks =
      asset &&
      asset.sys.contentType.sys.id === 'codeSuggestionsIde' &&
      asset.fields.codeBlocks.code_blocks;
    return {
      header: tab.fields.tabButtonText,
      header_mobile: tab.fields.tabId,
      data_ga_name: tab.fields.tabGaName,
      data_ga_location: tab.fields.tabGaLocation,
      blurb: tabPanel.fields.text,
      features: tabPanel.fields.blockGroup && features,
      code_blocks: codeBlocks,
      image: asset && !codeBlocks && imageBuilder(asset.fields.image.fields),
    };
  });
  return {
    header: section.header,
    blurb: section.tabControlsSubtext,
    tabs,
  };
}
function quotesCarouselBlock(section) {
  const tabs = section.tabs.map((tab) => {
    const sectionFields = tab.fields;
    const button = tab.fields.tabCta.fields && { ...tab.fields.tabCta.fields };
    const tabContentArray = sectionFields.tabPanelContent.map(
      (block) => block.fields,
    );
    const tabContentObjects = tabContentArray.reduce(
      (previous: any, current: any) => {
        const mergedArray = {
          ...previous,
          ...current,
        };

        return mergedArray;
      },
    );
    const stats =
      tabContentObjects.pills &&
      tabContentObjects.pills.map((pill) => {
        const getStatArray = pill.split(':');
        return {
          data: {
            highlight: getStatArray[0],
            subtitle: getStatArray[1],
          },
        };
      });
    const isModal =
      button.externalUrl.includes('vimeo') ||
      button.externalUrl.includes('youtube');
    return {
      logo: {
        url: sectionFields.tabButtonLogo.fields.image.fields.file.url,
        alt: sectionFields.tabButtonLogo.fields.altText,
      },
      header: tabContentObjects.description,
      url: button.externalUrl,
      data_ga_name: button.dataGaName,
      data_ga_location: button.dataGaLocation,
      cta_text: button.text,
      quote: tabContentObjects.quoteText,
      author: tabContentObjects.author,
      role: tabContentObjects.authorTitle,
      company: tabContentObjects.authorCompany,
      modal: isModal,
      main_img: {
        url: tabContentObjects.authorHeadshot.fields.image.fields.file.url,
        alt: tabContentObjects.authorHeadshot.fields.altText,
      },
      statistic_samples: stats,
    };
  });
  return tabs;
}
function recognitionSpotlight(block) {
  const statArray = block.description && block.description.split('\n');

  const stats = statArray.map((pill) => {
    const getStatArray = pill.split(',');
    return {
      value: getStatArray[0],
      blurb: getStatArray[1],
    };
  });

  const cards = block.card.map((card) => {
    const badges =
      card.fields.contentArea &&
      card.fields.contentArea.map((badge) => {
        return {
          src: badge.fields.image.fields.file.url,
          alt: badge.fields.altText,
        };
      });
    return {
      header: card.fields.title,
      text: card.fields.description,
      logo: card.fields.image?.fields.file.url || '',
      alt: card.fields.image?.fields.title || '',
      link: card.fields.button && buttonBuilder(card.fields.button.fields),
      badges,
    };
  });
  return {
    heading: block.header,
    stats,
    cards,
  };
}
function pageBuilder(page) {
  const data = page.map((section) => {
    const id = section.sys?.id;
    switch (id) {
      case '5bE64F5zEqjDz7JOqgvQal':
        return { hero_scroll_gallery: { ...heroBuilder(section.fields) } };
      case '4gp23P3JTXnhXKnZcRwjYG':
        return {
          customer_logos_block: { ...customerLogosBuilder(section.fields) },
        };
      case '60tHoPK8BI5hKO1pyKhjSA':
        return {
          intro_block: { ...introBuilder(section.fields) },
        };
      case '6F8B5pC7tNI5xmoXHi1n70':
        return {
          blurb_grid: blurbGridBuilder(section.fields),
        };
      case '2m4k7kemp0VoeYuVvE5gao':
        return {
          feature_showcase: featureShowcaseBuilder(section.fields),
        };
      case '18udhuRjROGMgBzVW1Y3Lx':
        return {
          tabbed_features: tabbedFeatures(section.fields),
        };
      case '69ibQBf31027f3pC4DjGtO':
        return {
          quotes_carousel_block: {
            controls: { prev: ' previous case study', next: 'next case study' },
            quotes: quotesCarouselBlock(section.fields),
          },
        };
      case '5BLkajORKNLLdqzacBjEUN':
        return {
          recognition_spotlight: recognitionSpotlight(section.fields),
        };
      default:
        return { type: id, ...section.fields };
    }
  });
  const extractAsObjects = data.reduce((previous: any, current: any) => {
    const mergedBlocks = {
      ...previous,
      ...current,
    };

    return mergedBlocks;
  });

  return extractAsObjects;
}
function metadataHelper(data) {
  const seo = data[0].fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    no_index: seo.noIndex,
  };
}

export function homepageHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = pageBuilder(pageContent);
  const metadata = metadataHelper(seoMetadata);
  const transformedData: homePageData = {
    ...metadata,
    ...cleanPagecontent,
  };

  return transformedData;
}
